﻿namespace AutoStorage
{
    partial class redactWayBills
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(redactWayBills));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            this.backToWorkWithBills = new System.Windows.Forms.Button();
            this.bunifuGradientPanel2 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.обновитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bunifuGradientPanel3 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.slidemenu = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuFlatButton1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton5 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton4 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton3 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton8 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnSlide = new System.Windows.Forms.Button();
            this.contentSlideMenu = new MetroFramework.Controls.MetroPanel();
            this.metroTabControl2 = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage4 = new MetroFramework.Controls.MetroTabPage();
            this.imgBtn3 = new Bunifu.Framework.UI.BunifuImageButton();
            this.imgBtn4 = new Bunifu.Framework.UI.BunifuImageButton();
            this.imgBtn1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.imgBtn2 = new Bunifu.Framework.UI.BunifuImageButton();
            this.metroTabPage5 = new MetroFramework.Controls.MetroTabPage();
            this.imgBtn21 = new Bunifu.Framework.UI.BunifuImageButton();
            this.imgBtn22 = new Bunifu.Framework.UI.BunifuImageButton();
            this.metroTabPage6 = new MetroFramework.Controls.MetroTabPage();
            this.imgBtn31 = new Bunifu.Framework.UI.BunifuImageButton();
            this.imgBtn33 = new Bunifu.Framework.UI.BunifuImageButton();
            this.imgBtn32 = new Bunifu.Framework.UI.BunifuImageButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.exitBtn = new System.Windows.Forms.Button();
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.priceSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.typeSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.dateSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.updateBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.transportPriceBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel27 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel23 = new MetroFramework.Controls.MetroLabel();
            this.loadTypeBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel21 = new MetroFramework.Controls.MetroLabel();
            this.carcasMaterialBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.heighBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.amountBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel22 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.capacityBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel24 = new MetroFramework.Controls.MetroLabel();
            this.carcasTypeBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel25 = new MetroFramework.Controls.MetroLabel();
            this.carryingBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.lengthBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.widthBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.metroLabel26 = new MetroFramework.Controls.MetroLabel();
            this.bunifuFlatButton7 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.addGoodsBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.imgList = new System.Windows.Forms.PictureBox();
            this.metroLabel20 = new MetroFramework.Controls.MetroLabel();
            this.bunifuFlatButton6 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.metroDateTime1 = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel28 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel29 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.transportSumBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.cubageBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.weightBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.goodsNameBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.goodsTypeBox = new MetroFramework.Controls.MetroComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.idBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.fromPhoneBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.fromEmailBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.fromCityBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.companyFromBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.fromAdresBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.toPhoneBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.toEmailBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.toCityBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.companyToBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.toAdresBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroGrid1 = new MetroFramework.Controls.MetroGrid();
            this.panelAnimator = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.bunifuGradientPanel2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.bunifuGradientPanel3.SuspendLayout();
            this.slidemenu.SuspendLayout();
            this.contentSlideMenu.SuspendLayout();
            this.metroTabControl2.SuspendLayout();
            this.metroTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn2)).BeginInit();
            this.metroTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn22)).BeginInit();
            this.metroTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn32)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgList)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // backToWorkWithBills
            // 
            this.backToWorkWithBills.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("backToWorkWithBills.BackgroundImage")));
            this.backToWorkWithBills.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelAnimator.SetDecoration(this.backToWorkWithBills, BunifuAnimatorNS.DecorationType.None);
            this.backToWorkWithBills.FlatAppearance.BorderSize = 0;
            this.backToWorkWithBills.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backToWorkWithBills.Location = new System.Drawing.Point(6, 21);
            this.backToWorkWithBills.Name = "backToWorkWithBills";
            this.backToWorkWithBills.Size = new System.Drawing.Size(32, 32);
            this.backToWorkWithBills.TabIndex = 20;
            this.backToWorkWithBills.UseVisualStyleBackColor = true;
            this.backToWorkWithBills.Click += new System.EventHandler(this.backToWorkWithBills_Click);
            // 
            // bunifuGradientPanel2
            // 
            this.bunifuGradientPanel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel2.BackgroundImage")));
            this.bunifuGradientPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel2.Controls.Add(this.menuStrip1);
            this.panelAnimator.SetDecoration(this.bunifuGradientPanel2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuGradientPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuGradientPanel2.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.bunifuGradientPanel2.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.bunifuGradientPanel2.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.bunifuGradientPanel2.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.bunifuGradientPanel2.Location = new System.Drawing.Point(20, 60);
            this.bunifuGradientPanel2.Name = "bunifuGradientPanel2";
            this.bunifuGradientPanel2.Quality = 10;
            this.bunifuGradientPanel2.Size = new System.Drawing.Size(1272, 48);
            this.bunifuGradientPanel2.TabIndex = 34;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.menuStrip1, BunifuAnimatorNS.DecorationType.None);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.обновитьToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStrip1.Size = new System.Drawing.Size(1272, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // обновитьToolStripMenuItem
            // 
            this.обновитьToolStripMenuItem.Name = "обновитьToolStripMenuItem";
            this.обновитьToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.обновитьToolStripMenuItem.Text = "Обновить";
            this.обновитьToolStripMenuItem.Click += new System.EventHandler(this.обновитьToolStripMenuItem_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            this.справкаToolStripMenuItem.Click += new System.EventHandler(this.справкаToolStripMenuItem_Click);
            // 
            // bunifuGradientPanel3
            // 
            this.bunifuGradientPanel3.AutoScroll = true;
            this.bunifuGradientPanel3.BackColor = System.Drawing.Color.White;
            this.bunifuGradientPanel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel3.BackgroundImage")));
            this.bunifuGradientPanel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel3.Controls.Add(this.slidemenu);
            this.bunifuGradientPanel3.Controls.Add(this.contentSlideMenu);
            this.bunifuGradientPanel3.Controls.Add(this.priceSearchBox);
            this.bunifuGradientPanel3.Controls.Add(this.typeSearchBox);
            this.bunifuGradientPanel3.Controls.Add(this.dateSearchBox);
            this.bunifuGradientPanel3.Controls.Add(this.groupBox4);
            this.bunifuGradientPanel3.Controls.Add(this.groupBox3);
            this.bunifuGradientPanel3.Controls.Add(this.groupBox1);
            this.bunifuGradientPanel3.Controls.Add(this.groupBox2);
            this.bunifuGradientPanel3.Controls.Add(this.metroGrid1);
            this.panelAnimator.SetDecoration(this.bunifuGradientPanel3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuGradientPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bunifuGradientPanel3.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel3.Location = new System.Drawing.Point(20, 108);
            this.bunifuGradientPanel3.Name = "bunifuGradientPanel3";
            this.bunifuGradientPanel3.Quality = 10;
            this.bunifuGradientPanel3.Size = new System.Drawing.Size(1272, 609);
            this.bunifuGradientPanel3.TabIndex = 35;
            this.bunifuGradientPanel3.MouseEnter += new System.EventHandler(this.bunifuGradientPanel3_MouseEnter);
            // 
            // slidemenu
            // 
            this.slidemenu.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("slidemenu.BackgroundImage")));
            this.slidemenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slidemenu.Controls.Add(this.bunifuFlatButton1);
            this.slidemenu.Controls.Add(this.bunifuFlatButton5);
            this.slidemenu.Controls.Add(this.bunifuFlatButton4);
            this.slidemenu.Controls.Add(this.bunifuFlatButton3);
            this.slidemenu.Controls.Add(this.bunifuFlatButton2);
            this.slidemenu.Controls.Add(this.bunifuFlatButton8);
            this.slidemenu.Controls.Add(this.btnSlide);
            this.panelAnimator.SetDecoration(this.slidemenu, BunifuAnimatorNS.DecorationType.None);
            this.slidemenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.slidemenu.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.slidemenu.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.slidemenu.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.slidemenu.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.slidemenu.Location = new System.Drawing.Point(0, 0);
            this.slidemenu.Name = "slidemenu";
            this.slidemenu.Quality = 10;
            this.slidemenu.Size = new System.Drawing.Size(50, 875);
            this.slidemenu.TabIndex = 65;
            // 
            // bunifuFlatButton1
            // 
            this.bunifuFlatButton1.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton1.BackColor = System.Drawing.Color.Peru;
            this.bunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton1.BorderRadius = 0;
            this.bunifuFlatButton1.ButtonText = "Сменить пользователя";
            this.bunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.bunifuFlatButton1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton1.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton1.Iconimage = global::AutoStorage.Properties.Resources.icons8_Export_64;
            this.bunifuFlatButton1.Iconimage_right = null;
            this.bunifuFlatButton1.Iconimage_right_Selected = null;
            this.bunifuFlatButton1.Iconimage_Selected = null;
            this.bunifuFlatButton1.IconMarginLeft = 0;
            this.bunifuFlatButton1.IconMarginRight = 0;
            this.bunifuFlatButton1.IconRightVisible = true;
            this.bunifuFlatButton1.IconRightZoom = 0D;
            this.bunifuFlatButton1.IconVisible = true;
            this.bunifuFlatButton1.IconZoom = 90D;
            this.bunifuFlatButton1.IsTab = false;
            this.bunifuFlatButton1.Location = new System.Drawing.Point(0, 371);
            this.bunifuFlatButton1.Name = "bunifuFlatButton1";
            this.bunifuFlatButton1.Normalcolor = System.Drawing.Color.Peru;
            this.bunifuFlatButton1.OnHovercolor = System.Drawing.Color.DarkOrange;
            this.bunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton1.selected = false;
            this.bunifuFlatButton1.Size = new System.Drawing.Size(241, 48);
            this.bunifuFlatButton1.TabIndex = 29;
            this.bunifuFlatButton1.Text = "Сменить пользователя";
            this.bunifuFlatButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton1.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton1.Click += new System.EventHandler(this.bunifuFlatButton1_Click);
            // 
            // bunifuFlatButton5
            // 
            this.bunifuFlatButton5.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton5.BackColor = System.Drawing.Color.Peru;
            this.bunifuFlatButton5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton5.BorderRadius = 0;
            this.bunifuFlatButton5.ButtonText = " Личный кабинет";
            this.bunifuFlatButton5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.bunifuFlatButton5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton5.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton5.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton5.Iconimage = global::AutoStorage.Properties.Resources.icons8_User_64;
            this.bunifuFlatButton5.Iconimage_right = null;
            this.bunifuFlatButton5.Iconimage_right_Selected = null;
            this.bunifuFlatButton5.Iconimage_Selected = null;
            this.bunifuFlatButton5.IconMarginLeft = 0;
            this.bunifuFlatButton5.IconMarginRight = 0;
            this.bunifuFlatButton5.IconRightVisible = true;
            this.bunifuFlatButton5.IconRightZoom = 0D;
            this.bunifuFlatButton5.IconVisible = true;
            this.bunifuFlatButton5.IconZoom = 90D;
            this.bunifuFlatButton5.IsTab = false;
            this.bunifuFlatButton5.Location = new System.Drawing.Point(0, 317);
            this.bunifuFlatButton5.Name = "bunifuFlatButton5";
            this.bunifuFlatButton5.Normalcolor = System.Drawing.Color.Peru;
            this.bunifuFlatButton5.OnHovercolor = System.Drawing.Color.DarkOrange;
            this.bunifuFlatButton5.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton5.selected = false;
            this.bunifuFlatButton5.Size = new System.Drawing.Size(241, 48);
            this.bunifuFlatButton5.TabIndex = 24;
            this.bunifuFlatButton5.Text = " Личный кабинет";
            this.bunifuFlatButton5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton5.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton5.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton5.Click += new System.EventHandler(this.bunifuFlatButton5_Click);
            // 
            // bunifuFlatButton4
            // 
            this.bunifuFlatButton4.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton4.BackColor = System.Drawing.Color.Peru;
            this.bunifuFlatButton4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton4.BorderRadius = 0;
            this.bunifuFlatButton4.ButtonText = " Просмотр складов";
            this.bunifuFlatButton4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.bunifuFlatButton4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton4.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton4.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton4.Iconimage = global::AutoStorage.Properties.Resources.icons8_Product_64;
            this.bunifuFlatButton4.Iconimage_right = null;
            this.bunifuFlatButton4.Iconimage_right_Selected = null;
            this.bunifuFlatButton4.Iconimage_Selected = null;
            this.bunifuFlatButton4.IconMarginLeft = 0;
            this.bunifuFlatButton4.IconMarginRight = 0;
            this.bunifuFlatButton4.IconRightVisible = true;
            this.bunifuFlatButton4.IconRightZoom = 0D;
            this.bunifuFlatButton4.IconVisible = true;
            this.bunifuFlatButton4.IconZoom = 90D;
            this.bunifuFlatButton4.IsTab = false;
            this.bunifuFlatButton4.Location = new System.Drawing.Point(0, 263);
            this.bunifuFlatButton4.Name = "bunifuFlatButton4";
            this.bunifuFlatButton4.Normalcolor = System.Drawing.Color.Peru;
            this.bunifuFlatButton4.OnHovercolor = System.Drawing.Color.DarkOrange;
            this.bunifuFlatButton4.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton4.selected = false;
            this.bunifuFlatButton4.Size = new System.Drawing.Size(241, 48);
            this.bunifuFlatButton4.TabIndex = 25;
            this.bunifuFlatButton4.Text = " Просмотр складов";
            this.bunifuFlatButton4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton4.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton4.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton4.Click += new System.EventHandler(this.bunifuFlatButton4_Click);
            // 
            // bunifuFlatButton3
            // 
            this.bunifuFlatButton3.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton3.BackColor = System.Drawing.Color.Peru;
            this.bunifuFlatButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton3.BorderRadius = 0;
            this.bunifuFlatButton3.ButtonText = " Перегрузочные операции";
            this.bunifuFlatButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.bunifuFlatButton3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton3.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton3.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton3.Iconimage = global::AutoStorage.Properties.Resources.icons8_Fork_Lift_64;
            this.bunifuFlatButton3.Iconimage_right = null;
            this.bunifuFlatButton3.Iconimage_right_Selected = null;
            this.bunifuFlatButton3.Iconimage_Selected = null;
            this.bunifuFlatButton3.IconMarginLeft = 0;
            this.bunifuFlatButton3.IconMarginRight = 0;
            this.bunifuFlatButton3.IconRightVisible = true;
            this.bunifuFlatButton3.IconRightZoom = 0D;
            this.bunifuFlatButton3.IconVisible = true;
            this.bunifuFlatButton3.IconZoom = 90D;
            this.bunifuFlatButton3.IsTab = false;
            this.bunifuFlatButton3.Location = new System.Drawing.Point(0, 209);
            this.bunifuFlatButton3.Name = "bunifuFlatButton3";
            this.bunifuFlatButton3.Normalcolor = System.Drawing.Color.Peru;
            this.bunifuFlatButton3.OnHovercolor = System.Drawing.Color.DarkOrange;
            this.bunifuFlatButton3.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton3.selected = false;
            this.bunifuFlatButton3.Size = new System.Drawing.Size(241, 48);
            this.bunifuFlatButton3.TabIndex = 26;
            this.bunifuFlatButton3.Text = " Перегрузочные операции";
            this.bunifuFlatButton3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton3.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton3.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton3.Click += new System.EventHandler(this.bunifuFlatButton3_Click);
            // 
            // bunifuFlatButton2
            // 
            this.bunifuFlatButton2.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton2.BackColor = System.Drawing.Color.Peru;
            this.bunifuFlatButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton2.BorderRadius = 0;
            this.bunifuFlatButton2.ButtonText = " Работа с накладными";
            this.bunifuFlatButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.bunifuFlatButton2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton2.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton2.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton2.Iconimage = global::AutoStorage.Properties.Resources.icons8_Purchase_Order_641;
            this.bunifuFlatButton2.Iconimage_right = null;
            this.bunifuFlatButton2.Iconimage_right_Selected = null;
            this.bunifuFlatButton2.Iconimage_Selected = null;
            this.bunifuFlatButton2.IconMarginLeft = 0;
            this.bunifuFlatButton2.IconMarginRight = 0;
            this.bunifuFlatButton2.IconRightVisible = true;
            this.bunifuFlatButton2.IconRightZoom = 0D;
            this.bunifuFlatButton2.IconVisible = true;
            this.bunifuFlatButton2.IconZoom = 90D;
            this.bunifuFlatButton2.IsTab = false;
            this.bunifuFlatButton2.Location = new System.Drawing.Point(0, 155);
            this.bunifuFlatButton2.Name = "bunifuFlatButton2";
            this.bunifuFlatButton2.Normalcolor = System.Drawing.Color.Peru;
            this.bunifuFlatButton2.OnHovercolor = System.Drawing.Color.DarkOrange;
            this.bunifuFlatButton2.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton2.selected = false;
            this.bunifuFlatButton2.Size = new System.Drawing.Size(241, 48);
            this.bunifuFlatButton2.TabIndex = 27;
            this.bunifuFlatButton2.Text = " Работа с накладными";
            this.bunifuFlatButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton2.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton2.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton2.Click += new System.EventHandler(this.bunifuFlatButton2_Click);
            // 
            // bunifuFlatButton8
            // 
            this.bunifuFlatButton8.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton8.BackColor = System.Drawing.Color.Peru;
            this.bunifuFlatButton8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton8.BorderRadius = 0;
            this.bunifuFlatButton8.ButtonText = " Просмотр накладных";
            this.bunifuFlatButton8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.bunifuFlatButton8, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton8.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton8.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton8.Iconimage = global::AutoStorage.Properties.Resources.icons8_Search_64;
            this.bunifuFlatButton8.Iconimage_right = null;
            this.bunifuFlatButton8.Iconimage_right_Selected = null;
            this.bunifuFlatButton8.Iconimage_Selected = null;
            this.bunifuFlatButton8.IconMarginLeft = 0;
            this.bunifuFlatButton8.IconMarginRight = 0;
            this.bunifuFlatButton8.IconRightVisible = true;
            this.bunifuFlatButton8.IconRightZoom = 0D;
            this.bunifuFlatButton8.IconVisible = true;
            this.bunifuFlatButton8.IconZoom = 90D;
            this.bunifuFlatButton8.IsTab = false;
            this.bunifuFlatButton8.Location = new System.Drawing.Point(0, 101);
            this.bunifuFlatButton8.Name = "bunifuFlatButton8";
            this.bunifuFlatButton8.Normalcolor = System.Drawing.Color.Peru;
            this.bunifuFlatButton8.OnHovercolor = System.Drawing.Color.DarkOrange;
            this.bunifuFlatButton8.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton8.selected = false;
            this.bunifuFlatButton8.Size = new System.Drawing.Size(241, 48);
            this.bunifuFlatButton8.TabIndex = 28;
            this.bunifuFlatButton8.Text = " Просмотр накладных";
            this.bunifuFlatButton8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton8.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton8.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton8.Click += new System.EventHandler(this.bunifuFlatButton8_Click);
            // 
            // btnSlide
            // 
            this.btnSlide.BackColor = System.Drawing.Color.Transparent;
            this.btnSlide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelAnimator.SetDecoration(this.btnSlide, BunifuAnimatorNS.DecorationType.None);
            this.btnSlide.FlatAppearance.BorderSize = 0;
            this.btnSlide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSlide.Image = global::AutoStorage.Properties.Resources.icons8_Menu_32;
            this.btnSlide.Location = new System.Drawing.Point(6, 12);
            this.btnSlide.Name = "btnSlide";
            this.btnSlide.Size = new System.Drawing.Size(36, 37);
            this.btnSlide.TabIndex = 4;
            this.btnSlide.UseVisualStyleBackColor = false;
            this.btnSlide.Click += new System.EventHandler(this.btnSlide_Click);
            // 
            // contentSlideMenu
            // 
            this.contentSlideMenu.Controls.Add(this.metroTabControl2);
            this.contentSlideMenu.Controls.Add(this.panel1);
            this.contentSlideMenu.Controls.Add(this.metroTabControl1);
            this.panelAnimator.SetDecoration(this.contentSlideMenu, BunifuAnimatorNS.DecorationType.None);
            this.contentSlideMenu.HorizontalScrollbarBarColor = true;
            this.contentSlideMenu.HorizontalScrollbarHighlightOnWheel = false;
            this.contentSlideMenu.HorizontalScrollbarSize = 10;
            this.contentSlideMenu.Location = new System.Drawing.Point(50, -1);
            this.contentSlideMenu.Name = "contentSlideMenu";
            this.contentSlideMenu.Size = new System.Drawing.Size(26, 19);
            this.contentSlideMenu.TabIndex = 19;
            this.contentSlideMenu.VerticalScrollbarBarColor = true;
            this.contentSlideMenu.VerticalScrollbarHighlightOnWheel = false;
            this.contentSlideMenu.VerticalScrollbarSize = 10;
            this.contentSlideMenu.Visible = false;
            // 
            // metroTabControl2
            // 
            this.metroTabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroTabControl2.Controls.Add(this.metroTabPage4);
            this.metroTabControl2.Controls.Add(this.metroTabPage5);
            this.metroTabControl2.Controls.Add(this.metroTabPage6);
            this.panelAnimator.SetDecoration(this.metroTabControl2, BunifuAnimatorNS.DecorationType.None);
            this.metroTabControl2.Location = new System.Drawing.Point(-1175, 39);
            this.metroTabControl2.Name = "metroTabControl2";
            this.metroTabControl2.SelectedIndex = 2;
            this.metroTabControl2.Size = new System.Drawing.Size(979, 733);
            this.metroTabControl2.TabIndex = 2;
            this.metroTabControl2.UseSelectable = true;
            // 
            // metroTabPage4
            // 
            this.metroTabPage4.Controls.Add(this.imgBtn3);
            this.metroTabPage4.Controls.Add(this.imgBtn4);
            this.metroTabPage4.Controls.Add(this.imgBtn1);
            this.metroTabPage4.Controls.Add(this.imgBtn2);
            this.panelAnimator.SetDecoration(this.metroTabPage4, BunifuAnimatorNS.DecorationType.None);
            this.metroTabPage4.HorizontalScrollbarBarColor = true;
            this.metroTabPage4.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.HorizontalScrollbarSize = 10;
            this.metroTabPage4.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage4.Name = "metroTabPage4";
            this.metroTabPage4.Size = new System.Drawing.Size(971, 691);
            this.metroTabPage4.TabIndex = 0;
            this.metroTabPage4.Text = "С рефрижераторным кузовом";
            this.metroTabPage4.VerticalScrollbarBarColor = true;
            this.metroTabPage4.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.VerticalScrollbarSize = 10;
            // 
            // imgBtn3
            // 
            this.imgBtn3.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.imgBtn3, BunifuAnimatorNS.DecorationType.None);
            this.imgBtn3.Image = global::AutoStorage.Properties.Resources._3;
            this.imgBtn3.ImageActive = null;
            this.imgBtn3.InitialImage = null;
            this.imgBtn3.Location = new System.Drawing.Point(63, 344);
            this.imgBtn3.Name = "imgBtn3";
            this.imgBtn3.Size = new System.Drawing.Size(364, 344);
            this.imgBtn3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBtn3.TabIndex = 5;
            this.imgBtn3.TabStop = false;
            this.imgBtn3.Zoom = 10;
            this.imgBtn3.Click += new System.EventHandler(this.imgBtn3_Click);
            // 
            // imgBtn4
            // 
            this.imgBtn4.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.imgBtn4, BunifuAnimatorNS.DecorationType.None);
            this.imgBtn4.Image = global::AutoStorage.Properties.Resources._4;
            this.imgBtn4.ImageActive = null;
            this.imgBtn4.InitialImage = null;
            this.imgBtn4.Location = new System.Drawing.Point(495, 337);
            this.imgBtn4.Name = "imgBtn4";
            this.imgBtn4.Size = new System.Drawing.Size(362, 337);
            this.imgBtn4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBtn4.TabIndex = 6;
            this.imgBtn4.TabStop = false;
            this.imgBtn4.Zoom = 10;
            this.imgBtn4.Click += new System.EventHandler(this.imgBtn4_Click);
            // 
            // imgBtn1
            // 
            this.imgBtn1.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.imgBtn1, BunifuAnimatorNS.DecorationType.None);
            this.imgBtn1.Image = global::AutoStorage.Properties.Resources._1;
            this.imgBtn1.ImageActive = null;
            this.imgBtn1.InitialImage = null;
            this.imgBtn1.Location = new System.Drawing.Point(63, 8);
            this.imgBtn1.Name = "imgBtn1";
            this.imgBtn1.Size = new System.Drawing.Size(352, 330);
            this.imgBtn1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBtn1.TabIndex = 2;
            this.imgBtn1.TabStop = false;
            this.imgBtn1.Zoom = 10;
            this.imgBtn1.Click += new System.EventHandler(this.imgBtn1_Click);
            // 
            // imgBtn2
            // 
            this.imgBtn2.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.imgBtn2, BunifuAnimatorNS.DecorationType.None);
            this.imgBtn2.Image = global::AutoStorage.Properties.Resources._21;
            this.imgBtn2.ImageActive = null;
            this.imgBtn2.InitialImage = null;
            this.imgBtn2.Location = new System.Drawing.Point(505, 8);
            this.imgBtn2.Name = "imgBtn2";
            this.imgBtn2.Size = new System.Drawing.Size(352, 330);
            this.imgBtn2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBtn2.TabIndex = 2;
            this.imgBtn2.TabStop = false;
            this.imgBtn2.Zoom = 10;
            this.imgBtn2.Click += new System.EventHandler(this.imgBtn2_Click);
            // 
            // metroTabPage5
            // 
            this.metroTabPage5.Controls.Add(this.imgBtn21);
            this.metroTabPage5.Controls.Add(this.imgBtn22);
            this.panelAnimator.SetDecoration(this.metroTabPage5, BunifuAnimatorNS.DecorationType.None);
            this.metroTabPage5.HorizontalScrollbarBarColor = true;
            this.metroTabPage5.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.HorizontalScrollbarSize = 10;
            this.metroTabPage5.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage5.Name = "metroTabPage5";
            this.metroTabPage5.Size = new System.Drawing.Size(971, 691);
            this.metroTabPage5.TabIndex = 1;
            this.metroTabPage5.Text = "С изотермическим кузовом";
            this.metroTabPage5.VerticalScrollbarBarColor = true;
            this.metroTabPage5.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.VerticalScrollbarSize = 10;
            // 
            // imgBtn21
            // 
            this.imgBtn21.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.imgBtn21, BunifuAnimatorNS.DecorationType.None);
            this.imgBtn21.Image = global::AutoStorage.Properties.Resources._2_1;
            this.imgBtn21.ImageActive = null;
            this.imgBtn21.InitialImage = null;
            this.imgBtn21.Location = new System.Drawing.Point(87, 31);
            this.imgBtn21.Name = "imgBtn21";
            this.imgBtn21.Size = new System.Drawing.Size(352, 330);
            this.imgBtn21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBtn21.TabIndex = 7;
            this.imgBtn21.TabStop = false;
            this.imgBtn21.Zoom = 10;
            this.imgBtn21.Click += new System.EventHandler(this.imgBtn21_Click);
            // 
            // imgBtn22
            // 
            this.imgBtn22.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.imgBtn22, BunifuAnimatorNS.DecorationType.None);
            this.imgBtn22.Image = global::AutoStorage.Properties.Resources._4266_3c2ed52bf940ddf2e14a954b2851f2c7;
            this.imgBtn22.ImageActive = null;
            this.imgBtn22.InitialImage = null;
            this.imgBtn22.Location = new System.Drawing.Point(528, 31);
            this.imgBtn22.Name = "imgBtn22";
            this.imgBtn22.Size = new System.Drawing.Size(352, 330);
            this.imgBtn22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBtn22.TabIndex = 8;
            this.imgBtn22.TabStop = false;
            this.imgBtn22.Zoom = 10;
            this.imgBtn22.Click += new System.EventHandler(this.imgBtn22_Click);
            // 
            // metroTabPage6
            // 
            this.metroTabPage6.Controls.Add(this.imgBtn31);
            this.metroTabPage6.Controls.Add(this.imgBtn33);
            this.metroTabPage6.Controls.Add(this.imgBtn32);
            this.panelAnimator.SetDecoration(this.metroTabPage6, BunifuAnimatorNS.DecorationType.None);
            this.metroTabPage6.HorizontalScrollbarBarColor = true;
            this.metroTabPage6.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage6.HorizontalScrollbarSize = 10;
            this.metroTabPage6.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage6.Name = "metroTabPage6";
            this.metroTabPage6.Size = new System.Drawing.Size(971, 691);
            this.metroTabPage6.TabIndex = 2;
            this.metroTabPage6.Text = "С крытым тентованным кузовом";
            this.metroTabPage6.VerticalScrollbarBarColor = true;
            this.metroTabPage6.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage6.VerticalScrollbarSize = 10;
            // 
            // imgBtn31
            // 
            this.imgBtn31.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.imgBtn31, BunifuAnimatorNS.DecorationType.None);
            this.imgBtn31.Image = global::AutoStorage.Properties.Resources._3_1;
            this.imgBtn31.ImageActive = null;
            this.imgBtn31.InitialImage = null;
            this.imgBtn31.Location = new System.Drawing.Point(7, 22);
            this.imgBtn31.Name = "imgBtn31";
            this.imgBtn31.Size = new System.Drawing.Size(330, 330);
            this.imgBtn31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBtn31.TabIndex = 9;
            this.imgBtn31.TabStop = false;
            this.imgBtn31.Zoom = 10;
            this.imgBtn31.Click += new System.EventHandler(this.imgBtn31_Click);
            // 
            // imgBtn33
            // 
            this.imgBtn33.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.imgBtn33, BunifuAnimatorNS.DecorationType.None);
            this.imgBtn33.Image = global::AutoStorage.Properties.Resources._3_3;
            this.imgBtn33.ImageActive = null;
            this.imgBtn33.InitialImage = null;
            this.imgBtn33.Location = new System.Drawing.Point(677, 22);
            this.imgBtn33.Name = "imgBtn33";
            this.imgBtn33.Size = new System.Drawing.Size(306, 330);
            this.imgBtn33.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBtn33.TabIndex = 10;
            this.imgBtn33.TabStop = false;
            this.imgBtn33.Zoom = 10;
            this.imgBtn33.Click += new System.EventHandler(this.imgBtn33_Click);
            // 
            // imgBtn32
            // 
            this.imgBtn32.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.imgBtn32, BunifuAnimatorNS.DecorationType.None);
            this.imgBtn32.Image = global::AutoStorage.Properties.Resources._3_2;
            this.imgBtn32.ImageActive = null;
            this.imgBtn32.InitialImage = null;
            this.imgBtn32.Location = new System.Drawing.Point(354, 22);
            this.imgBtn32.Name = "imgBtn32";
            this.imgBtn32.Size = new System.Drawing.Size(306, 330);
            this.imgBtn32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBtn32.TabIndex = 10;
            this.imgBtn32.TabStop = false;
            this.imgBtn32.Zoom = 10;
            this.imgBtn32.Click += new System.EventHandler(this.imgBtn32_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.panel1.Controls.Add(this.exitBtn);
            this.panelAnimator.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.panel1.Location = new System.Drawing.Point(-1420, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1442, 30);
            this.panel1.TabIndex = 3;
            // 
            // exitBtn
            // 
            this.exitBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.exitBtn.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.exitBtn, BunifuAnimatorNS.DecorationType.None);
            this.exitBtn.FlatAppearance.BorderSize = 0;
            this.exitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exitBtn.Image = ((System.Drawing.Image)(resources.GetObject("exitBtn.Image")));
            this.exitBtn.Location = new System.Drawing.Point(1417, 6);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(21, 18);
            this.exitBtn.TabIndex = 12;
            this.exitBtn.UseVisualStyleBackColor = false;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // metroTabControl1
            // 
            this.panelAnimator.SetDecoration(this.metroTabControl1, BunifuAnimatorNS.DecorationType.None);
            this.metroTabControl1.Location = new System.Drawing.Point(26, 39);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.Size = new System.Drawing.Size(979, 733);
            this.metroTabControl1.TabIndex = 2;
            this.metroTabControl1.UseSelectable = true;
            // 
            // priceSearchBox
            // 
            this.priceSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.priceSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.priceSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.priceSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.priceSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.priceSearchBox.HintText = "Поиск по стоимости перегрузки";
            this.priceSearchBox.isPassword = false;
            this.priceSearchBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.priceSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.priceSearchBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.priceSearchBox.LineThickness = 3;
            this.priceSearchBox.Location = new System.Drawing.Point(513, 7);
            this.priceSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.priceSearchBox.Name = "priceSearchBox";
            this.priceSearchBox.Size = new System.Drawing.Size(192, 27);
            this.priceSearchBox.TabIndex = 75;
            this.priceSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.priceSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.priceSearchBox_KeyUp);
            // 
            // typeSearchBox
            // 
            this.typeSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.typeSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.typeSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.typeSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.typeSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.typeSearchBox.HintText = "Поиск по типу товара";
            this.typeSearchBox.isPassword = false;
            this.typeSearchBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.typeSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.typeSearchBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.typeSearchBox.LineThickness = 3;
            this.typeSearchBox.Location = new System.Drawing.Point(289, 7);
            this.typeSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.typeSearchBox.Name = "typeSearchBox";
            this.typeSearchBox.Size = new System.Drawing.Size(192, 27);
            this.typeSearchBox.TabIndex = 74;
            this.typeSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.typeSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.typeSearchBox_KeyUp);
            // 
            // dateSearchBox
            // 
            this.dateSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.dateSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.dateSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.dateSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dateSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.dateSearchBox.HintText = "Поиск по дате отправки";
            this.dateSearchBox.isPassword = false;
            this.dateSearchBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.dateSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.dateSearchBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.dateSearchBox.LineThickness = 3;
            this.dateSearchBox.Location = new System.Drawing.Point(67, 7);
            this.dateSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.dateSearchBox.Name = "dateSearchBox";
            this.dateSearchBox.Size = new System.Drawing.Size(192, 27);
            this.dateSearchBox.TabIndex = 73;
            this.dateSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.dateSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateSearchBox_KeyUp);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.updateBtn);
            this.groupBox4.Controls.Add(this.transportPriceBox);
            this.groupBox4.Controls.Add(this.metroLabel27);
            this.groupBox4.Controls.Add(this.metroLabel23);
            this.groupBox4.Controls.Add(this.loadTypeBox);
            this.groupBox4.Controls.Add(this.metroLabel21);
            this.groupBox4.Controls.Add(this.carcasMaterialBox);
            this.groupBox4.Controls.Add(this.metroLabel18);
            this.groupBox4.Controls.Add(this.heighBox);
            this.groupBox4.Controls.Add(this.metroLabel17);
            this.groupBox4.Controls.Add(this.amountBox);
            this.groupBox4.Controls.Add(this.metroLabel16);
            this.groupBox4.Controls.Add(this.metroLabel22);
            this.groupBox4.Controls.Add(this.metroLabel15);
            this.groupBox4.Controls.Add(this.capacityBox);
            this.groupBox4.Controls.Add(this.metroLabel24);
            this.groupBox4.Controls.Add(this.carcasTypeBox);
            this.groupBox4.Controls.Add(this.metroLabel25);
            this.groupBox4.Controls.Add(this.carryingBox);
            this.groupBox4.Controls.Add(this.lengthBox);
            this.groupBox4.Controls.Add(this.widthBox);
            this.panelAnimator.SetDecoration(this.groupBox4, BunifuAnimatorNS.DecorationType.None);
            this.groupBox4.Location = new System.Drawing.Point(716, 609);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(758, 266);
            this.groupBox4.TabIndex = 41;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Информация о погрузчике";
            // 
            // updateBtn
            // 
            this.updateBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.updateBtn.BackColor = System.Drawing.Color.Peru;
            this.updateBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.updateBtn.BorderRadius = 0;
            this.updateBtn.ButtonText = "Добавить обновленные данные";
            this.updateBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.updateBtn, BunifuAnimatorNS.DecorationType.None);
            this.updateBtn.DisabledColor = System.Drawing.Color.Gray;
            this.updateBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.updateBtn.Iconimage = null;
            this.updateBtn.Iconimage_right = null;
            this.updateBtn.Iconimage_right_Selected = null;
            this.updateBtn.Iconimage_Selected = null;
            this.updateBtn.IconMarginLeft = 0;
            this.updateBtn.IconMarginRight = 0;
            this.updateBtn.IconRightVisible = true;
            this.updateBtn.IconRightZoom = 0D;
            this.updateBtn.IconVisible = true;
            this.updateBtn.IconZoom = 90D;
            this.updateBtn.IsTab = false;
            this.updateBtn.Location = new System.Drawing.Point(52, 198);
            this.updateBtn.Name = "updateBtn";
            this.updateBtn.Normalcolor = System.Drawing.Color.Peru;
            this.updateBtn.OnHovercolor = System.Drawing.Color.DarkOrange;
            this.updateBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.updateBtn.selected = false;
            this.updateBtn.Size = new System.Drawing.Size(241, 48);
            this.updateBtn.TabIndex = 40;
            this.updateBtn.Text = "Добавить обновленные данные";
            this.updateBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.updateBtn.Textcolor = System.Drawing.Color.White;
            this.updateBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateBtn.Click += new System.EventHandler(this.updateBtn_Click);
            // 
            // transportPriceBox
            // 
            this.transportPriceBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.transportPriceBox, BunifuAnimatorNS.DecorationType.None);
            this.transportPriceBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.transportPriceBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.transportPriceBox.HintForeColor = System.Drawing.Color.Empty;
            this.transportPriceBox.HintText = "";
            this.transportPriceBox.isPassword = false;
            this.transportPriceBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.transportPriceBox.LineIdleColor = System.Drawing.Color.Gray;
            this.transportPriceBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.transportPriceBox.LineThickness = 3;
            this.transportPriceBox.Location = new System.Drawing.Point(543, 165);
            this.transportPriceBox.Margin = new System.Windows.Forms.Padding(4);
            this.transportPriceBox.Name = "transportPriceBox";
            this.transportPriceBox.Size = new System.Drawing.Size(192, 27);
            this.transportPriceBox.TabIndex = 39;
            this.transportPriceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.transportPriceBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.transportPriceBox_KeyPress);
            this.transportPriceBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.transportPriceBox_KeyUp);
            // 
            // metroLabel27
            // 
            this.metroLabel27.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel27, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel27.Enabled = false;
            this.metroLabel27.Location = new System.Drawing.Point(387, 177);
            this.metroLabel27.Name = "metroLabel27";
            this.metroLabel27.Size = new System.Drawing.Size(156, 19);
            this.metroLabel27.TabIndex = 38;
            this.metroLabel27.Text = "Цена тонны перегрузки";
            // 
            // metroLabel23
            // 
            this.metroLabel23.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel23, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel23.Enabled = false;
            this.metroLabel23.Location = new System.Drawing.Point(394, 112);
            this.metroLabel23.Name = "metroLabel23";
            this.metroLabel23.Size = new System.Drawing.Size(114, 19);
            this.metroLabel23.TabIndex = 30;
            this.metroLabel23.Text = "Материал кузова";
            // 
            // loadTypeBox
            // 
            this.loadTypeBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.loadTypeBox, BunifuAnimatorNS.DecorationType.None);
            this.loadTypeBox.Enabled = false;
            this.loadTypeBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.loadTypeBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.loadTypeBox.HintForeColor = System.Drawing.Color.Empty;
            this.loadTypeBox.HintText = "";
            this.loadTypeBox.isPassword = false;
            this.loadTypeBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.loadTypeBox.LineIdleColor = System.Drawing.Color.Gray;
            this.loadTypeBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.loadTypeBox.LineThickness = 3;
            this.loadTypeBox.Location = new System.Drawing.Point(515, 132);
            this.loadTypeBox.Margin = new System.Windows.Forms.Padding(4);
            this.loadTypeBox.Name = "loadTypeBox";
            this.loadTypeBox.Size = new System.Drawing.Size(192, 27);
            this.loadTypeBox.TabIndex = 33;
            this.loadTypeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel21
            // 
            this.metroLabel21.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel21, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel21.Enabled = false;
            this.metroLabel21.Location = new System.Drawing.Point(33, 43);
            this.metroLabel21.Name = "metroLabel21";
            this.metroLabel21.Size = new System.Drawing.Size(141, 19);
            this.metroLabel21.TabIndex = 22;
            this.metroLabel21.Text = "Тип грузового кузова";
            // 
            // carcasMaterialBox
            // 
            this.carcasMaterialBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.carcasMaterialBox, BunifuAnimatorNS.DecorationType.None);
            this.carcasMaterialBox.Enabled = false;
            this.carcasMaterialBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.carcasMaterialBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.carcasMaterialBox.HintForeColor = System.Drawing.Color.Empty;
            this.carcasMaterialBox.HintText = "";
            this.carcasMaterialBox.isPassword = false;
            this.carcasMaterialBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.carcasMaterialBox.LineIdleColor = System.Drawing.Color.Gray;
            this.carcasMaterialBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.carcasMaterialBox.LineThickness = 3;
            this.carcasMaterialBox.Location = new System.Drawing.Point(515, 100);
            this.carcasMaterialBox.Margin = new System.Windows.Forms.Padding(4);
            this.carcasMaterialBox.Name = "carcasMaterialBox";
            this.carcasMaterialBox.Size = new System.Drawing.Size(192, 27);
            this.carcasMaterialBox.TabIndex = 35;
            this.carcasMaterialBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel18
            // 
            this.metroLabel18.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel18, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel18.Enabled = false;
            this.metroLabel18.Location = new System.Drawing.Point(88, 72);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(86, 19);
            this.metroLabel18.TabIndex = 21;
            this.metroLabel18.Text = "Вместимость";
            // 
            // heighBox
            // 
            this.heighBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.heighBox, BunifuAnimatorNS.DecorationType.None);
            this.heighBox.Enabled = false;
            this.heighBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.heighBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.heighBox.HintForeColor = System.Drawing.Color.Empty;
            this.heighBox.HintText = "";
            this.heighBox.isPassword = false;
            this.heighBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.heighBox.LineIdleColor = System.Drawing.Color.Gray;
            this.heighBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.heighBox.LineThickness = 3;
            this.heighBox.Location = new System.Drawing.Point(515, 36);
            this.heighBox.Margin = new System.Windows.Forms.Padding(4);
            this.heighBox.Name = "heighBox";
            this.heighBox.Size = new System.Drawing.Size(192, 27);
            this.heighBox.TabIndex = 36;
            this.heighBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel17
            // 
            this.metroLabel17.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel17, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel17.Enabled = false;
            this.metroLabel17.Location = new System.Drawing.Point(51, 107);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(123, 19);
            this.metroLabel17.TabIndex = 20;
            this.metroLabel17.Text = "Грузоподъемность";
            // 
            // amountBox
            // 
            this.amountBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.amountBox, BunifuAnimatorNS.DecorationType.None);
            this.amountBox.Enabled = false;
            this.amountBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.amountBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.amountBox.HintForeColor = System.Drawing.Color.Empty;
            this.amountBox.HintText = "";
            this.amountBox.isPassword = false;
            this.amountBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.amountBox.LineIdleColor = System.Drawing.Color.Gray;
            this.amountBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.amountBox.LineThickness = 3;
            this.amountBox.Location = new System.Drawing.Point(515, 68);
            this.amountBox.Margin = new System.Windows.Forms.Padding(4);
            this.amountBox.Name = "amountBox";
            this.amountBox.Size = new System.Drawing.Size(192, 27);
            this.amountBox.TabIndex = 37;
            this.amountBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel16
            // 
            this.metroLabel16.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel16, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel16.Enabled = false;
            this.metroLabel16.Location = new System.Drawing.Point(110, 139);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(64, 19);
            this.metroLabel16.TabIndex = 19;
            this.metroLabel16.Text = "Длина, м";
            // 
            // metroLabel22
            // 
            this.metroLabel22.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel22, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel22.Enabled = false;
            this.metroLabel22.Location = new System.Drawing.Point(387, 144);
            this.metroLabel22.Name = "metroLabel22";
            this.metroLabel22.Size = new System.Drawing.Size(121, 19);
            this.metroLabel22.TabIndex = 29;
            this.metroLabel22.Text = "Способы загрузки";
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel15, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel15.Location = new System.Drawing.Point(98, 165);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(76, 19);
            this.metroLabel15.TabIndex = 23;
            this.metroLabel15.Text = "Ширина, м";
            // 
            // capacityBox
            // 
            this.capacityBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.capacityBox, BunifuAnimatorNS.DecorationType.None);
            this.capacityBox.Enabled = false;
            this.capacityBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.capacityBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.capacityBox.HintForeColor = System.Drawing.Color.Empty;
            this.capacityBox.HintText = "";
            this.capacityBox.isPassword = false;
            this.capacityBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.capacityBox.LineIdleColor = System.Drawing.Color.Gray;
            this.capacityBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.capacityBox.LineThickness = 3;
            this.capacityBox.Location = new System.Drawing.Point(182, 65);
            this.capacityBox.Margin = new System.Windows.Forms.Padding(4);
            this.capacityBox.Name = "capacityBox";
            this.capacityBox.Size = new System.Drawing.Size(192, 27);
            this.capacityBox.TabIndex = 28;
            this.capacityBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel24
            // 
            this.metroLabel24.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel24, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel24.Enabled = false;
            this.metroLabel24.Location = new System.Drawing.Point(433, 77);
            this.metroLabel24.Name = "metroLabel24";
            this.metroLabel24.Size = new System.Drawing.Size(75, 19);
            this.metroLabel24.TabIndex = 31;
            this.metroLabel24.Text = "Объем, м3";
            // 
            // carcasTypeBox
            // 
            this.carcasTypeBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.carcasTypeBox, BunifuAnimatorNS.DecorationType.None);
            this.carcasTypeBox.Enabled = false;
            this.carcasTypeBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.carcasTypeBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.carcasTypeBox.HintForeColor = System.Drawing.Color.Empty;
            this.carcasTypeBox.HintText = "";
            this.carcasTypeBox.isPassword = false;
            this.carcasTypeBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.carcasTypeBox.LineIdleColor = System.Drawing.Color.Gray;
            this.carcasTypeBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.carcasTypeBox.LineThickness = 3;
            this.carcasTypeBox.Location = new System.Drawing.Point(182, 35);
            this.carcasTypeBox.Margin = new System.Windows.Forms.Padding(4);
            this.carcasTypeBox.Name = "carcasTypeBox";
            this.carcasTypeBox.Size = new System.Drawing.Size(192, 27);
            this.carcasTypeBox.TabIndex = 27;
            this.carcasTypeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel25
            // 
            this.metroLabel25.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel25, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel25.Enabled = false;
            this.metroLabel25.Location = new System.Drawing.Point(440, 48);
            this.metroLabel25.Name = "metroLabel25";
            this.metroLabel25.Size = new System.Drawing.Size(68, 19);
            this.metroLabel25.TabIndex = 32;
            this.metroLabel25.Text = "Высота, м";
            // 
            // carryingBox
            // 
            this.carryingBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.carryingBox, BunifuAnimatorNS.DecorationType.None);
            this.carryingBox.Enabled = false;
            this.carryingBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.carryingBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.carryingBox.HintForeColor = System.Drawing.Color.Empty;
            this.carryingBox.HintText = "";
            this.carryingBox.isPassword = false;
            this.carryingBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.carryingBox.LineIdleColor = System.Drawing.Color.Gray;
            this.carryingBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.carryingBox.LineThickness = 3;
            this.carryingBox.Location = new System.Drawing.Point(182, 95);
            this.carryingBox.Margin = new System.Windows.Forms.Padding(4);
            this.carryingBox.Name = "carryingBox";
            this.carryingBox.Size = new System.Drawing.Size(192, 27);
            this.carryingBox.TabIndex = 26;
            this.carryingBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // lengthBox
            // 
            this.lengthBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.lengthBox, BunifuAnimatorNS.DecorationType.None);
            this.lengthBox.Enabled = false;
            this.lengthBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.lengthBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lengthBox.HintForeColor = System.Drawing.Color.Empty;
            this.lengthBox.HintText = "";
            this.lengthBox.isPassword = false;
            this.lengthBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.lengthBox.LineIdleColor = System.Drawing.Color.Gray;
            this.lengthBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.lengthBox.LineThickness = 3;
            this.lengthBox.Location = new System.Drawing.Point(182, 125);
            this.lengthBox.Margin = new System.Windows.Forms.Padding(4);
            this.lengthBox.Name = "lengthBox";
            this.lengthBox.Size = new System.Drawing.Size(192, 27);
            this.lengthBox.TabIndex = 24;
            this.lengthBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // widthBox
            // 
            this.widthBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.widthBox, BunifuAnimatorNS.DecorationType.None);
            this.widthBox.Enabled = false;
            this.widthBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.widthBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.widthBox.HintForeColor = System.Drawing.Color.Empty;
            this.widthBox.HintText = "";
            this.widthBox.isPassword = false;
            this.widthBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.widthBox.LineIdleColor = System.Drawing.Color.Gray;
            this.widthBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.widthBox.LineThickness = 3;
            this.widthBox.Location = new System.Drawing.Point(182, 155);
            this.widthBox.Margin = new System.Windows.Forms.Padding(4);
            this.widthBox.Name = "widthBox";
            this.widthBox.Size = new System.Drawing.Size(192, 27);
            this.widthBox.TabIndex = 25;
            this.widthBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.metroLabel26);
            this.groupBox3.Controls.Add(this.bunifuFlatButton7);
            this.groupBox3.Controls.Add(this.addGoodsBtn);
            this.groupBox3.Controls.Add(this.imgList);
            this.groupBox3.Controls.Add(this.metroLabel20);
            this.groupBox3.Controls.Add(this.bunifuFlatButton6);
            this.groupBox3.Controls.Add(this.metroDateTime1);
            this.groupBox3.Controls.Add(this.metroLabel19);
            this.groupBox3.Controls.Add(this.metroLabel14);
            this.groupBox3.Controls.Add(this.metroLabel28);
            this.groupBox3.Controls.Add(this.metroLabel29);
            this.groupBox3.Controls.Add(this.metroLabel12);
            this.groupBox3.Controls.Add(this.metroLabel11);
            this.groupBox3.Controls.Add(this.transportSumBox);
            this.groupBox3.Controls.Add(this.cubageBox);
            this.groupBox3.Controls.Add(this.weightBox);
            this.groupBox3.Controls.Add(this.goodsNameBox);
            this.groupBox3.Controls.Add(this.goodsTypeBox);
            this.panelAnimator.SetDecoration(this.groupBox3, BunifuAnimatorNS.DecorationType.None);
            this.groupBox3.Location = new System.Drawing.Point(720, 224);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(4741, 5195);
            this.groupBox3.TabIndex = 64;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Товары";
            // 
            // metroLabel26
            // 
            this.metroLabel26.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel26, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel26.Location = new System.Drawing.Point(1639, 1440);
            this.metroLabel26.Name = "metroLabel26";
            this.metroLabel26.Size = new System.Drawing.Size(142, 19);
            this.metroLabel26.TabIndex = 29;
            this.metroLabel26.Text = "Стоимость перевозки";
            // 
            // bunifuFlatButton7
            // 
            this.bunifuFlatButton7.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton7.BorderRadius = 0;
            this.bunifuFlatButton7.ButtonText = "Обновить данные";
            this.bunifuFlatButton7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.bunifuFlatButton7, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton7.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton7.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton7.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton7.Iconimage")));
            this.bunifuFlatButton7.Iconimage_right = null;
            this.bunifuFlatButton7.Iconimage_right_Selected = null;
            this.bunifuFlatButton7.Iconimage_Selected = null;
            this.bunifuFlatButton7.IconMarginLeft = 0;
            this.bunifuFlatButton7.IconMarginRight = 0;
            this.bunifuFlatButton7.IconRightVisible = true;
            this.bunifuFlatButton7.IconRightZoom = 0D;
            this.bunifuFlatButton7.IconVisible = true;
            this.bunifuFlatButton7.IconZoom = 90D;
            this.bunifuFlatButton7.IsTab = false;
            this.bunifuFlatButton7.Location = new System.Drawing.Point(8, 603);
            this.bunifuFlatButton7.Name = "bunifuFlatButton7";
            this.bunifuFlatButton7.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton7.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.bunifuFlatButton7.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton7.selected = false;
            this.bunifuFlatButton7.Size = new System.Drawing.Size(241, 48);
            this.bunifuFlatButton7.TabIndex = 27;
            this.bunifuFlatButton7.Text = "Обновить данные";
            this.bunifuFlatButton7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton7.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton7.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // addGoodsBtn
            // 
            this.addGoodsBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.addGoodsBtn.BackColor = System.Drawing.Color.Peru;
            this.addGoodsBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.addGoodsBtn.BorderRadius = 0;
            this.addGoodsBtn.ButtonText = "Изменить список товаров";
            this.addGoodsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.addGoodsBtn, BunifuAnimatorNS.DecorationType.None);
            this.addGoodsBtn.DisabledColor = System.Drawing.Color.Gray;
            this.addGoodsBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.addGoodsBtn.Iconimage = null;
            this.addGoodsBtn.Iconimage_right = null;
            this.addGoodsBtn.Iconimage_right_Selected = null;
            this.addGoodsBtn.Iconimage_Selected = null;
            this.addGoodsBtn.IconMarginLeft = 0;
            this.addGoodsBtn.IconMarginRight = 0;
            this.addGoodsBtn.IconRightVisible = true;
            this.addGoodsBtn.IconRightZoom = 0D;
            this.addGoodsBtn.IconVisible = true;
            this.addGoodsBtn.IconZoom = 90D;
            this.addGoodsBtn.IsTab = false;
            this.addGoodsBtn.Location = new System.Drawing.Point(449, 248);
            this.addGoodsBtn.Name = "addGoodsBtn";
            this.addGoodsBtn.Normalcolor = System.Drawing.Color.Peru;
            this.addGoodsBtn.OnHovercolor = System.Drawing.Color.DarkOrange;
            this.addGoodsBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.addGoodsBtn.selected = false;
            this.addGoodsBtn.Size = new System.Drawing.Size(296, 48);
            this.addGoodsBtn.TabIndex = 24;
            this.addGoodsBtn.Text = "Изменить список товаров";
            this.addGoodsBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.addGoodsBtn.Textcolor = System.Drawing.Color.White;
            this.addGoodsBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addGoodsBtn.Click += new System.EventHandler(this.addGoodsBtn_Click);
            // 
            // imgList
            // 
            this.imgList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.imgList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelAnimator.SetDecoration(this.imgList, BunifuAnimatorNS.DecorationType.None);
            this.imgList.ImageLocation = "";
            this.imgList.Location = new System.Drawing.Point(172, 77);
            this.imgList.Name = "imgList";
            this.imgList.Size = new System.Drawing.Size(241, 302);
            this.imgList.TabIndex = 23;
            this.imgList.TabStop = false;
            // 
            // metroLabel20
            // 
            this.metroLabel20.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel20, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel20.Location = new System.Drawing.Point(447, 158);
            this.metroLabel20.Name = "metroLabel20";
            this.metroLabel20.Size = new System.Drawing.Size(37, 19);
            this.metroLabel20.TabIndex = 22;
            this.metroLabel20.Text = "Дата";
            // 
            // bunifuFlatButton6
            // 
            this.bunifuFlatButton6.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton6.BackColor = System.Drawing.Color.Peru;
            this.bunifuFlatButton6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton6.BorderRadius = 0;
            this.bunifuFlatButton6.ButtonText = "Выбрать тип погрузчика";
            this.bunifuFlatButton6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.bunifuFlatButton6, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton6.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton6.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton6.Iconimage = null;
            this.bunifuFlatButton6.Iconimage_right = null;
            this.bunifuFlatButton6.Iconimage_right_Selected = null;
            this.bunifuFlatButton6.Iconimage_Selected = null;
            this.bunifuFlatButton6.IconMarginLeft = 0;
            this.bunifuFlatButton6.IconMarginRight = 0;
            this.bunifuFlatButton6.IconRightVisible = true;
            this.bunifuFlatButton6.IconRightZoom = 0D;
            this.bunifuFlatButton6.IconVisible = true;
            this.bunifuFlatButton6.IconZoom = 90D;
            this.bunifuFlatButton6.IsTab = false;
            this.bunifuFlatButton6.Location = new System.Drawing.Point(449, 194);
            this.bunifuFlatButton6.Name = "bunifuFlatButton6";
            this.bunifuFlatButton6.Normalcolor = System.Drawing.Color.Peru;
            this.bunifuFlatButton6.OnHovercolor = System.Drawing.Color.DarkOrange;
            this.bunifuFlatButton6.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton6.selected = false;
            this.bunifuFlatButton6.Size = new System.Drawing.Size(296, 48);
            this.bunifuFlatButton6.TabIndex = 19;
            this.bunifuFlatButton6.Text = "Выбрать тип погрузчика";
            this.bunifuFlatButton6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton6.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton6.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton6.Click += new System.EventHandler(this.bunifuFlatButton6_Click);
            // 
            // metroDateTime1
            // 
            this.panelAnimator.SetDecoration(this.metroDateTime1, BunifuAnimatorNS.DecorationType.None);
            this.metroDateTime1.Location = new System.Drawing.Point(490, 149);
            this.metroDateTime1.MinimumSize = new System.Drawing.Size(0, 29);
            this.metroDateTime1.Name = "metroDateTime1";
            this.metroDateTime1.Size = new System.Drawing.Size(253, 29);
            this.metroDateTime1.TabIndex = 21;
            // 
            // metroLabel19
            // 
            this.metroLabel19.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel19, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel19.Location = new System.Drawing.Point(449, 116);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(32, 19);
            this.metroLabel19.TabIndex = 20;
            this.metroLabel19.Text = "Тип";
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel14, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel14.Location = new System.Drawing.Point(48, 84);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(108, 19);
            this.metroLabel14.TabIndex = 18;
            this.metroLabel14.Text = "Список товаров";
            // 
            // metroLabel28
            // 
            this.metroLabel28.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel28, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel28.Location = new System.Drawing.Point(451, 309);
            this.metroLabel28.Name = "metroLabel28";
            this.metroLabel28.Size = new System.Drawing.Size(142, 19);
            this.metroLabel28.TabIndex = 18;
            this.metroLabel28.Text = "Стоимость перевозки";
            // 
            // metroLabel29
            // 
            this.metroLabel29.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel29, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel29.Location = new System.Drawing.Point(447, 78);
            this.metroLabel29.Name = "metroLabel29";
            this.metroLabel29.Size = new System.Drawing.Size(99, 19);
            this.metroLabel29.TabIndex = 18;
            this.metroLabel29.Text = "Кубатура ( м3 )";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel12, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel12.Location = new System.Drawing.Point(491, 38);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(55, 19);
            this.metroLabel12.TabIndex = 18;
            this.metroLabel12.Text = "Вес ( т )";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel11, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel11.Location = new System.Drawing.Point(8, 41);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(204, 19);
            this.metroLabel11.TabIndex = 18;
            this.metroLabel11.Text = "Наименование группы товаров";
            // 
            // transportSumBox
            // 
            this.transportSumBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.transportSumBox, BunifuAnimatorNS.DecorationType.None);
            this.transportSumBox.Enabled = false;
            this.transportSumBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.transportSumBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.transportSumBox.HintForeColor = System.Drawing.Color.Empty;
            this.transportSumBox.HintText = "";
            this.transportSumBox.isPassword = false;
            this.transportSumBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.transportSumBox.LineIdleColor = System.Drawing.Color.Gray;
            this.transportSumBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.transportSumBox.LineThickness = 3;
            this.transportSumBox.Location = new System.Drawing.Point(610, 299);
            this.transportSumBox.Margin = new System.Windows.Forms.Padding(4);
            this.transportSumBox.Name = "transportSumBox";
            this.transportSumBox.Size = new System.Drawing.Size(129, 29);
            this.transportSumBox.TabIndex = 17;
            this.transportSumBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cubageBox
            // 
            this.cubageBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.cubageBox, BunifuAnimatorNS.DecorationType.None);
            this.cubageBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.cubageBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cubageBox.HintForeColor = System.Drawing.Color.Empty;
            this.cubageBox.HintText = "";
            this.cubageBox.isPassword = false;
            this.cubageBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.cubageBox.LineIdleColor = System.Drawing.Color.Gray;
            this.cubageBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.cubageBox.LineThickness = 3;
            this.cubageBox.Location = new System.Drawing.Point(553, 65);
            this.cubageBox.Margin = new System.Windows.Forms.Padding(4);
            this.cubageBox.Name = "cubageBox";
            this.cubageBox.Size = new System.Drawing.Size(78, 29);
            this.cubageBox.TabIndex = 17;
            this.cubageBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.cubageBox.OnValueChanged += new System.EventHandler(this.weightBox_OnValueChanged);
            this.cubageBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cubageBox_KeyPress);
            this.cubageBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.weightBox_KeyUp);
            // 
            // weightBox
            // 
            this.weightBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.weightBox, BunifuAnimatorNS.DecorationType.None);
            this.weightBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.weightBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.weightBox.HintForeColor = System.Drawing.Color.Empty;
            this.weightBox.HintText = "";
            this.weightBox.isPassword = false;
            this.weightBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.weightBox.LineIdleColor = System.Drawing.Color.Gray;
            this.weightBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.weightBox.LineThickness = 3;
            this.weightBox.Location = new System.Drawing.Point(553, 31);
            this.weightBox.Margin = new System.Windows.Forms.Padding(4);
            this.weightBox.Name = "weightBox";
            this.weightBox.Size = new System.Drawing.Size(78, 29);
            this.weightBox.TabIndex = 17;
            this.weightBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.weightBox.OnValueChanged += new System.EventHandler(this.weightBox_OnValueChanged);
            this.weightBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.weightBox_KeyPress);
            this.weightBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.weightBox_KeyUp);
            // 
            // goodsNameBox
            // 
            this.goodsNameBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.goodsNameBox, BunifuAnimatorNS.DecorationType.None);
            this.goodsNameBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.goodsNameBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.goodsNameBox.HintForeColor = System.Drawing.Color.Empty;
            this.goodsNameBox.HintText = "";
            this.goodsNameBox.isPassword = false;
            this.goodsNameBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.goodsNameBox.LineIdleColor = System.Drawing.Color.Gray;
            this.goodsNameBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.goodsNameBox.LineThickness = 3;
            this.goodsNameBox.Location = new System.Drawing.Point(212, 28);
            this.goodsNameBox.Margin = new System.Windows.Forms.Padding(4);
            this.goodsNameBox.Name = "goodsNameBox";
            this.goodsNameBox.Size = new System.Drawing.Size(201, 29);
            this.goodsNameBox.TabIndex = 17;
            this.goodsNameBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // goodsTypeBox
            // 
            this.panelAnimator.SetDecoration(this.goodsTypeBox, BunifuAnimatorNS.DecorationType.None);
            this.goodsTypeBox.FormattingEnabled = true;
            this.goodsTypeBox.ItemHeight = 23;
            this.goodsTypeBox.Items.AddRange(new object[] {
            "Продовольственные товары",
            "Электронные товары",
            "Товары с температурным режимом",
            "Строительные материалы",
            "Сыпучие"});
            this.goodsTypeBox.Location = new System.Drawing.Point(490, 113);
            this.goodsTypeBox.Name = "goodsTypeBox";
            this.goodsTypeBox.Size = new System.Drawing.Size(253, 29);
            this.goodsTypeBox.TabIndex = 16;
            this.goodsTypeBox.Tag = "Тип товаров";
            this.goodsTypeBox.UseSelectable = true;
            this.goodsTypeBox.SelectionChangeCommitted += new System.EventHandler(this.goodsTypeBox_SelectionChangeCommitted);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.idBox);
            this.groupBox1.Controls.Add(this.fromPhoneBox);
            this.groupBox1.Controls.Add(this.fromEmailBox);
            this.groupBox1.Controls.Add(this.fromCityBox);
            this.groupBox1.Controls.Add(this.companyFromBox);
            this.groupBox1.Controls.Add(this.fromAdresBox);
            this.groupBox1.Controls.Add(this.metroLabel5);
            this.groupBox1.Controls.Add(this.metroLabel4);
            this.groupBox1.Controls.Add(this.metroLabel3);
            this.groupBox1.Controls.Add(this.metroLabel2);
            this.groupBox1.Controls.Add(this.metroLabel1);
            this.panelAnimator.SetDecoration(this.groupBox1, BunifuAnimatorNS.DecorationType.None);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(714, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(384, 206);
            this.groupBox1.TabIndex = 63;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Отправитель";
            // 
            // idBox
            // 
            this.idBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.idBox, BunifuAnimatorNS.DecorationType.None);
            this.idBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.idBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.idBox.HintForeColor = System.Drawing.Color.Empty;
            this.idBox.HintText = "";
            this.idBox.isPassword = false;
            this.idBox.LineFocusedColor = System.Drawing.Color.Blue;
            this.idBox.LineIdleColor = System.Drawing.Color.Gray;
            this.idBox.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.idBox.LineThickness = 3;
            this.idBox.Location = new System.Drawing.Point(7, 62);
            this.idBox.Margin = new System.Windows.Forms.Padding(4);
            this.idBox.Name = "idBox";
            this.idBox.Size = new System.Drawing.Size(36, 33);
            this.idBox.TabIndex = 18;
            this.idBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.idBox.Visible = false;
            // 
            // fromPhoneBox
            // 
            this.fromPhoneBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.fromPhoneBox, BunifuAnimatorNS.DecorationType.None);
            this.fromPhoneBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.fromPhoneBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.fromPhoneBox.HintForeColor = System.Drawing.Color.Empty;
            this.fromPhoneBox.HintText = "";
            this.fromPhoneBox.isPassword = false;
            this.fromPhoneBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.fromPhoneBox.LineIdleColor = System.Drawing.Color.Gray;
            this.fromPhoneBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.fromPhoneBox.LineThickness = 3;
            this.fromPhoneBox.Location = new System.Drawing.Point(183, 119);
            this.fromPhoneBox.Margin = new System.Windows.Forms.Padding(4);
            this.fromPhoneBox.Name = "fromPhoneBox";
            this.fromPhoneBox.Size = new System.Drawing.Size(192, 27);
            this.fromPhoneBox.TabIndex = 13;
            this.fromPhoneBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.fromPhoneBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fromPhoneBox_KeyPress);
            // 
            // fromEmailBox
            // 
            this.fromEmailBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.fromEmailBox, BunifuAnimatorNS.DecorationType.None);
            this.fromEmailBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.fromEmailBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.fromEmailBox.HintForeColor = System.Drawing.Color.Empty;
            this.fromEmailBox.HintText = "";
            this.fromEmailBox.isPassword = false;
            this.fromEmailBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.fromEmailBox.LineIdleColor = System.Drawing.Color.Gray;
            this.fromEmailBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.fromEmailBox.LineThickness = 3;
            this.fromEmailBox.Location = new System.Drawing.Point(183, 149);
            this.fromEmailBox.Margin = new System.Windows.Forms.Padding(4);
            this.fromEmailBox.Name = "fromEmailBox";
            this.fromEmailBox.Size = new System.Drawing.Size(192, 27);
            this.fromEmailBox.TabIndex = 14;
            this.fromEmailBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // fromCityBox
            // 
            this.fromCityBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.fromCityBox, BunifuAnimatorNS.DecorationType.None);
            this.fromCityBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.fromCityBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.fromCityBox.HintForeColor = System.Drawing.Color.Empty;
            this.fromCityBox.HintText = "";
            this.fromCityBox.isPassword = false;
            this.fromCityBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.fromCityBox.LineIdleColor = System.Drawing.Color.Gray;
            this.fromCityBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.fromCityBox.LineThickness = 3;
            this.fromCityBox.Location = new System.Drawing.Point(183, 89);
            this.fromCityBox.Margin = new System.Windows.Forms.Padding(4);
            this.fromCityBox.Name = "fromCityBox";
            this.fromCityBox.Size = new System.Drawing.Size(192, 27);
            this.fromCityBox.TabIndex = 15;
            this.fromCityBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // companyFromBox
            // 
            this.companyFromBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.companyFromBox, BunifuAnimatorNS.DecorationType.None);
            this.companyFromBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.companyFromBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.companyFromBox.HintForeColor = System.Drawing.Color.Empty;
            this.companyFromBox.HintText = "";
            this.companyFromBox.isPassword = false;
            this.companyFromBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.companyFromBox.LineIdleColor = System.Drawing.Color.Gray;
            this.companyFromBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.companyFromBox.LineThickness = 3;
            this.companyFromBox.Location = new System.Drawing.Point(183, 29);
            this.companyFromBox.Margin = new System.Windows.Forms.Padding(4);
            this.companyFromBox.Name = "companyFromBox";
            this.companyFromBox.Size = new System.Drawing.Size(192, 27);
            this.companyFromBox.TabIndex = 1;
            this.companyFromBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // fromAdresBox
            // 
            this.fromAdresBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.fromAdresBox, BunifuAnimatorNS.DecorationType.None);
            this.fromAdresBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.fromAdresBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.fromAdresBox.HintForeColor = System.Drawing.Color.Empty;
            this.fromAdresBox.HintText = "";
            this.fromAdresBox.isPassword = false;
            this.fromAdresBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.fromAdresBox.LineIdleColor = System.Drawing.Color.Gray;
            this.fromAdresBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.fromAdresBox.LineThickness = 3;
            this.fromAdresBox.Location = new System.Drawing.Point(183, 59);
            this.fromAdresBox.Margin = new System.Windows.Forms.Padding(4);
            this.fromAdresBox.Name = "fromAdresBox";
            this.fromAdresBox.Size = new System.Drawing.Size(192, 27);
            this.fromAdresBox.TabIndex = 17;
            this.fromAdresBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel5, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel5.Location = new System.Drawing.Point(135, 157);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(41, 19);
            this.metroLabel5.TabIndex = 12;
            this.metroLabel5.Text = "Email";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel4, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel4.Enabled = false;
            this.metroLabel4.Location = new System.Drawing.Point(106, 127);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(70, 19);
            this.metroLabel4.TabIndex = 8;
            this.metroLabel4.Text = "Телефоон";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel3, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel3.Enabled = false;
            this.metroLabel3.Location = new System.Drawing.Point(130, 97);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(46, 19);
            this.metroLabel3.TabIndex = 9;
            this.metroLabel3.Text = "Город";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel2, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel2.Enabled = false;
            this.metroLabel2.Location = new System.Drawing.Point(130, 67);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(46, 19);
            this.metroLabel2.TabIndex = 10;
            this.metroLabel2.Text = "Адрес";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel1, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel1.Enabled = false;
            this.metroLabel1.Location = new System.Drawing.Point(6, 37);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(170, 19);
            this.metroLabel1.TabIndex = 11;
            this.metroLabel1.Text = "Отправляющая компания";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.toPhoneBox);
            this.groupBox2.Controls.Add(this.toEmailBox);
            this.groupBox2.Controls.Add(this.toCityBox);
            this.groupBox2.Controls.Add(this.companyToBox);
            this.groupBox2.Controls.Add(this.toAdresBox);
            this.groupBox2.Controls.Add(this.metroLabel6);
            this.groupBox2.Controls.Add(this.metroLabel7);
            this.groupBox2.Controls.Add(this.metroLabel8);
            this.groupBox2.Controls.Add(this.metroLabel9);
            this.groupBox2.Controls.Add(this.metroLabel10);
            this.panelAnimator.SetDecoration(this.groupBox2, BunifuAnimatorNS.DecorationType.None);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(1110, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(384, 212);
            this.groupBox2.TabIndex = 62;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Получатель";
            // 
            // toPhoneBox
            // 
            this.toPhoneBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.toPhoneBox, BunifuAnimatorNS.DecorationType.None);
            this.toPhoneBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.toPhoneBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.toPhoneBox.HintForeColor = System.Drawing.Color.Empty;
            this.toPhoneBox.HintText = "";
            this.toPhoneBox.isPassword = false;
            this.toPhoneBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.toPhoneBox.LineIdleColor = System.Drawing.Color.Gray;
            this.toPhoneBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.toPhoneBox.LineThickness = 3;
            this.toPhoneBox.Location = new System.Drawing.Point(172, 127);
            this.toPhoneBox.Margin = new System.Windows.Forms.Padding(4);
            this.toPhoneBox.Name = "toPhoneBox";
            this.toPhoneBox.Size = new System.Drawing.Size(192, 27);
            this.toPhoneBox.TabIndex = 18;
            this.toPhoneBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.toPhoneBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toPhoneBox_KeyPress);
            // 
            // toEmailBox
            // 
            this.toEmailBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.toEmailBox, BunifuAnimatorNS.DecorationType.None);
            this.toEmailBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.toEmailBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.toEmailBox.HintForeColor = System.Drawing.Color.Empty;
            this.toEmailBox.HintText = "";
            this.toEmailBox.isPassword = false;
            this.toEmailBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.toEmailBox.LineIdleColor = System.Drawing.Color.Gray;
            this.toEmailBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.toEmailBox.LineThickness = 3;
            this.toEmailBox.Location = new System.Drawing.Point(172, 159);
            this.toEmailBox.Margin = new System.Windows.Forms.Padding(4);
            this.toEmailBox.Name = "toEmailBox";
            this.toEmailBox.Size = new System.Drawing.Size(192, 27);
            this.toEmailBox.TabIndex = 19;
            this.toEmailBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // toCityBox
            // 
            this.toCityBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.toCityBox, BunifuAnimatorNS.DecorationType.None);
            this.toCityBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.toCityBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.toCityBox.HintForeColor = System.Drawing.Color.Empty;
            this.toCityBox.HintText = "";
            this.toCityBox.isPassword = false;
            this.toCityBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.toCityBox.LineIdleColor = System.Drawing.Color.Gray;
            this.toCityBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.toCityBox.LineThickness = 3;
            this.toCityBox.Location = new System.Drawing.Point(172, 95);
            this.toCityBox.Margin = new System.Windows.Forms.Padding(4);
            this.toCityBox.Name = "toCityBox";
            this.toCityBox.Size = new System.Drawing.Size(192, 27);
            this.toCityBox.TabIndex = 20;
            this.toCityBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // companyToBox
            // 
            this.companyToBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.companyToBox, BunifuAnimatorNS.DecorationType.None);
            this.companyToBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.companyToBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.companyToBox.HintForeColor = System.Drawing.Color.Empty;
            this.companyToBox.HintText = "";
            this.companyToBox.isPassword = false;
            this.companyToBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.companyToBox.LineIdleColor = System.Drawing.Color.Gray;
            this.companyToBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.companyToBox.LineThickness = 3;
            this.companyToBox.Location = new System.Drawing.Point(172, 31);
            this.companyToBox.Margin = new System.Windows.Forms.Padding(4);
            this.companyToBox.Name = "companyToBox";
            this.companyToBox.Size = new System.Drawing.Size(192, 27);
            this.companyToBox.TabIndex = 21;
            this.companyToBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // toAdresBox
            // 
            this.toAdresBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.toAdresBox, BunifuAnimatorNS.DecorationType.None);
            this.toAdresBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.toAdresBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.toAdresBox.HintForeColor = System.Drawing.Color.Empty;
            this.toAdresBox.HintText = "";
            this.toAdresBox.isPassword = false;
            this.toAdresBox.LineFocusedColor = System.Drawing.Color.Peru;
            this.toAdresBox.LineIdleColor = System.Drawing.Color.Gray;
            this.toAdresBox.LineMouseHoverColor = System.Drawing.Color.DarkOrange;
            this.toAdresBox.LineThickness = 3;
            this.toAdresBox.Location = new System.Drawing.Point(172, 63);
            this.toAdresBox.Margin = new System.Windows.Forms.Padding(4);
            this.toAdresBox.Name = "toAdresBox";
            this.toAdresBox.Size = new System.Drawing.Size(192, 27);
            this.toAdresBox.TabIndex = 22;
            this.toAdresBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel6, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel6.Location = new System.Drawing.Point(124, 166);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(41, 19);
            this.metroLabel6.TabIndex = 17;
            this.metroLabel6.Text = "Email";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel7, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel7.Enabled = false;
            this.metroLabel7.Location = new System.Drawing.Point(95, 136);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(70, 19);
            this.metroLabel7.TabIndex = 13;
            this.metroLabel7.Text = "Телефоон";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel8, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel8.Enabled = false;
            this.metroLabel8.Location = new System.Drawing.Point(119, 106);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(46, 19);
            this.metroLabel8.TabIndex = 14;
            this.metroLabel8.Text = "Город";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel9, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel9.Enabled = false;
            this.metroLabel9.Location = new System.Drawing.Point(113, 76);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(52, 19);
            this.metroLabel9.TabIndex = 15;
            this.metroLabel9.Text = "Адресс";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel10, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel10.Enabled = false;
            this.metroLabel10.Location = new System.Drawing.Point(11, 46);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(154, 19);
            this.metroLabel10.TabIndex = 16;
            this.metroLabel10.Text = "Получающая компания";
            // 
            // metroGrid1
            // 
            this.metroGrid1.AllowUserToResizeRows = false;
            this.metroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.metroGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.panelAnimator.SetDecoration(this.metroGrid1, BunifuAnimatorNS.DecorationType.None);
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid1.DefaultCellStyle = dataGridViewCellStyle2;
            this.metroGrid1.EnableHeadersVisualStyles = false;
            this.metroGrid1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.Location = new System.Drawing.Point(48, 39);
            this.metroGrid1.Name = "metroGrid1";
            this.metroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.metroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid1.Size = new System.Drawing.Size(660, 517);
            this.metroGrid1.Style = MetroFramework.MetroColorStyle.Silver;
            this.metroGrid1.TabIndex = 66;
            this.metroGrid1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.metroGrid1_CellContentClick_1);
            this.metroGrid1.MouseEnter += new System.EventHandler(this.metroGrid1_MouseEnter);
            // 
            // panelAnimator
            // 
            this.panelAnimator.AnimationType = BunifuAnimatorNS.AnimationType.Leaf;
            this.panelAnimator.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 1F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 0F;
            this.panelAnimator.DefaultAnimation = animation1;
            // 
            // redactWayBills
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1312, 737);
            this.Controls.Add(this.bunifuGradientPanel3);
            this.Controls.Add(this.backToWorkWithBills);
            this.Controls.Add(this.bunifuGradientPanel2);
            this.panelAnimator.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "redactWayBills";
            this.Text = "  Редактировать накладную";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.redactBills_FormClosing);
            this.Load += new System.EventHandler(this.redactBills_Load);
            this.bunifuGradientPanel2.ResumeLayout(false);
            this.bunifuGradientPanel2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.bunifuGradientPanel3.ResumeLayout(false);
            this.slidemenu.ResumeLayout(false);
            this.contentSlideMenu.ResumeLayout(false);
            this.metroTabControl2.ResumeLayout(false);
            this.metroTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn2)).EndInit();
            this.metroTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn22)).EndInit();
            this.metroTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn32)).EndInit();
            this.panel1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgList)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button backToWorkWithBills;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel2;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel3;
        private System.Windows.Forms.GroupBox groupBox2;
        private Bunifu.Framework.UI.BunifuMaterialTextbox toPhoneBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox toEmailBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox toCityBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox companyToBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox toAdresBox;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private System.Windows.Forms.GroupBox groupBox1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox fromPhoneBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox fromEmailBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox fromCityBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox companyFromBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox fromAdresBox;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.GroupBox groupBox3;
        private Bunifu.Framework.UI.BunifuFlatButton addGoodsBtn;
        private System.Windows.Forms.PictureBox imgList;
        private MetroFramework.Controls.MetroLabel metroLabel20;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton6;
        private MetroFramework.Controls.MetroDateTime metroDateTime1;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private Bunifu.Framework.UI.BunifuMaterialTextbox weightBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox goodsNameBox;
        private MetroFramework.Controls.MetroComboBox goodsTypeBox;
        private Bunifu.Framework.UI.BunifuGradientPanel slidemenu;
        private System.Windows.Forms.Button btnSlide;
        private BunifuAnimatorNS.BunifuTransition panelAnimator;
        private System.Windows.Forms.GroupBox groupBox4;
        private Bunifu.Framework.UI.BunifuMaterialTextbox transportPriceBox;
        private MetroFramework.Controls.MetroLabel metroLabel27;
        private MetroFramework.Controls.MetroLabel metroLabel23;
        private Bunifu.Framework.UI.BunifuMaterialTextbox loadTypeBox;
        private MetroFramework.Controls.MetroLabel metroLabel21;
        private Bunifu.Framework.UI.BunifuMaterialTextbox carcasMaterialBox;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private Bunifu.Framework.UI.BunifuMaterialTextbox heighBox;
        private MetroFramework.Controls.MetroLabel metroLabel17;
        private Bunifu.Framework.UI.BunifuMaterialTextbox amountBox;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private MetroFramework.Controls.MetroLabel metroLabel22;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private Bunifu.Framework.UI.BunifuMaterialTextbox capacityBox;
        private MetroFramework.Controls.MetroLabel metroLabel24;
        private Bunifu.Framework.UI.BunifuMaterialTextbox carcasTypeBox;
        private MetroFramework.Controls.MetroLabel metroLabel25;
        private Bunifu.Framework.UI.BunifuMaterialTextbox carryingBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox lengthBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox widthBox;
        private MetroFramework.Controls.MetroGrid metroGrid1;
        private MetroFramework.Controls.MetroPanel contentSlideMenu;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button exitBtn;
        private MetroFramework.Controls.MetroTabControl metroTabControl2;
        private MetroFramework.Controls.MetroTabPage metroTabPage4;
        private Bunifu.Framework.UI.BunifuImageButton imgBtn3;
        private Bunifu.Framework.UI.BunifuImageButton imgBtn4;
        private Bunifu.Framework.UI.BunifuImageButton imgBtn1;
        private Bunifu.Framework.UI.BunifuImageButton imgBtn2;
        private MetroFramework.Controls.MetroTabPage metroTabPage5;
        private Bunifu.Framework.UI.BunifuImageButton imgBtn21;
        private Bunifu.Framework.UI.BunifuImageButton imgBtn22;
        private MetroFramework.Controls.MetroTabPage metroTabPage6;
        private Bunifu.Framework.UI.BunifuImageButton imgBtn31;
        private Bunifu.Framework.UI.BunifuImageButton imgBtn33;
        private Bunifu.Framework.UI.BunifuImageButton imgBtn32;
        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton7;
        private Bunifu.Framework.UI.BunifuFlatButton updateBtn;
        private Bunifu.Framework.UI.BunifuMaterialTextbox idBox;
        private MetroFramework.Controls.MetroLabel metroLabel26;
        private MetroFramework.Controls.MetroLabel metroLabel28;
        private Bunifu.Framework.UI.BunifuMaterialTextbox transportSumBox;
        private MetroFramework.Controls.MetroLabel metroLabel29;
        private Bunifu.Framework.UI.BunifuMaterialTextbox cubageBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem обновитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private Bunifu.Framework.UI.BunifuMaterialTextbox priceSearchBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox typeSearchBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox dateSearchBox;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton1;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton5;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton4;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton3;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton2;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton8;
    }
}