﻿namespace AutoStorage
{
    partial class redactPurchaseBills
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(redactPurchaseBills));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelAnimator = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.bunifuGradientPanel3 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.contentSlideMenu = new MetroFramework.Controls.MetroPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.exitBtn = new System.Windows.Forms.Button();
            this.metroTabControl2 = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage4 = new MetroFramework.Controls.MetroTabPage();
            this.imgBtn3 = new Bunifu.Framework.UI.BunifuImageButton();
            this.imgBtn4 = new Bunifu.Framework.UI.BunifuImageButton();
            this.imgBtn1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.imgBtn2 = new Bunifu.Framework.UI.BunifuImageButton();
            this.metroTabPage5 = new MetroFramework.Controls.MetroTabPage();
            this.imgBtn21 = new Bunifu.Framework.UI.BunifuImageButton();
            this.imgBtn22 = new Bunifu.Framework.UI.BunifuImageButton();
            this.metroTabPage6 = new MetroFramework.Controls.MetroTabPage();
            this.imgBtn31 = new Bunifu.Framework.UI.BunifuImageButton();
            this.imgBtn33 = new Bunifu.Framework.UI.BunifuImageButton();
            this.imgBtn32 = new Bunifu.Framework.UI.BunifuImageButton();
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.loaderSliderMenu = new MetroFramework.Controls.MetroPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.metroTabControl3 = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage7 = new MetroFramework.Controls.MetroTabPage();
            this.vilBtn = new Bunifu.Framework.UI.BunifuImageButton();
            this.metroTabPage8 = new MetroFramework.Controls.MetroTabPage();
            this.kovshBtn = new Bunifu.Framework.UI.BunifuImageButton();
            this.metroTabControl4 = new MetroFramework.Controls.MetroTabControl();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.loaderTonnageBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.loaderCapacityBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.loaderTypeBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.transportPriceBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel27 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel23 = new MetroFramework.Controls.MetroLabel();
            this.loadTypeBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel21 = new MetroFramework.Controls.MetroLabel();
            this.carcasMaterialBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.heighBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel22 = new MetroFramework.Controls.MetroLabel();
            this.amountBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel24 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel25 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel29 = new MetroFramework.Controls.MetroLabel();
            this.capacityBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel30 = new MetroFramework.Controls.MetroLabel();
            this.carcasTypeBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel31 = new MetroFramework.Controls.MetroLabel();
            this.carryingBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.lengthBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.widthBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.priceSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.typeSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.updPurshaseBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.fromPhoneBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.fromEmailBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.idBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.fromCityBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.companyFromBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.fromAdresBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.toPhoneBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.toEmailBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.toCityBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.companyToBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.toAdresBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.updLoaderBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.updTransrtBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.totalDaysBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.keepingForWeekPriceBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.metroDateTime2 = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel28 = new MetroFramework.Controls.MetroLabel();
            this.cubageBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel26 = new MetroFramework.Controls.MetroLabel();
            this.keepingSumBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.updateGoodsBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.imgList = new System.Windows.Forms.PictureBox();
            this.metroLabel20 = new MetroFramework.Controls.MetroLabel();
            this.metroDateTime1 = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.weightBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.goodsNameBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.goodsTypeBox = new MetroFramework.Controls.MetroComboBox();
            this.slidemenu = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuFlatButton1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton5 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton4 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton3 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton7 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnSlide = new System.Windows.Forms.Button();
            this.metroGrid1 = new MetroFramework.Controls.MetroGrid();
            this.dateSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuGradientPanel2 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.обновитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backToWorkWithBills = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.bunifuGradientPanel3.SuspendLayout();
            this.contentSlideMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            this.metroTabControl2.SuspendLayout();
            this.metroTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn2)).BeginInit();
            this.metroTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn22)).BeginInit();
            this.metroTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn32)).BeginInit();
            this.loaderSliderMenu.SuspendLayout();
            this.panel2.SuspendLayout();
            this.metroTabControl3.SuspendLayout();
            this.metroTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vilBtn)).BeginInit();
            this.metroTabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kovshBtn)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgList)).BeginInit();
            this.slidemenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).BeginInit();
            this.bunifuGradientPanel2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelAnimator
            // 
            this.panelAnimator.AnimationType = BunifuAnimatorNS.AnimationType.Leaf;
            this.panelAnimator.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 1F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 0F;
            this.panelAnimator.DefaultAnimation = animation1;
            // 
            // bunifuGradientPanel3
            // 
            this.bunifuGradientPanel3.AutoScroll = true;
            this.bunifuGradientPanel3.BackColor = System.Drawing.Color.White;
            this.bunifuGradientPanel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel3.BackgroundImage")));
            this.bunifuGradientPanel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel3.Controls.Add(this.contentSlideMenu);
            this.bunifuGradientPanel3.Controls.Add(this.loaderSliderMenu);
            this.bunifuGradientPanel3.Controls.Add(this.groupBox6);
            this.bunifuGradientPanel3.Controls.Add(this.groupBox5);
            this.bunifuGradientPanel3.Controls.Add(this.priceSearchBox);
            this.bunifuGradientPanel3.Controls.Add(this.typeSearchBox);
            this.bunifuGradientPanel3.Controls.Add(this.updPurshaseBtn);
            this.bunifuGradientPanel3.Controls.Add(this.groupBox1);
            this.bunifuGradientPanel3.Controls.Add(this.groupBox2);
            this.bunifuGradientPanel3.Controls.Add(this.groupBox3);
            this.bunifuGradientPanel3.Controls.Add(this.slidemenu);
            this.bunifuGradientPanel3.Controls.Add(this.metroGrid1);
            this.bunifuGradientPanel3.Controls.Add(this.dateSearchBox);
            this.panelAnimator.SetDecoration(this.bunifuGradientPanel3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuGradientPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bunifuGradientPanel3.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel3.Location = new System.Drawing.Point(20, 108);
            this.bunifuGradientPanel3.Name = "bunifuGradientPanel3";
            this.bunifuGradientPanel3.Quality = 10;
            this.bunifuGradientPanel3.Size = new System.Drawing.Size(1272, 609);
            this.bunifuGradientPanel3.TabIndex = 38;
            this.bunifuGradientPanel3.MouseEnter += new System.EventHandler(this.bunifuGradientPanel3_MouseEnter);
            // 
            // contentSlideMenu
            // 
            this.contentSlideMenu.Controls.Add(this.panel1);
            this.contentSlideMenu.Controls.Add(this.metroTabControl2);
            this.contentSlideMenu.Controls.Add(this.metroTabControl1);
            this.panelAnimator.SetDecoration(this.contentSlideMenu, BunifuAnimatorNS.DecorationType.None);
            this.contentSlideMenu.HorizontalScrollbarBarColor = true;
            this.contentSlideMenu.HorizontalScrollbarHighlightOnWheel = false;
            this.contentSlideMenu.HorizontalScrollbarSize = 10;
            this.contentSlideMenu.Location = new System.Drawing.Point(60, 0);
            this.contentSlideMenu.Name = "contentSlideMenu";
            this.contentSlideMenu.Size = new System.Drawing.Size(26, 19);
            this.contentSlideMenu.TabIndex = 74;
            this.contentSlideMenu.VerticalScrollbarBarColor = true;
            this.contentSlideMenu.VerticalScrollbarHighlightOnWheel = false;
            this.contentSlideMenu.VerticalScrollbarSize = 10;
            this.contentSlideMenu.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(196)))), ((int)(((byte)(37)))));
            this.panel1.Controls.Add(this.exitBtn);
            this.panelAnimator.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.panel1.Location = new System.Drawing.Point(-1179, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1201, 30);
            this.panel1.TabIndex = 3;
            // 
            // exitBtn
            // 
            this.exitBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.exitBtn.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.exitBtn, BunifuAnimatorNS.DecorationType.None);
            this.exitBtn.FlatAppearance.BorderSize = 0;
            this.exitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exitBtn.Image = ((System.Drawing.Image)(resources.GetObject("exitBtn.Image")));
            this.exitBtn.Location = new System.Drawing.Point(1176, 6);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(21, 18);
            this.exitBtn.TabIndex = 12;
            this.exitBtn.UseVisualStyleBackColor = false;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // metroTabControl2
            // 
            this.metroTabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroTabControl2.Controls.Add(this.metroTabPage4);
            this.metroTabControl2.Controls.Add(this.metroTabPage5);
            this.metroTabControl2.Controls.Add(this.metroTabPage6);
            this.panelAnimator.SetDecoration(this.metroTabControl2, BunifuAnimatorNS.DecorationType.None);
            this.metroTabControl2.Location = new System.Drawing.Point(-965, 41);
            this.metroTabControl2.Name = "metroTabControl2";
            this.metroTabControl2.SelectedIndex = 2;
            this.metroTabControl2.Size = new System.Drawing.Size(979, 733);
            this.metroTabControl2.TabIndex = 2;
            this.metroTabControl2.UseSelectable = true;
            // 
            // metroTabPage4
            // 
            this.metroTabPage4.Controls.Add(this.imgBtn3);
            this.metroTabPage4.Controls.Add(this.imgBtn4);
            this.metroTabPage4.Controls.Add(this.imgBtn1);
            this.metroTabPage4.Controls.Add(this.imgBtn2);
            this.panelAnimator.SetDecoration(this.metroTabPage4, BunifuAnimatorNS.DecorationType.None);
            this.metroTabPage4.HorizontalScrollbarBarColor = true;
            this.metroTabPage4.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.HorizontalScrollbarSize = 10;
            this.metroTabPage4.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage4.Name = "metroTabPage4";
            this.metroTabPage4.Size = new System.Drawing.Size(971, 691);
            this.metroTabPage4.TabIndex = 0;
            this.metroTabPage4.Text = "С рефрижераторным кузовом";
            this.metroTabPage4.VerticalScrollbarBarColor = true;
            this.metroTabPage4.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.VerticalScrollbarSize = 10;
            // 
            // imgBtn3
            // 
            this.imgBtn3.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.imgBtn3, BunifuAnimatorNS.DecorationType.None);
            this.imgBtn3.Image = global::AutoStorage.Properties.Resources._3;
            this.imgBtn3.ImageActive = null;
            this.imgBtn3.InitialImage = null;
            this.imgBtn3.Location = new System.Drawing.Point(63, 344);
            this.imgBtn3.Name = "imgBtn3";
            this.imgBtn3.Size = new System.Drawing.Size(364, 344);
            this.imgBtn3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBtn3.TabIndex = 5;
            this.imgBtn3.TabStop = false;
            this.imgBtn3.Zoom = 10;
            this.imgBtn3.Click += new System.EventHandler(this.imgBtn3_Click);
            // 
            // imgBtn4
            // 
            this.imgBtn4.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.imgBtn4, BunifuAnimatorNS.DecorationType.None);
            this.imgBtn4.Image = global::AutoStorage.Properties.Resources._4;
            this.imgBtn4.ImageActive = null;
            this.imgBtn4.InitialImage = null;
            this.imgBtn4.Location = new System.Drawing.Point(495, 337);
            this.imgBtn4.Name = "imgBtn4";
            this.imgBtn4.Size = new System.Drawing.Size(362, 337);
            this.imgBtn4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBtn4.TabIndex = 6;
            this.imgBtn4.TabStop = false;
            this.imgBtn4.Zoom = 10;
            this.imgBtn4.Click += new System.EventHandler(this.imgBtn4_Click);
            // 
            // imgBtn1
            // 
            this.imgBtn1.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.imgBtn1, BunifuAnimatorNS.DecorationType.None);
            this.imgBtn1.Image = global::AutoStorage.Properties.Resources._1;
            this.imgBtn1.ImageActive = null;
            this.imgBtn1.InitialImage = null;
            this.imgBtn1.Location = new System.Drawing.Point(63, 8);
            this.imgBtn1.Name = "imgBtn1";
            this.imgBtn1.Size = new System.Drawing.Size(352, 330);
            this.imgBtn1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBtn1.TabIndex = 2;
            this.imgBtn1.TabStop = false;
            this.imgBtn1.Zoom = 10;
            this.imgBtn1.Click += new System.EventHandler(this.imgBtn1_Click);
            // 
            // imgBtn2
            // 
            this.imgBtn2.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.imgBtn2, BunifuAnimatorNS.DecorationType.None);
            this.imgBtn2.Image = global::AutoStorage.Properties.Resources._21;
            this.imgBtn2.ImageActive = null;
            this.imgBtn2.InitialImage = null;
            this.imgBtn2.Location = new System.Drawing.Point(505, 8);
            this.imgBtn2.Name = "imgBtn2";
            this.imgBtn2.Size = new System.Drawing.Size(352, 330);
            this.imgBtn2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBtn2.TabIndex = 2;
            this.imgBtn2.TabStop = false;
            this.imgBtn2.Zoom = 10;
            this.imgBtn2.Click += new System.EventHandler(this.imgBtn2_Click);
            // 
            // metroTabPage5
            // 
            this.metroTabPage5.Controls.Add(this.imgBtn21);
            this.metroTabPage5.Controls.Add(this.imgBtn22);
            this.panelAnimator.SetDecoration(this.metroTabPage5, BunifuAnimatorNS.DecorationType.None);
            this.metroTabPage5.HorizontalScrollbarBarColor = true;
            this.metroTabPage5.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.HorizontalScrollbarSize = 10;
            this.metroTabPage5.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage5.Name = "metroTabPage5";
            this.metroTabPage5.Size = new System.Drawing.Size(971, 691);
            this.metroTabPage5.TabIndex = 1;
            this.metroTabPage5.Text = "С изотермическим кузовом";
            this.metroTabPage5.VerticalScrollbarBarColor = true;
            this.metroTabPage5.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.VerticalScrollbarSize = 10;
            // 
            // imgBtn21
            // 
            this.imgBtn21.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.imgBtn21, BunifuAnimatorNS.DecorationType.None);
            this.imgBtn21.Image = global::AutoStorage.Properties.Resources._2_1;
            this.imgBtn21.ImageActive = null;
            this.imgBtn21.InitialImage = null;
            this.imgBtn21.Location = new System.Drawing.Point(87, 31);
            this.imgBtn21.Name = "imgBtn21";
            this.imgBtn21.Size = new System.Drawing.Size(352, 330);
            this.imgBtn21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBtn21.TabIndex = 7;
            this.imgBtn21.TabStop = false;
            this.imgBtn21.Zoom = 10;
            this.imgBtn21.Click += new System.EventHandler(this.imgBtn21_Click);
            // 
            // imgBtn22
            // 
            this.imgBtn22.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.imgBtn22, BunifuAnimatorNS.DecorationType.None);
            this.imgBtn22.Image = global::AutoStorage.Properties.Resources._4266_3c2ed52bf940ddf2e14a954b2851f2c7;
            this.imgBtn22.ImageActive = null;
            this.imgBtn22.InitialImage = null;
            this.imgBtn22.Location = new System.Drawing.Point(528, 31);
            this.imgBtn22.Name = "imgBtn22";
            this.imgBtn22.Size = new System.Drawing.Size(352, 330);
            this.imgBtn22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBtn22.TabIndex = 8;
            this.imgBtn22.TabStop = false;
            this.imgBtn22.Zoom = 10;
            this.imgBtn22.Click += new System.EventHandler(this.imgBtn22_Click);
            // 
            // metroTabPage6
            // 
            this.metroTabPage6.Controls.Add(this.imgBtn31);
            this.metroTabPage6.Controls.Add(this.imgBtn33);
            this.metroTabPage6.Controls.Add(this.imgBtn32);
            this.panelAnimator.SetDecoration(this.metroTabPage6, BunifuAnimatorNS.DecorationType.None);
            this.metroTabPage6.HorizontalScrollbarBarColor = true;
            this.metroTabPage6.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage6.HorizontalScrollbarSize = 10;
            this.metroTabPage6.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage6.Name = "metroTabPage6";
            this.metroTabPage6.Size = new System.Drawing.Size(971, 691);
            this.metroTabPage6.TabIndex = 2;
            this.metroTabPage6.Text = "С крытым тентованным кузовом";
            this.metroTabPage6.VerticalScrollbarBarColor = true;
            this.metroTabPage6.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage6.VerticalScrollbarSize = 10;
            // 
            // imgBtn31
            // 
            this.imgBtn31.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.imgBtn31, BunifuAnimatorNS.DecorationType.None);
            this.imgBtn31.Image = global::AutoStorage.Properties.Resources._3_1;
            this.imgBtn31.ImageActive = null;
            this.imgBtn31.InitialImage = null;
            this.imgBtn31.Location = new System.Drawing.Point(7, 22);
            this.imgBtn31.Name = "imgBtn31";
            this.imgBtn31.Size = new System.Drawing.Size(330, 330);
            this.imgBtn31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBtn31.TabIndex = 9;
            this.imgBtn31.TabStop = false;
            this.imgBtn31.Zoom = 10;
            this.imgBtn31.Click += new System.EventHandler(this.imgBtn31_Click);
            // 
            // imgBtn33
            // 
            this.imgBtn33.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.imgBtn33, BunifuAnimatorNS.DecorationType.None);
            this.imgBtn33.Image = global::AutoStorage.Properties.Resources._3_3;
            this.imgBtn33.ImageActive = null;
            this.imgBtn33.InitialImage = null;
            this.imgBtn33.Location = new System.Drawing.Point(677, 22);
            this.imgBtn33.Name = "imgBtn33";
            this.imgBtn33.Size = new System.Drawing.Size(306, 330);
            this.imgBtn33.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBtn33.TabIndex = 10;
            this.imgBtn33.TabStop = false;
            this.imgBtn33.Zoom = 10;
            this.imgBtn33.Click += new System.EventHandler(this.imgBtn33_Click);
            // 
            // imgBtn32
            // 
            this.imgBtn32.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.imgBtn32, BunifuAnimatorNS.DecorationType.None);
            this.imgBtn32.Image = global::AutoStorage.Properties.Resources._3_2;
            this.imgBtn32.ImageActive = null;
            this.imgBtn32.InitialImage = null;
            this.imgBtn32.Location = new System.Drawing.Point(354, 22);
            this.imgBtn32.Name = "imgBtn32";
            this.imgBtn32.Size = new System.Drawing.Size(306, 330);
            this.imgBtn32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBtn32.TabIndex = 10;
            this.imgBtn32.TabStop = false;
            this.imgBtn32.Zoom = 10;
            this.imgBtn32.Click += new System.EventHandler(this.imgBtn32_Click);
            // 
            // metroTabControl1
            // 
            this.panelAnimator.SetDecoration(this.metroTabControl1, BunifuAnimatorNS.DecorationType.None);
            this.metroTabControl1.Location = new System.Drawing.Point(30, 41);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.Size = new System.Drawing.Size(979, 733);
            this.metroTabControl1.TabIndex = 2;
            this.metroTabControl1.UseSelectable = true;
            // 
            // loaderSliderMenu
            // 
            this.loaderSliderMenu.Controls.Add(this.panel2);
            this.loaderSliderMenu.Controls.Add(this.metroTabControl3);
            this.loaderSliderMenu.Controls.Add(this.metroTabControl4);
            this.panelAnimator.SetDecoration(this.loaderSliderMenu, BunifuAnimatorNS.DecorationType.None);
            this.loaderSliderMenu.HorizontalScrollbarBarColor = true;
            this.loaderSliderMenu.HorizontalScrollbarHighlightOnWheel = false;
            this.loaderSliderMenu.HorizontalScrollbarSize = 10;
            this.loaderSliderMenu.Location = new System.Drawing.Point(94, 0);
            this.loaderSliderMenu.Name = "loaderSliderMenu";
            this.loaderSliderMenu.Size = new System.Drawing.Size(26, 19);
            this.loaderSliderMenu.TabIndex = 75;
            this.loaderSliderMenu.VerticalScrollbarBarColor = true;
            this.loaderSliderMenu.VerticalScrollbarHighlightOnWheel = false;
            this.loaderSliderMenu.VerticalScrollbarSize = 10;
            this.loaderSliderMenu.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(196)))), ((int)(((byte)(37)))));
            this.panel2.Controls.Add(this.button1);
            this.panelAnimator.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.panel2.Location = new System.Drawing.Point(-1145, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1167, 30);
            this.panel2.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.button1, BunifuAnimatorNS.DecorationType.None);
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(1142, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(21, 18);
            this.button1.TabIndex = 12;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // metroTabControl3
            // 
            this.metroTabControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroTabControl3.Controls.Add(this.metroTabPage7);
            this.metroTabControl3.Controls.Add(this.metroTabPage8);
            this.panelAnimator.SetDecoration(this.metroTabControl3, BunifuAnimatorNS.DecorationType.None);
            this.metroTabControl3.Location = new System.Drawing.Point(-965, 41);
            this.metroTabControl3.Name = "metroTabControl3";
            this.metroTabControl3.SelectedIndex = 0;
            this.metroTabControl3.Size = new System.Drawing.Size(979, 733);
            this.metroTabControl3.TabIndex = 2;
            this.metroTabControl3.UseSelectable = true;
            // 
            // metroTabPage7
            // 
            this.metroTabPage7.Controls.Add(this.vilBtn);
            this.panelAnimator.SetDecoration(this.metroTabPage7, BunifuAnimatorNS.DecorationType.None);
            this.metroTabPage7.HorizontalScrollbarBarColor = true;
            this.metroTabPage7.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage7.HorizontalScrollbarSize = 10;
            this.metroTabPage7.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage7.Name = "metroTabPage7";
            this.metroTabPage7.Size = new System.Drawing.Size(971, 691);
            this.metroTabPage7.TabIndex = 0;
            this.metroTabPage7.Text = "Вилочные прогрузчики";
            this.metroTabPage7.VerticalScrollbarBarColor = true;
            this.metroTabPage7.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage7.VerticalScrollbarSize = 10;
            // 
            // vilBtn
            // 
            this.vilBtn.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.vilBtn, BunifuAnimatorNS.DecorationType.None);
            this.vilBtn.Image = global::AutoStorage.Properties.Resources.VilochniyPorgruzckij;
            this.vilBtn.ImageActive = null;
            this.vilBtn.InitialImage = null;
            this.vilBtn.Location = new System.Drawing.Point(340, 25);
            this.vilBtn.Name = "vilBtn";
            this.vilBtn.Size = new System.Drawing.Size(412, 370);
            this.vilBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.vilBtn.TabIndex = 2;
            this.vilBtn.TabStop = false;
            this.vilBtn.Zoom = 10;
            this.vilBtn.Click += new System.EventHandler(this.vilBtn_Click);
            // 
            // metroTabPage8
            // 
            this.metroTabPage8.Controls.Add(this.kovshBtn);
            this.panelAnimator.SetDecoration(this.metroTabPage8, BunifuAnimatorNS.DecorationType.None);
            this.metroTabPage8.HorizontalScrollbarBarColor = true;
            this.metroTabPage8.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage8.HorizontalScrollbarSize = 10;
            this.metroTabPage8.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage8.Name = "metroTabPage8";
            this.metroTabPage8.Size = new System.Drawing.Size(971, 691);
            this.metroTabPage8.TabIndex = 1;
            this.metroTabPage8.Text = "Ковшовые погрузчики";
            this.metroTabPage8.VerticalScrollbarBarColor = true;
            this.metroTabPage8.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage8.VerticalScrollbarSize = 10;
            // 
            // kovshBtn
            // 
            this.kovshBtn.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.kovshBtn, BunifuAnimatorNS.DecorationType.None);
            this.kovshBtn.Image = global::AutoStorage.Properties.Resources.Frontalnyy_pogruzchik_LW500F;
            this.kovshBtn.ImageActive = null;
            this.kovshBtn.InitialImage = null;
            this.kovshBtn.Location = new System.Drawing.Point(290, 34);
            this.kovshBtn.Name = "kovshBtn";
            this.kovshBtn.Size = new System.Drawing.Size(462, 376);
            this.kovshBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.kovshBtn.TabIndex = 8;
            this.kovshBtn.TabStop = false;
            this.kovshBtn.Zoom = 10;
            this.kovshBtn.Click += new System.EventHandler(this.kovshBtn_Click);
            // 
            // metroTabControl4
            // 
            this.panelAnimator.SetDecoration(this.metroTabControl4, BunifuAnimatorNS.DecorationType.None);
            this.metroTabControl4.Location = new System.Drawing.Point(30, 41);
            this.metroTabControl4.Name = "metroTabControl4";
            this.metroTabControl4.Size = new System.Drawing.Size(979, 733);
            this.metroTabControl4.TabIndex = 2;
            this.metroTabControl4.UseSelectable = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.loaderTonnageBox);
            this.groupBox6.Controls.Add(this.loaderCapacityBox);
            this.panelAnimator.SetDecoration(this.groupBox6, BunifuAnimatorNS.DecorationType.None);
            this.groupBox6.Location = new System.Drawing.Point(1478, 753);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(121, 182);
            this.groupBox6.TabIndex = 73;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "groupBox6";
            this.groupBox6.Visible = false;
            // 
            // loaderTonnageBox
            // 
            this.loaderTonnageBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.loaderTonnageBox, BunifuAnimatorNS.DecorationType.None);
            this.loaderTonnageBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.loaderTonnageBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.loaderTonnageBox.HintForeColor = System.Drawing.Color.Empty;
            this.loaderTonnageBox.HintText = "";
            this.loaderTonnageBox.isPassword = false;
            this.loaderTonnageBox.LineFocusedColor = System.Drawing.Color.Blue;
            this.loaderTonnageBox.LineIdleColor = System.Drawing.Color.Gray;
            this.loaderTonnageBox.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.loaderTonnageBox.LineThickness = 3;
            this.loaderTonnageBox.Location = new System.Drawing.Point(21, 73);
            this.loaderTonnageBox.Margin = new System.Windows.Forms.Padding(4);
            this.loaderTonnageBox.Name = "loaderTonnageBox";
            this.loaderTonnageBox.Size = new System.Drawing.Size(93, 33);
            this.loaderTonnageBox.TabIndex = 0;
            this.loaderTonnageBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // loaderCapacityBox
            // 
            this.loaderCapacityBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.loaderCapacityBox, BunifuAnimatorNS.DecorationType.None);
            this.loaderCapacityBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.loaderCapacityBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.loaderCapacityBox.HintForeColor = System.Drawing.Color.Empty;
            this.loaderCapacityBox.HintText = "";
            this.loaderCapacityBox.isPassword = false;
            this.loaderCapacityBox.LineFocusedColor = System.Drawing.Color.Blue;
            this.loaderCapacityBox.LineIdleColor = System.Drawing.Color.Gray;
            this.loaderCapacityBox.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.loaderCapacityBox.LineThickness = 3;
            this.loaderCapacityBox.Location = new System.Drawing.Point(21, 32);
            this.loaderCapacityBox.Margin = new System.Windows.Forms.Padding(4);
            this.loaderCapacityBox.Name = "loaderCapacityBox";
            this.loaderCapacityBox.Size = new System.Drawing.Size(93, 33);
            this.loaderCapacityBox.TabIndex = 0;
            this.loaderCapacityBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.loaderTypeBox);
            this.groupBox5.Controls.Add(this.metroLabel13);
            this.groupBox5.Controls.Add(this.transportPriceBox);
            this.groupBox5.Controls.Add(this.metroLabel27);
            this.groupBox5.Controls.Add(this.metroLabel23);
            this.groupBox5.Controls.Add(this.loadTypeBox);
            this.groupBox5.Controls.Add(this.metroLabel21);
            this.groupBox5.Controls.Add(this.carcasMaterialBox);
            this.groupBox5.Controls.Add(this.metroLabel18);
            this.groupBox5.Controls.Add(this.heighBox);
            this.groupBox5.Controls.Add(this.metroLabel22);
            this.groupBox5.Controls.Add(this.amountBox);
            this.groupBox5.Controls.Add(this.metroLabel24);
            this.groupBox5.Controls.Add(this.metroLabel25);
            this.groupBox5.Controls.Add(this.metroLabel29);
            this.groupBox5.Controls.Add(this.capacityBox);
            this.groupBox5.Controls.Add(this.metroLabel30);
            this.groupBox5.Controls.Add(this.carcasTypeBox);
            this.groupBox5.Controls.Add(this.metroLabel31);
            this.groupBox5.Controls.Add(this.carryingBox);
            this.groupBox5.Controls.Add(this.lengthBox);
            this.groupBox5.Controls.Add(this.widthBox);
            this.panelAnimator.SetDecoration(this.groupBox5, BunifuAnimatorNS.DecorationType.None);
            this.groupBox5.Location = new System.Drawing.Point(714, 746);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(758, 229);
            this.groupBox5.TabIndex = 43;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Информация о перевозчике и перегрузчике";
            // 
            // loaderTypeBox
            // 
            this.loaderTypeBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.loaderTypeBox, BunifuAnimatorNS.DecorationType.None);
            this.loaderTypeBox.Enabled = false;
            this.loaderTypeBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.loaderTypeBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.loaderTypeBox.HintForeColor = System.Drawing.Color.Empty;
            this.loaderTypeBox.HintText = "";
            this.loaderTypeBox.isPassword = false;
            this.loaderTypeBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.loaderTypeBox.LineIdleColor = System.Drawing.Color.Gray;
            this.loaderTypeBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.loaderTypeBox.LineThickness = 3;
            this.loaderTypeBox.Location = new System.Drawing.Point(182, 190);
            this.loaderTypeBox.Margin = new System.Windows.Forms.Padding(4);
            this.loaderTypeBox.Name = "loaderTypeBox";
            this.loaderTypeBox.Size = new System.Drawing.Size(192, 27);
            this.loaderTypeBox.TabIndex = 41;
            this.loaderTypeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel13, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel13.Location = new System.Drawing.Point(55, 196);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(119, 19);
            this.metroLabel13.TabIndex = 40;
            this.metroLabel13.Text = "Тип перегрузчика";
            // 
            // transportPriceBox
            // 
            this.transportPriceBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.transportPriceBox, BunifuAnimatorNS.DecorationType.None);
            this.transportPriceBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.transportPriceBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.transportPriceBox.HintForeColor = System.Drawing.Color.Empty;
            this.transportPriceBox.HintText = "";
            this.transportPriceBox.isPassword = false;
            this.transportPriceBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.transportPriceBox.LineIdleColor = System.Drawing.Color.Gray;
            this.transportPriceBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.transportPriceBox.LineThickness = 3;
            this.transportPriceBox.Location = new System.Drawing.Point(543, 159);
            this.transportPriceBox.Margin = new System.Windows.Forms.Padding(4);
            this.transportPriceBox.Name = "transportPriceBox";
            this.transportPriceBox.Size = new System.Drawing.Size(192, 27);
            this.transportPriceBox.TabIndex = 39;
            this.transportPriceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.transportPriceBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.transportPriceBox_KeyPress);
            // 
            // metroLabel27
            // 
            this.metroLabel27.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel27, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel27.Enabled = false;
            this.metroLabel27.Location = new System.Drawing.Point(381, 170);
            this.metroLabel27.Name = "metroLabel27";
            this.metroLabel27.Size = new System.Drawing.Size(156, 19);
            this.metroLabel27.TabIndex = 38;
            this.metroLabel27.Text = "Цена тонны перегрузки";
            // 
            // metroLabel23
            // 
            this.metroLabel23.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel23, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel23.Enabled = false;
            this.metroLabel23.Location = new System.Drawing.Point(421, 106);
            this.metroLabel23.Name = "metroLabel23";
            this.metroLabel23.Size = new System.Drawing.Size(114, 19);
            this.metroLabel23.TabIndex = 30;
            this.metroLabel23.Text = "Материал кузова";
            // 
            // loadTypeBox
            // 
            this.loadTypeBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.loadTypeBox, BunifuAnimatorNS.DecorationType.None);
            this.loadTypeBox.Enabled = false;
            this.loadTypeBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.loadTypeBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.loadTypeBox.HintForeColor = System.Drawing.Color.Empty;
            this.loadTypeBox.HintText = "";
            this.loadTypeBox.isPassword = false;
            this.loadTypeBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.loadTypeBox.LineIdleColor = System.Drawing.Color.Gray;
            this.loadTypeBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.loadTypeBox.LineThickness = 3;
            this.loadTypeBox.Location = new System.Drawing.Point(543, 126);
            this.loadTypeBox.Margin = new System.Windows.Forms.Padding(4);
            this.loadTypeBox.Name = "loadTypeBox";
            this.loadTypeBox.Size = new System.Drawing.Size(192, 27);
            this.loadTypeBox.TabIndex = 33;
            this.loadTypeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel21
            // 
            this.metroLabel21.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel21, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel21.Enabled = false;
            this.metroLabel21.Location = new System.Drawing.Point(33, 47);
            this.metroLabel21.Name = "metroLabel21";
            this.metroLabel21.Size = new System.Drawing.Size(141, 19);
            this.metroLabel21.TabIndex = 22;
            this.metroLabel21.Text = "Тип грузового кузова";
            // 
            // carcasMaterialBox
            // 
            this.carcasMaterialBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.carcasMaterialBox, BunifuAnimatorNS.DecorationType.None);
            this.carcasMaterialBox.Enabled = false;
            this.carcasMaterialBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.carcasMaterialBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.carcasMaterialBox.HintForeColor = System.Drawing.Color.Empty;
            this.carcasMaterialBox.HintText = "";
            this.carcasMaterialBox.isPassword = false;
            this.carcasMaterialBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.carcasMaterialBox.LineIdleColor = System.Drawing.Color.Gray;
            this.carcasMaterialBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.carcasMaterialBox.LineThickness = 3;
            this.carcasMaterialBox.Location = new System.Drawing.Point(543, 94);
            this.carcasMaterialBox.Margin = new System.Windows.Forms.Padding(4);
            this.carcasMaterialBox.Name = "carcasMaterialBox";
            this.carcasMaterialBox.Size = new System.Drawing.Size(192, 27);
            this.carcasMaterialBox.TabIndex = 35;
            this.carcasMaterialBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel18
            // 
            this.metroLabel18.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel18, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel18.Enabled = false;
            this.metroLabel18.Location = new System.Drawing.Point(88, 77);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(86, 19);
            this.metroLabel18.TabIndex = 21;
            this.metroLabel18.Text = "Вместимость";
            // 
            // heighBox
            // 
            this.heighBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.heighBox, BunifuAnimatorNS.DecorationType.None);
            this.heighBox.Enabled = false;
            this.heighBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.heighBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.heighBox.HintForeColor = System.Drawing.Color.Empty;
            this.heighBox.HintText = "";
            this.heighBox.isPassword = false;
            this.heighBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.heighBox.LineIdleColor = System.Drawing.Color.Gray;
            this.heighBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.heighBox.LineThickness = 3;
            this.heighBox.Location = new System.Drawing.Point(543, 30);
            this.heighBox.Margin = new System.Windows.Forms.Padding(4);
            this.heighBox.Name = "heighBox";
            this.heighBox.Size = new System.Drawing.Size(192, 27);
            this.heighBox.TabIndex = 36;
            this.heighBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel22
            // 
            this.metroLabel22.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel22, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel22.Enabled = false;
            this.metroLabel22.Location = new System.Drawing.Point(51, 107);
            this.metroLabel22.Name = "metroLabel22";
            this.metroLabel22.Size = new System.Drawing.Size(123, 19);
            this.metroLabel22.TabIndex = 20;
            this.metroLabel22.Text = "Грузоподъемность";
            // 
            // amountBox
            // 
            this.amountBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.amountBox, BunifuAnimatorNS.DecorationType.None);
            this.amountBox.Enabled = false;
            this.amountBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.amountBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.amountBox.HintForeColor = System.Drawing.Color.Empty;
            this.amountBox.HintText = "";
            this.amountBox.isPassword = false;
            this.amountBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.amountBox.LineIdleColor = System.Drawing.Color.Gray;
            this.amountBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.amountBox.LineThickness = 3;
            this.amountBox.Location = new System.Drawing.Point(543, 62);
            this.amountBox.Margin = new System.Windows.Forms.Padding(4);
            this.amountBox.Name = "amountBox";
            this.amountBox.Size = new System.Drawing.Size(192, 27);
            this.amountBox.TabIndex = 37;
            this.amountBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel24
            // 
            this.metroLabel24.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel24, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel24.Enabled = false;
            this.metroLabel24.Location = new System.Drawing.Point(110, 137);
            this.metroLabel24.Name = "metroLabel24";
            this.metroLabel24.Size = new System.Drawing.Size(64, 19);
            this.metroLabel24.TabIndex = 19;
            this.metroLabel24.Text = "Длина, м";
            // 
            // metroLabel25
            // 
            this.metroLabel25.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel25, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel25.Enabled = false;
            this.metroLabel25.Location = new System.Drawing.Point(414, 138);
            this.metroLabel25.Name = "metroLabel25";
            this.metroLabel25.Size = new System.Drawing.Size(121, 19);
            this.metroLabel25.TabIndex = 29;
            this.metroLabel25.Text = "Способы загрузки";
            // 
            // metroLabel29
            // 
            this.metroLabel29.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel29, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel29.Location = new System.Drawing.Point(98, 167);
            this.metroLabel29.Name = "metroLabel29";
            this.metroLabel29.Size = new System.Drawing.Size(76, 19);
            this.metroLabel29.TabIndex = 23;
            this.metroLabel29.Text = "Ширина, м";
            // 
            // capacityBox
            // 
            this.capacityBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.capacityBox, BunifuAnimatorNS.DecorationType.None);
            this.capacityBox.Enabled = false;
            this.capacityBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.capacityBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.capacityBox.HintForeColor = System.Drawing.Color.Empty;
            this.capacityBox.HintText = "";
            this.capacityBox.isPassword = false;
            this.capacityBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.capacityBox.LineIdleColor = System.Drawing.Color.Gray;
            this.capacityBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.capacityBox.LineThickness = 3;
            this.capacityBox.Location = new System.Drawing.Point(182, 65);
            this.capacityBox.Margin = new System.Windows.Forms.Padding(4);
            this.capacityBox.Name = "capacityBox";
            this.capacityBox.Size = new System.Drawing.Size(192, 27);
            this.capacityBox.TabIndex = 28;
            this.capacityBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel30
            // 
            this.metroLabel30.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel30, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel30.Enabled = false;
            this.metroLabel30.Location = new System.Drawing.Point(460, 74);
            this.metroLabel30.Name = "metroLabel30";
            this.metroLabel30.Size = new System.Drawing.Size(75, 19);
            this.metroLabel30.TabIndex = 31;
            this.metroLabel30.Text = "Объем, м3";
            // 
            // carcasTypeBox
            // 
            this.carcasTypeBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.carcasTypeBox, BunifuAnimatorNS.DecorationType.None);
            this.carcasTypeBox.Enabled = false;
            this.carcasTypeBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.carcasTypeBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.carcasTypeBox.HintForeColor = System.Drawing.Color.Empty;
            this.carcasTypeBox.HintText = "";
            this.carcasTypeBox.isPassword = false;
            this.carcasTypeBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.carcasTypeBox.LineIdleColor = System.Drawing.Color.Gray;
            this.carcasTypeBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.carcasTypeBox.LineThickness = 3;
            this.carcasTypeBox.Location = new System.Drawing.Point(182, 35);
            this.carcasTypeBox.Margin = new System.Windows.Forms.Padding(4);
            this.carcasTypeBox.Name = "carcasTypeBox";
            this.carcasTypeBox.Size = new System.Drawing.Size(192, 27);
            this.carcasTypeBox.TabIndex = 27;
            this.carcasTypeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel31
            // 
            this.metroLabel31.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel31, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel31.Enabled = false;
            this.metroLabel31.Location = new System.Drawing.Point(467, 42);
            this.metroLabel31.Name = "metroLabel31";
            this.metroLabel31.Size = new System.Drawing.Size(68, 19);
            this.metroLabel31.TabIndex = 32;
            this.metroLabel31.Text = "Высота, м";
            // 
            // carryingBox
            // 
            this.carryingBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.carryingBox, BunifuAnimatorNS.DecorationType.None);
            this.carryingBox.Enabled = false;
            this.carryingBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.carryingBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.carryingBox.HintForeColor = System.Drawing.Color.Empty;
            this.carryingBox.HintText = "";
            this.carryingBox.isPassword = false;
            this.carryingBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.carryingBox.LineIdleColor = System.Drawing.Color.Gray;
            this.carryingBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.carryingBox.LineThickness = 3;
            this.carryingBox.Location = new System.Drawing.Point(182, 95);
            this.carryingBox.Margin = new System.Windows.Forms.Padding(4);
            this.carryingBox.Name = "carryingBox";
            this.carryingBox.Size = new System.Drawing.Size(192, 27);
            this.carryingBox.TabIndex = 26;
            this.carryingBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // lengthBox
            // 
            this.lengthBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.lengthBox, BunifuAnimatorNS.DecorationType.None);
            this.lengthBox.Enabled = false;
            this.lengthBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.lengthBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lengthBox.HintForeColor = System.Drawing.Color.Empty;
            this.lengthBox.HintText = "";
            this.lengthBox.isPassword = false;
            this.lengthBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.lengthBox.LineIdleColor = System.Drawing.Color.Gray;
            this.lengthBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.lengthBox.LineThickness = 3;
            this.lengthBox.Location = new System.Drawing.Point(182, 125);
            this.lengthBox.Margin = new System.Windows.Forms.Padding(4);
            this.lengthBox.Name = "lengthBox";
            this.lengthBox.Size = new System.Drawing.Size(192, 27);
            this.lengthBox.TabIndex = 24;
            this.lengthBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // widthBox
            // 
            this.widthBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.widthBox, BunifuAnimatorNS.DecorationType.None);
            this.widthBox.Enabled = false;
            this.widthBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.widthBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.widthBox.HintForeColor = System.Drawing.Color.Empty;
            this.widthBox.HintText = "";
            this.widthBox.isPassword = false;
            this.widthBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.widthBox.LineIdleColor = System.Drawing.Color.Gray;
            this.widthBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.widthBox.LineThickness = 3;
            this.widthBox.Location = new System.Drawing.Point(182, 155);
            this.widthBox.Margin = new System.Windows.Forms.Padding(4);
            this.widthBox.Name = "widthBox";
            this.widthBox.Size = new System.Drawing.Size(192, 27);
            this.widthBox.TabIndex = 25;
            this.widthBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // priceSearchBox
            // 
            this.priceSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.priceSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.priceSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.priceSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.priceSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.priceSearchBox.HintText = "Поиск по цене";
            this.priceSearchBox.isPassword = false;
            this.priceSearchBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.priceSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.priceSearchBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.priceSearchBox.LineThickness = 3;
            this.priceSearchBox.Location = new System.Drawing.Point(502, -1);
            this.priceSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.priceSearchBox.Name = "priceSearchBox";
            this.priceSearchBox.Size = new System.Drawing.Size(192, 27);
            this.priceSearchBox.TabIndex = 72;
            this.priceSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.priceSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.priceSearchBox_KeyUp);
            // 
            // typeSearchBox
            // 
            this.typeSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.typeSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.typeSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.typeSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.typeSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.typeSearchBox.HintText = "Поиск по типу товара";
            this.typeSearchBox.isPassword = false;
            this.typeSearchBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.typeSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.typeSearchBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.typeSearchBox.LineThickness = 3;
            this.typeSearchBox.Location = new System.Drawing.Point(282, -1);
            this.typeSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.typeSearchBox.Name = "typeSearchBox";
            this.typeSearchBox.Size = new System.Drawing.Size(192, 27);
            this.typeSearchBox.TabIndex = 71;
            this.typeSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.typeSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.typeSearchBox_KeyUp);
            // 
            // updPurshaseBtn
            // 
            this.updPurshaseBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.updPurshaseBtn.BackColor = System.Drawing.Color.Peru;
            this.updPurshaseBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.updPurshaseBtn.BorderRadius = 0;
            this.updPurshaseBtn.ButtonText = "Добавить обновленные данные";
            this.updPurshaseBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.updPurshaseBtn, BunifuAnimatorNS.DecorationType.None);
            this.updPurshaseBtn.DisabledColor = System.Drawing.Color.Gray;
            this.updPurshaseBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.updPurshaseBtn.Iconimage = null;
            this.updPurshaseBtn.Iconimage_right = null;
            this.updPurshaseBtn.Iconimage_right_Selected = null;
            this.updPurshaseBtn.Iconimage_Selected = null;
            this.updPurshaseBtn.IconMarginLeft = 0;
            this.updPurshaseBtn.IconMarginRight = 0;
            this.updPurshaseBtn.IconRightVisible = true;
            this.updPurshaseBtn.IconRightZoom = 0D;
            this.updPurshaseBtn.IconVisible = true;
            this.updPurshaseBtn.IconZoom = 90D;
            this.updPurshaseBtn.IsTab = false;
            this.updPurshaseBtn.Location = new System.Drawing.Point(710, 981);
            this.updPurshaseBtn.Name = "updPurshaseBtn";
            this.updPurshaseBtn.Normalcolor = System.Drawing.Color.Peru;
            this.updPurshaseBtn.OnHovercolor = System.Drawing.Color.SaddleBrown;
            this.updPurshaseBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.updPurshaseBtn.selected = false;
            this.updPurshaseBtn.Size = new System.Drawing.Size(240, 48);
            this.updPurshaseBtn.TabIndex = 70;
            this.updPurshaseBtn.Text = "Добавить обновленные данные";
            this.updPurshaseBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.updPurshaseBtn.Textcolor = System.Drawing.Color.White;
            this.updPurshaseBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updPurshaseBtn.Click += new System.EventHandler(this.addPurshaseBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.fromPhoneBox);
            this.groupBox1.Controls.Add(this.fromEmailBox);
            this.groupBox1.Controls.Add(this.idBox);
            this.groupBox1.Controls.Add(this.fromCityBox);
            this.groupBox1.Controls.Add(this.companyFromBox);
            this.groupBox1.Controls.Add(this.fromAdresBox);
            this.groupBox1.Controls.Add(this.metroLabel5);
            this.groupBox1.Controls.Add(this.metroLabel4);
            this.groupBox1.Controls.Add(this.metroLabel3);
            this.groupBox1.Controls.Add(this.metroLabel2);
            this.groupBox1.Controls.Add(this.metroLabel1);
            this.panelAnimator.SetDecoration(this.groupBox1, BunifuAnimatorNS.DecorationType.None);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(774, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(381, 206);
            this.groupBox1.TabIndex = 67;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Отправитель";
            // 
            // fromPhoneBox
            // 
            this.fromPhoneBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.fromPhoneBox, BunifuAnimatorNS.DecorationType.None);
            this.fromPhoneBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.fromPhoneBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.fromPhoneBox.HintForeColor = System.Drawing.Color.Empty;
            this.fromPhoneBox.HintText = "";
            this.fromPhoneBox.isPassword = false;
            this.fromPhoneBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.fromPhoneBox.LineIdleColor = System.Drawing.Color.Gray;
            this.fromPhoneBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.fromPhoneBox.LineThickness = 3;
            this.fromPhoneBox.Location = new System.Drawing.Point(183, 119);
            this.fromPhoneBox.Margin = new System.Windows.Forms.Padding(4);
            this.fromPhoneBox.Name = "fromPhoneBox";
            this.fromPhoneBox.Size = new System.Drawing.Size(192, 27);
            this.fromPhoneBox.TabIndex = 13;
            this.fromPhoneBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.fromPhoneBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fromPhoneBox_KeyPress);
            // 
            // fromEmailBox
            // 
            this.fromEmailBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.fromEmailBox, BunifuAnimatorNS.DecorationType.None);
            this.fromEmailBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.fromEmailBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.fromEmailBox.HintForeColor = System.Drawing.Color.Empty;
            this.fromEmailBox.HintText = "";
            this.fromEmailBox.isPassword = false;
            this.fromEmailBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.fromEmailBox.LineIdleColor = System.Drawing.Color.Gray;
            this.fromEmailBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.fromEmailBox.LineThickness = 3;
            this.fromEmailBox.Location = new System.Drawing.Point(183, 149);
            this.fromEmailBox.Margin = new System.Windows.Forms.Padding(4);
            this.fromEmailBox.Name = "fromEmailBox";
            this.fromEmailBox.Size = new System.Drawing.Size(192, 27);
            this.fromEmailBox.TabIndex = 14;
            this.fromEmailBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // idBox
            // 
            this.idBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.idBox, BunifuAnimatorNS.DecorationType.None);
            this.idBox.Enabled = false;
            this.idBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.idBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.idBox.HintForeColor = System.Drawing.Color.Empty;
            this.idBox.HintText = "";
            this.idBox.isPassword = false;
            this.idBox.LineFocusedColor = System.Drawing.Color.Blue;
            this.idBox.LineIdleColor = System.Drawing.Color.Gray;
            this.idBox.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.idBox.LineThickness = 3;
            this.idBox.Location = new System.Drawing.Point(28, 60);
            this.idBox.Margin = new System.Windows.Forms.Padding(4);
            this.idBox.Name = "idBox";
            this.idBox.Size = new System.Drawing.Size(35, 29);
            this.idBox.TabIndex = 36;
            this.idBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.idBox.Visible = false;
            // 
            // fromCityBox
            // 
            this.fromCityBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.fromCityBox, BunifuAnimatorNS.DecorationType.None);
            this.fromCityBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.fromCityBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.fromCityBox.HintForeColor = System.Drawing.Color.Empty;
            this.fromCityBox.HintText = "";
            this.fromCityBox.isPassword = false;
            this.fromCityBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.fromCityBox.LineIdleColor = System.Drawing.Color.Gray;
            this.fromCityBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.fromCityBox.LineThickness = 3;
            this.fromCityBox.Location = new System.Drawing.Point(183, 89);
            this.fromCityBox.Margin = new System.Windows.Forms.Padding(4);
            this.fromCityBox.Name = "fromCityBox";
            this.fromCityBox.Size = new System.Drawing.Size(192, 27);
            this.fromCityBox.TabIndex = 15;
            this.fromCityBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // companyFromBox
            // 
            this.companyFromBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.companyFromBox, BunifuAnimatorNS.DecorationType.None);
            this.companyFromBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.companyFromBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.companyFromBox.HintForeColor = System.Drawing.Color.Empty;
            this.companyFromBox.HintText = "";
            this.companyFromBox.isPassword = false;
            this.companyFromBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.companyFromBox.LineIdleColor = System.Drawing.Color.Gray;
            this.companyFromBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.companyFromBox.LineThickness = 3;
            this.companyFromBox.Location = new System.Drawing.Point(183, 29);
            this.companyFromBox.Margin = new System.Windows.Forms.Padding(4);
            this.companyFromBox.Name = "companyFromBox";
            this.companyFromBox.Size = new System.Drawing.Size(192, 27);
            this.companyFromBox.TabIndex = 1;
            this.companyFromBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // fromAdresBox
            // 
            this.fromAdresBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.fromAdresBox, BunifuAnimatorNS.DecorationType.None);
            this.fromAdresBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.fromAdresBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.fromAdresBox.HintForeColor = System.Drawing.Color.Empty;
            this.fromAdresBox.HintText = "";
            this.fromAdresBox.isPassword = false;
            this.fromAdresBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.fromAdresBox.LineIdleColor = System.Drawing.Color.Gray;
            this.fromAdresBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.fromAdresBox.LineThickness = 3;
            this.fromAdresBox.Location = new System.Drawing.Point(183, 59);
            this.fromAdresBox.Margin = new System.Windows.Forms.Padding(4);
            this.fromAdresBox.Name = "fromAdresBox";
            this.fromAdresBox.Size = new System.Drawing.Size(192, 27);
            this.fromAdresBox.TabIndex = 17;
            this.fromAdresBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel5, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel5.Location = new System.Drawing.Point(135, 157);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(41, 19);
            this.metroLabel5.TabIndex = 12;
            this.metroLabel5.Text = "Email";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel4, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel4.Enabled = false;
            this.metroLabel4.Location = new System.Drawing.Point(106, 127);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(70, 19);
            this.metroLabel4.TabIndex = 8;
            this.metroLabel4.Text = "Телефоон";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel3, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel3.Enabled = false;
            this.metroLabel3.Location = new System.Drawing.Point(130, 97);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(46, 19);
            this.metroLabel3.TabIndex = 9;
            this.metroLabel3.Text = "Город";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel2, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel2.Enabled = false;
            this.metroLabel2.Location = new System.Drawing.Point(130, 67);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(46, 19);
            this.metroLabel2.TabIndex = 10;
            this.metroLabel2.Text = "Адрес";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel1, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel1.Enabled = false;
            this.metroLabel1.Location = new System.Drawing.Point(6, 37);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(170, 19);
            this.metroLabel1.TabIndex = 11;
            this.metroLabel1.Text = "Отправляющая компания";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.toPhoneBox);
            this.groupBox2.Controls.Add(this.toEmailBox);
            this.groupBox2.Controls.Add(this.toCityBox);
            this.groupBox2.Controls.Add(this.companyToBox);
            this.groupBox2.Controls.Add(this.toAdresBox);
            this.groupBox2.Controls.Add(this.metroLabel6);
            this.groupBox2.Controls.Add(this.metroLabel7);
            this.groupBox2.Controls.Add(this.metroLabel8);
            this.groupBox2.Controls.Add(this.metroLabel9);
            this.groupBox2.Controls.Add(this.metroLabel10);
            this.panelAnimator.SetDecoration(this.groupBox2, BunifuAnimatorNS.DecorationType.None);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(1161, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(381, 206);
            this.groupBox2.TabIndex = 68;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Получатель";
            // 
            // toPhoneBox
            // 
            this.toPhoneBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.toPhoneBox, BunifuAnimatorNS.DecorationType.None);
            this.toPhoneBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.toPhoneBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.toPhoneBox.HintForeColor = System.Drawing.Color.Empty;
            this.toPhoneBox.HintText = "";
            this.toPhoneBox.isPassword = false;
            this.toPhoneBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.toPhoneBox.LineIdleColor = System.Drawing.Color.Gray;
            this.toPhoneBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.toPhoneBox.LineThickness = 3;
            this.toPhoneBox.Location = new System.Drawing.Point(172, 127);
            this.toPhoneBox.Margin = new System.Windows.Forms.Padding(4);
            this.toPhoneBox.Name = "toPhoneBox";
            this.toPhoneBox.Size = new System.Drawing.Size(192, 27);
            this.toPhoneBox.TabIndex = 18;
            this.toPhoneBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // toEmailBox
            // 
            this.toEmailBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.toEmailBox, BunifuAnimatorNS.DecorationType.None);
            this.toEmailBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.toEmailBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.toEmailBox.HintForeColor = System.Drawing.Color.Empty;
            this.toEmailBox.HintText = "";
            this.toEmailBox.isPassword = false;
            this.toEmailBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.toEmailBox.LineIdleColor = System.Drawing.Color.Gray;
            this.toEmailBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.toEmailBox.LineThickness = 3;
            this.toEmailBox.Location = new System.Drawing.Point(172, 159);
            this.toEmailBox.Margin = new System.Windows.Forms.Padding(4);
            this.toEmailBox.Name = "toEmailBox";
            this.toEmailBox.Size = new System.Drawing.Size(192, 27);
            this.toEmailBox.TabIndex = 19;
            this.toEmailBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // toCityBox
            // 
            this.toCityBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.toCityBox, BunifuAnimatorNS.DecorationType.None);
            this.toCityBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.toCityBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.toCityBox.HintForeColor = System.Drawing.Color.Empty;
            this.toCityBox.HintText = "";
            this.toCityBox.isPassword = false;
            this.toCityBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.toCityBox.LineIdleColor = System.Drawing.Color.Gray;
            this.toCityBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.toCityBox.LineThickness = 3;
            this.toCityBox.Location = new System.Drawing.Point(172, 95);
            this.toCityBox.Margin = new System.Windows.Forms.Padding(4);
            this.toCityBox.Name = "toCityBox";
            this.toCityBox.Size = new System.Drawing.Size(192, 27);
            this.toCityBox.TabIndex = 20;
            this.toCityBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // companyToBox
            // 
            this.companyToBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.companyToBox, BunifuAnimatorNS.DecorationType.None);
            this.companyToBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.companyToBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.companyToBox.HintForeColor = System.Drawing.Color.Empty;
            this.companyToBox.HintText = "";
            this.companyToBox.isPassword = false;
            this.companyToBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.companyToBox.LineIdleColor = System.Drawing.Color.Gray;
            this.companyToBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.companyToBox.LineThickness = 3;
            this.companyToBox.Location = new System.Drawing.Point(172, 31);
            this.companyToBox.Margin = new System.Windows.Forms.Padding(4);
            this.companyToBox.Name = "companyToBox";
            this.companyToBox.Size = new System.Drawing.Size(192, 27);
            this.companyToBox.TabIndex = 21;
            this.companyToBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // toAdresBox
            // 
            this.toAdresBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.toAdresBox, BunifuAnimatorNS.DecorationType.None);
            this.toAdresBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.toAdresBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.toAdresBox.HintForeColor = System.Drawing.Color.Empty;
            this.toAdresBox.HintText = "";
            this.toAdresBox.isPassword = false;
            this.toAdresBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.toAdresBox.LineIdleColor = System.Drawing.Color.Gray;
            this.toAdresBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.toAdresBox.LineThickness = 3;
            this.toAdresBox.Location = new System.Drawing.Point(172, 63);
            this.toAdresBox.Margin = new System.Windows.Forms.Padding(4);
            this.toAdresBox.Name = "toAdresBox";
            this.toAdresBox.Size = new System.Drawing.Size(192, 27);
            this.toAdresBox.TabIndex = 22;
            this.toAdresBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel6, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel6.Location = new System.Drawing.Point(124, 166);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(41, 19);
            this.metroLabel6.TabIndex = 17;
            this.metroLabel6.Text = "Email";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel7, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel7.Enabled = false;
            this.metroLabel7.Location = new System.Drawing.Point(95, 136);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(70, 19);
            this.metroLabel7.TabIndex = 13;
            this.metroLabel7.Text = "Телефоон";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel8, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel8.Enabled = false;
            this.metroLabel8.Location = new System.Drawing.Point(119, 106);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(46, 19);
            this.metroLabel8.TabIndex = 14;
            this.metroLabel8.Text = "Город";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel9, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel9.Enabled = false;
            this.metroLabel9.Location = new System.Drawing.Point(113, 76);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(52, 19);
            this.metroLabel9.TabIndex = 15;
            this.metroLabel9.Text = "Адресс";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel10, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel10.Enabled = false;
            this.metroLabel10.Location = new System.Drawing.Point(11, 46);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(154, 19);
            this.metroLabel10.TabIndex = 16;
            this.metroLabel10.Text = "Получающая компания";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.updLoaderBtn);
            this.groupBox3.Controls.Add(this.updTransrtBtn);
            this.groupBox3.Controls.Add(this.metroLabel17);
            this.groupBox3.Controls.Add(this.totalDaysBox);
            this.groupBox3.Controls.Add(this.metroLabel16);
            this.groupBox3.Controls.Add(this.keepingForWeekPriceBox);
            this.groupBox3.Controls.Add(this.metroLabel15);
            this.groupBox3.Controls.Add(this.metroDateTime2);
            this.groupBox3.Controls.Add(this.metroLabel28);
            this.groupBox3.Controls.Add(this.cubageBox);
            this.groupBox3.Controls.Add(this.metroLabel26);
            this.groupBox3.Controls.Add(this.keepingSumBox);
            this.groupBox3.Controls.Add(this.updateGoodsBtn);
            this.groupBox3.Controls.Add(this.imgList);
            this.groupBox3.Controls.Add(this.metroLabel20);
            this.groupBox3.Controls.Add(this.metroDateTime1);
            this.groupBox3.Controls.Add(this.metroLabel19);
            this.groupBox3.Controls.Add(this.metroLabel14);
            this.groupBox3.Controls.Add(this.metroLabel12);
            this.groupBox3.Controls.Add(this.metroLabel11);
            this.groupBox3.Controls.Add(this.weightBox);
            this.groupBox3.Controls.Add(this.goodsNameBox);
            this.groupBox3.Controls.Add(this.goodsTypeBox);
            this.panelAnimator.SetDecoration(this.groupBox3, BunifuAnimatorNS.DecorationType.None);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.Location = new System.Drawing.Point(714, 224);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(878, 516);
            this.groupBox3.TabIndex = 69;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Товары";
            // 
            // updLoaderBtn
            // 
            this.updLoaderBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.updLoaderBtn.BackColor = System.Drawing.Color.Peru;
            this.updLoaderBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.updLoaderBtn.BorderRadius = 0;
            this.updLoaderBtn.ButtonText = "Изменить тип погрузчика";
            this.updLoaderBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.updLoaderBtn, BunifuAnimatorNS.DecorationType.None);
            this.updLoaderBtn.DisabledColor = System.Drawing.Color.Gray;
            this.updLoaderBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.updLoaderBtn.Iconimage = null;
            this.updLoaderBtn.Iconimage_right = null;
            this.updLoaderBtn.Iconimage_right_Selected = null;
            this.updLoaderBtn.Iconimage_Selected = null;
            this.updLoaderBtn.IconMarginLeft = 0;
            this.updLoaderBtn.IconMarginRight = 0;
            this.updLoaderBtn.IconRightVisible = true;
            this.updLoaderBtn.IconRightZoom = 0D;
            this.updLoaderBtn.IconVisible = true;
            this.updLoaderBtn.IconZoom = 90D;
            this.updLoaderBtn.IsTab = false;
            this.updLoaderBtn.Location = new System.Drawing.Point(605, 437);
            this.updLoaderBtn.Name = "updLoaderBtn";
            this.updLoaderBtn.Normalcolor = System.Drawing.Color.Peru;
            this.updLoaderBtn.OnHovercolor = System.Drawing.Color.SaddleBrown;
            this.updLoaderBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.updLoaderBtn.selected = false;
            this.updLoaderBtn.Size = new System.Drawing.Size(253, 48);
            this.updLoaderBtn.TabIndex = 37;
            this.updLoaderBtn.Text = "Изменить тип погрузчика";
            this.updLoaderBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.updLoaderBtn.Textcolor = System.Drawing.Color.White;
            this.updLoaderBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updLoaderBtn.Click += new System.EventHandler(this.updLoaderBtn_Click);
            // 
            // updTransrtBtn
            // 
            this.updTransrtBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.updTransrtBtn.BackColor = System.Drawing.Color.Peru;
            this.updTransrtBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.updTransrtBtn.BorderRadius = 0;
            this.updTransrtBtn.ButtonText = "Изменить тип перевозчика";
            this.updTransrtBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.updTransrtBtn, BunifuAnimatorNS.DecorationType.None);
            this.updTransrtBtn.DisabledColor = System.Drawing.Color.Gray;
            this.updTransrtBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.updTransrtBtn.Iconimage = null;
            this.updTransrtBtn.Iconimage_right = null;
            this.updTransrtBtn.Iconimage_right_Selected = null;
            this.updTransrtBtn.Iconimage_Selected = null;
            this.updTransrtBtn.IconMarginLeft = 0;
            this.updTransrtBtn.IconMarginRight = 0;
            this.updTransrtBtn.IconRightVisible = true;
            this.updTransrtBtn.IconRightZoom = 0D;
            this.updTransrtBtn.IconVisible = true;
            this.updTransrtBtn.IconZoom = 90D;
            this.updTransrtBtn.IsTab = false;
            this.updTransrtBtn.Location = new System.Drawing.Point(605, 383);
            this.updTransrtBtn.Name = "updTransrtBtn";
            this.updTransrtBtn.Normalcolor = System.Drawing.Color.Peru;
            this.updTransrtBtn.OnHovercolor = System.Drawing.Color.SaddleBrown;
            this.updTransrtBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.updTransrtBtn.selected = false;
            this.updTransrtBtn.Size = new System.Drawing.Size(253, 48);
            this.updTransrtBtn.TabIndex = 37;
            this.updTransrtBtn.Text = "Изменить тип перевозчика";
            this.updTransrtBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.updTransrtBtn.Textcolor = System.Drawing.Color.White;
            this.updTransrtBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updTransrtBtn.Click += new System.EventHandler(this.updTransrtBtn_Click);
            // 
            // metroLabel17
            // 
            this.metroLabel17.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.metroLabel17.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel17, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel17.Location = new System.Drawing.Point(518, 258);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(90, 19);
            this.metroLabel17.TabIndex = 35;
            this.metroLabel17.Text = "Кол-во дней ";
            // 
            // totalDaysBox
            // 
            this.totalDaysBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.totalDaysBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.totalDaysBox, BunifuAnimatorNS.DecorationType.None);
            this.totalDaysBox.Enabled = false;
            this.totalDaysBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.totalDaysBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.totalDaysBox.HintForeColor = System.Drawing.Color.Empty;
            this.totalDaysBox.HintText = "";
            this.totalDaysBox.isPassword = false;
            this.totalDaysBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.totalDaysBox.LineIdleColor = System.Drawing.Color.Gray;
            this.totalDaysBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.totalDaysBox.LineThickness = 3;
            this.totalDaysBox.Location = new System.Drawing.Point(618, 248);
            this.totalDaysBox.Margin = new System.Windows.Forms.Padding(4);
            this.totalDaysBox.Name = "totalDaysBox";
            this.totalDaysBox.Size = new System.Drawing.Size(78, 29);
            this.totalDaysBox.TabIndex = 34;
            this.totalDaysBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.totalDaysBox.OnValueChanged += new System.EventHandler(this.totalDaysBox_OnValueChanged);
            // 
            // metroLabel16
            // 
            this.metroLabel16.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.metroLabel16.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel16, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel16.Location = new System.Drawing.Point(427, 224);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(181, 19);
            this.metroLabel16.TabIndex = 33;
            this.metroLabel16.Text = "Цена хранение за 1 неделю";
            // 
            // keepingForWeekPriceBox
            // 
            this.keepingForWeekPriceBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.keepingForWeekPriceBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.keepingForWeekPriceBox, BunifuAnimatorNS.DecorationType.None);
            this.keepingForWeekPriceBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.keepingForWeekPriceBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.keepingForWeekPriceBox.HintForeColor = System.Drawing.Color.Empty;
            this.keepingForWeekPriceBox.HintText = "";
            this.keepingForWeekPriceBox.isPassword = false;
            this.keepingForWeekPriceBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.keepingForWeekPriceBox.LineIdleColor = System.Drawing.Color.Gray;
            this.keepingForWeekPriceBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.keepingForWeekPriceBox.LineThickness = 3;
            this.keepingForWeekPriceBox.Location = new System.Drawing.Point(618, 211);
            this.keepingForWeekPriceBox.Margin = new System.Windows.Forms.Padding(4);
            this.keepingForWeekPriceBox.Name = "keepingForWeekPriceBox";
            this.keepingForWeekPriceBox.Size = new System.Drawing.Size(78, 29);
            this.keepingForWeekPriceBox.TabIndex = 32;
            this.keepingForWeekPriceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.keepingForWeekPriceBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.keepingForWeekPriceBox_KeyPress);
            this.keepingForWeekPriceBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keepingForWeekPriceBox_KeyUp);
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel15, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel15.Location = new System.Drawing.Point(518, 181);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(91, 19);
            this.metroLabel15.TabIndex = 30;
            this.metroLabel15.Text = "Дата отбытия";
            // 
            // metroDateTime2
            // 
            this.metroDateTime2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroDateTime2.Checked = false;
            this.panelAnimator.SetDecoration(this.metroDateTime2, BunifuAnimatorNS.DecorationType.None);
            this.metroDateTime2.Location = new System.Drawing.Point(619, 176);
            this.metroDateTime2.MinimumSize = new System.Drawing.Size(4, 29);
            this.metroDateTime2.Name = "metroDateTime2";
            this.metroDateTime2.Size = new System.Drawing.Size(253, 29);
            this.metroDateTime2.TabIndex = 29;
            this.metroDateTime2.CloseUp += new System.EventHandler(this.metroDateTime2_CloseUp);
            // 
            // metroLabel28
            // 
            this.metroLabel28.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel28, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel28.Location = new System.Drawing.Point(618, 75);
            this.metroLabel28.Name = "metroLabel28";
            this.metroLabel28.Size = new System.Drawing.Size(99, 19);
            this.metroLabel28.TabIndex = 28;
            this.metroLabel28.Text = "Кубатура ( м3 )";
            // 
            // cubageBox
            // 
            this.cubageBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.cubageBox, BunifuAnimatorNS.DecorationType.None);
            this.cubageBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.cubageBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cubageBox.HintForeColor = System.Drawing.Color.Empty;
            this.cubageBox.HintText = "";
            this.cubageBox.isPassword = false;
            this.cubageBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.cubageBox.LineIdleColor = System.Drawing.Color.Gray;
            this.cubageBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.cubageBox.LineThickness = 3;
            this.cubageBox.Location = new System.Drawing.Point(716, 62);
            this.cubageBox.Margin = new System.Windows.Forms.Padding(4);
            this.cubageBox.Name = "cubageBox";
            this.cubageBox.Size = new System.Drawing.Size(78, 29);
            this.cubageBox.TabIndex = 27;
            this.cubageBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.cubageBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cubageBox_KeyPress);
            // 
            // metroLabel26
            // 
            this.metroLabel26.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.metroLabel26.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel26, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel26.Location = new System.Drawing.Point(467, 295);
            this.metroLabel26.Name = "metroLabel26";
            this.metroLabel26.Size = new System.Drawing.Size(136, 19);
            this.metroLabel26.TabIndex = 26;
            this.metroLabel26.Text = "Стоимость хранения";
            // 
            // keepingSumBox
            // 
            this.keepingSumBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.keepingSumBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.keepingSumBox, BunifuAnimatorNS.DecorationType.None);
            this.keepingSumBox.Enabled = false;
            this.keepingSumBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.keepingSumBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.keepingSumBox.HintForeColor = System.Drawing.Color.Empty;
            this.keepingSumBox.HintText = "";
            this.keepingSumBox.isPassword = false;
            this.keepingSumBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.keepingSumBox.LineIdleColor = System.Drawing.Color.Gray;
            this.keepingSumBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.keepingSumBox.LineThickness = 3;
            this.keepingSumBox.Location = new System.Drawing.Point(618, 285);
            this.keepingSumBox.Margin = new System.Windows.Forms.Padding(4);
            this.keepingSumBox.Name = "keepingSumBox";
            this.keepingSumBox.Size = new System.Drawing.Size(96, 29);
            this.keepingSumBox.TabIndex = 25;
            this.keepingSumBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // updateGoodsBtn
            // 
            this.updateGoodsBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.updateGoodsBtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.updateGoodsBtn.BackColor = System.Drawing.Color.Peru;
            this.updateGoodsBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.updateGoodsBtn.BorderRadius = 0;
            this.updateGoodsBtn.ButtonText = "Изменить список товаров";
            this.updateGoodsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.updateGoodsBtn, BunifuAnimatorNS.DecorationType.None);
            this.updateGoodsBtn.DisabledColor = System.Drawing.Color.Gray;
            this.updateGoodsBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.updateGoodsBtn.Iconimage = null;
            this.updateGoodsBtn.Iconimage_right = null;
            this.updateGoodsBtn.Iconimage_right_Selected = null;
            this.updateGoodsBtn.Iconimage_Selected = null;
            this.updateGoodsBtn.IconMarginLeft = 0;
            this.updateGoodsBtn.IconMarginRight = 0;
            this.updateGoodsBtn.IconRightVisible = true;
            this.updateGoodsBtn.IconRightZoom = 0D;
            this.updateGoodsBtn.IconVisible = true;
            this.updateGoodsBtn.IconZoom = 90D;
            this.updateGoodsBtn.IsTab = false;
            this.updateGoodsBtn.Location = new System.Drawing.Point(605, 329);
            this.updateGoodsBtn.Name = "updateGoodsBtn";
            this.updateGoodsBtn.Normalcolor = System.Drawing.Color.Peru;
            this.updateGoodsBtn.OnHovercolor = System.Drawing.Color.SaddleBrown;
            this.updateGoodsBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.updateGoodsBtn.selected = false;
            this.updateGoodsBtn.Size = new System.Drawing.Size(253, 48);
            this.updateGoodsBtn.TabIndex = 24;
            this.updateGoodsBtn.Text = "Изменить список товаров";
            this.updateGoodsBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.updateGoodsBtn.Textcolor = System.Drawing.Color.White;
            this.updateGoodsBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateGoodsBtn.Click += new System.EventHandler(this.updateGoodsBtn_Click);
            // 
            // imgList
            // 
            this.imgList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.imgList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imgList.Cursor = System.Windows.Forms.Cursors.Default;
            this.panelAnimator.SetDecoration(this.imgList, BunifuAnimatorNS.DecorationType.None);
            this.imgList.Location = new System.Drawing.Point(161, 110);
            this.imgList.Name = "imgList";
            this.imgList.Size = new System.Drawing.Size(260, 341);
            this.imgList.TabIndex = 23;
            this.imgList.TabStop = false;
            // 
            // metroLabel20
            // 
            this.metroLabel20.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel20, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel20.Location = new System.Drawing.Point(507, 143);
            this.metroLabel20.Name = "metroLabel20";
            this.metroLabel20.Size = new System.Drawing.Size(102, 19);
            this.metroLabel20.TabIndex = 22;
            this.metroLabel20.Text = "Дата прибытия";
            // 
            // metroDateTime1
            // 
            this.metroDateTime1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroDateTime1.Checked = false;
            this.panelAnimator.SetDecoration(this.metroDateTime1, BunifuAnimatorNS.DecorationType.None);
            this.metroDateTime1.Location = new System.Drawing.Point(618, 139);
            this.metroDateTime1.MinimumSize = new System.Drawing.Size(4, 29);
            this.metroDateTime1.Name = "metroDateTime1";
            this.metroDateTime1.Size = new System.Drawing.Size(253, 29);
            this.metroDateTime1.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroDateTime1.TabIndex = 21;
            this.metroDateTime1.CloseUp += new System.EventHandler(this.metroDateTime1_CloseUp);
            // 
            // metroLabel19
            // 
            this.metroLabel19.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel19, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel19.Location = new System.Drawing.Point(577, 105);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(32, 19);
            this.metroLabel19.TabIndex = 20;
            this.metroLabel19.Text = "Тип";
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel14, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel14.Location = new System.Drawing.Point(47, 110);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(108, 19);
            this.metroLabel14.TabIndex = 18;
            this.metroLabel14.Text = "Список товаров";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel12, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel12.Location = new System.Drawing.Point(662, 41);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(55, 19);
            this.metroLabel12.TabIndex = 18;
            this.metroLabel12.Text = "Вес ( т )";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel11, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel11.Location = new System.Drawing.Point(7, 41);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(204, 19);
            this.metroLabel11.TabIndex = 18;
            this.metroLabel11.Text = "Наименование группы товаров";
            // 
            // weightBox
            // 
            this.weightBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.weightBox, BunifuAnimatorNS.DecorationType.None);
            this.weightBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.weightBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.weightBox.HintForeColor = System.Drawing.Color.Empty;
            this.weightBox.HintText = "";
            this.weightBox.isPassword = false;
            this.weightBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.weightBox.LineIdleColor = System.Drawing.Color.Gray;
            this.weightBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.weightBox.LineThickness = 3;
            this.weightBox.Location = new System.Drawing.Point(716, 28);
            this.weightBox.Margin = new System.Windows.Forms.Padding(4);
            this.weightBox.Name = "weightBox";
            this.weightBox.Size = new System.Drawing.Size(78, 29);
            this.weightBox.TabIndex = 17;
            this.weightBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.weightBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.weightBox_KeyPress);
            // 
            // goodsNameBox
            // 
            this.goodsNameBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.goodsNameBox, BunifuAnimatorNS.DecorationType.None);
            this.goodsNameBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.goodsNameBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.goodsNameBox.HintForeColor = System.Drawing.Color.Empty;
            this.goodsNameBox.HintText = "";
            this.goodsNameBox.isPassword = false;
            this.goodsNameBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.goodsNameBox.LineIdleColor = System.Drawing.Color.Gray;
            this.goodsNameBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.goodsNameBox.LineThickness = 3;
            this.goodsNameBox.Location = new System.Drawing.Point(212, 28);
            this.goodsNameBox.Margin = new System.Windows.Forms.Padding(4);
            this.goodsNameBox.Name = "goodsNameBox";
            this.goodsNameBox.Size = new System.Drawing.Size(209, 29);
            this.goodsNameBox.TabIndex = 17;
            this.goodsNameBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // goodsTypeBox
            // 
            this.panelAnimator.SetDecoration(this.goodsTypeBox, BunifuAnimatorNS.DecorationType.None);
            this.goodsTypeBox.FormattingEnabled = true;
            this.goodsTypeBox.ItemHeight = 23;
            this.goodsTypeBox.Items.AddRange(new object[] {
            "Продовольственные товары",
            "Электронные товары",
            "Товары с температурным режимом",
            "Строительные материалы",
            "Сыпучие"});
            this.goodsTypeBox.Location = new System.Drawing.Point(618, 102);
            this.goodsTypeBox.Name = "goodsTypeBox";
            this.goodsTypeBox.Size = new System.Drawing.Size(253, 29);
            this.goodsTypeBox.TabIndex = 16;
            this.goodsTypeBox.Tag = "Тип товаров";
            this.goodsTypeBox.UseSelectable = true;
            this.goodsTypeBox.SelectedIndexChanged += new System.EventHandler(this.goodsTypeBox_SelectedIndexChanged);
            this.goodsTypeBox.SelectionChangeCommitted += new System.EventHandler(this.goodsTypeBox_SelectionChangeCommitted);
            // 
            // slidemenu
            // 
            this.slidemenu.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("slidemenu.BackgroundImage")));
            this.slidemenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slidemenu.Controls.Add(this.bunifuFlatButton1);
            this.slidemenu.Controls.Add(this.bunifuFlatButton5);
            this.slidemenu.Controls.Add(this.bunifuFlatButton4);
            this.slidemenu.Controls.Add(this.bunifuFlatButton3);
            this.slidemenu.Controls.Add(this.bunifuFlatButton2);
            this.slidemenu.Controls.Add(this.bunifuFlatButton7);
            this.slidemenu.Controls.Add(this.btnSlide);
            this.panelAnimator.SetDecoration(this.slidemenu, BunifuAnimatorNS.DecorationType.None);
            this.slidemenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.slidemenu.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(196)))), ((int)(((byte)(37)))));
            this.slidemenu.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(196)))), ((int)(((byte)(37)))));
            this.slidemenu.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(196)))), ((int)(((byte)(37)))));
            this.slidemenu.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(196)))), ((int)(((byte)(37)))));
            this.slidemenu.Location = new System.Drawing.Point(0, 0);
            this.slidemenu.Name = "slidemenu";
            this.slidemenu.Quality = 10;
            this.slidemenu.Size = new System.Drawing.Size(50, 1029);
            this.slidemenu.TabIndex = 65;
            // 
            // bunifuFlatButton1
            // 
            this.bunifuFlatButton1.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.bunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton1.BorderRadius = 0;
            this.bunifuFlatButton1.ButtonText = "Сменить пользователя";
            this.bunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.bunifuFlatButton1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton1.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton1.Iconimage = global::AutoStorage.Properties.Resources.icons8_Export_64;
            this.bunifuFlatButton1.Iconimage_right = null;
            this.bunifuFlatButton1.Iconimage_right_Selected = null;
            this.bunifuFlatButton1.Iconimage_Selected = null;
            this.bunifuFlatButton1.IconMarginLeft = 0;
            this.bunifuFlatButton1.IconMarginRight = 0;
            this.bunifuFlatButton1.IconRightVisible = true;
            this.bunifuFlatButton1.IconRightZoom = 0D;
            this.bunifuFlatButton1.IconVisible = true;
            this.bunifuFlatButton1.IconZoom = 90D;
            this.bunifuFlatButton1.IsTab = false;
            this.bunifuFlatButton1.Location = new System.Drawing.Point(0, 377);
            this.bunifuFlatButton1.Name = "bunifuFlatButton1";
            this.bunifuFlatButton1.Normalcolor = System.Drawing.Color.DarkGoldenrod;
            this.bunifuFlatButton1.OnHovercolor = System.Drawing.Color.Goldenrod;
            this.bunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton1.selected = false;
            this.bunifuFlatButton1.Size = new System.Drawing.Size(241, 48);
            this.bunifuFlatButton1.TabIndex = 23;
            this.bunifuFlatButton1.Text = "Сменить пользователя";
            this.bunifuFlatButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton1.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton1.Click += new System.EventHandler(this.bunifuFlatButton1_Click);
            // 
            // bunifuFlatButton5
            // 
            this.bunifuFlatButton5.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton5.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.bunifuFlatButton5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton5.BorderRadius = 0;
            this.bunifuFlatButton5.ButtonText = " Личный кабинет";
            this.bunifuFlatButton5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.bunifuFlatButton5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton5.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton5.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton5.Iconimage = global::AutoStorage.Properties.Resources.icons8_User_64;
            this.bunifuFlatButton5.Iconimage_right = null;
            this.bunifuFlatButton5.Iconimage_right_Selected = null;
            this.bunifuFlatButton5.Iconimage_Selected = null;
            this.bunifuFlatButton5.IconMarginLeft = 0;
            this.bunifuFlatButton5.IconMarginRight = 0;
            this.bunifuFlatButton5.IconRightVisible = true;
            this.bunifuFlatButton5.IconRightZoom = 0D;
            this.bunifuFlatButton5.IconVisible = true;
            this.bunifuFlatButton5.IconZoom = 90D;
            this.bunifuFlatButton5.IsTab = false;
            this.bunifuFlatButton5.Location = new System.Drawing.Point(0, 323);
            this.bunifuFlatButton5.Name = "bunifuFlatButton5";
            this.bunifuFlatButton5.Normalcolor = System.Drawing.Color.DarkGoldenrod;
            this.bunifuFlatButton5.OnHovercolor = System.Drawing.Color.Goldenrod;
            this.bunifuFlatButton5.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton5.selected = false;
            this.bunifuFlatButton5.Size = new System.Drawing.Size(241, 48);
            this.bunifuFlatButton5.TabIndex = 18;
            this.bunifuFlatButton5.Text = " Личный кабинет";
            this.bunifuFlatButton5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton5.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton5.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton5.Click += new System.EventHandler(this.bunifuFlatButton5_Click);
            // 
            // bunifuFlatButton4
            // 
            this.bunifuFlatButton4.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton4.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.bunifuFlatButton4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton4.BorderRadius = 0;
            this.bunifuFlatButton4.ButtonText = " Просмотр складов";
            this.bunifuFlatButton4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.bunifuFlatButton4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton4.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton4.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton4.Iconimage = global::AutoStorage.Properties.Resources.icons8_Product_64;
            this.bunifuFlatButton4.Iconimage_right = null;
            this.bunifuFlatButton4.Iconimage_right_Selected = null;
            this.bunifuFlatButton4.Iconimage_Selected = null;
            this.bunifuFlatButton4.IconMarginLeft = 0;
            this.bunifuFlatButton4.IconMarginRight = 0;
            this.bunifuFlatButton4.IconRightVisible = true;
            this.bunifuFlatButton4.IconRightZoom = 0D;
            this.bunifuFlatButton4.IconVisible = true;
            this.bunifuFlatButton4.IconZoom = 90D;
            this.bunifuFlatButton4.IsTab = false;
            this.bunifuFlatButton4.Location = new System.Drawing.Point(0, 269);
            this.bunifuFlatButton4.Name = "bunifuFlatButton4";
            this.bunifuFlatButton4.Normalcolor = System.Drawing.Color.DarkGoldenrod;
            this.bunifuFlatButton4.OnHovercolor = System.Drawing.Color.Goldenrod;
            this.bunifuFlatButton4.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton4.selected = false;
            this.bunifuFlatButton4.Size = new System.Drawing.Size(241, 48);
            this.bunifuFlatButton4.TabIndex = 19;
            this.bunifuFlatButton4.Text = " Просмотр складов";
            this.bunifuFlatButton4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton4.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton4.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton4.Click += new System.EventHandler(this.bunifuFlatButton4_Click);
            // 
            // bunifuFlatButton3
            // 
            this.bunifuFlatButton3.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton3.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.bunifuFlatButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton3.BorderRadius = 0;
            this.bunifuFlatButton3.ButtonText = " Перегрузочные операции";
            this.bunifuFlatButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.bunifuFlatButton3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton3.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton3.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton3.Iconimage = global::AutoStorage.Properties.Resources.icons8_Fork_Lift_64;
            this.bunifuFlatButton3.Iconimage_right = null;
            this.bunifuFlatButton3.Iconimage_right_Selected = null;
            this.bunifuFlatButton3.Iconimage_Selected = null;
            this.bunifuFlatButton3.IconMarginLeft = 0;
            this.bunifuFlatButton3.IconMarginRight = 0;
            this.bunifuFlatButton3.IconRightVisible = true;
            this.bunifuFlatButton3.IconRightZoom = 0D;
            this.bunifuFlatButton3.IconVisible = true;
            this.bunifuFlatButton3.IconZoom = 90D;
            this.bunifuFlatButton3.IsTab = false;
            this.bunifuFlatButton3.Location = new System.Drawing.Point(0, 215);
            this.bunifuFlatButton3.Name = "bunifuFlatButton3";
            this.bunifuFlatButton3.Normalcolor = System.Drawing.Color.DarkGoldenrod;
            this.bunifuFlatButton3.OnHovercolor = System.Drawing.Color.Goldenrod;
            this.bunifuFlatButton3.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton3.selected = false;
            this.bunifuFlatButton3.Size = new System.Drawing.Size(241, 48);
            this.bunifuFlatButton3.TabIndex = 20;
            this.bunifuFlatButton3.Text = " Перегрузочные операции";
            this.bunifuFlatButton3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton3.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton3.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton3.Click += new System.EventHandler(this.bunifuFlatButton3_Click);
            // 
            // bunifuFlatButton2
            // 
            this.bunifuFlatButton2.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton2.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.bunifuFlatButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton2.BorderRadius = 0;
            this.bunifuFlatButton2.ButtonText = " Работа с накладными";
            this.bunifuFlatButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.bunifuFlatButton2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton2.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton2.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton2.Iconimage = global::AutoStorage.Properties.Resources.icons8_Purchase_Order_641;
            this.bunifuFlatButton2.Iconimage_right = null;
            this.bunifuFlatButton2.Iconimage_right_Selected = null;
            this.bunifuFlatButton2.Iconimage_Selected = null;
            this.bunifuFlatButton2.IconMarginLeft = 0;
            this.bunifuFlatButton2.IconMarginRight = 0;
            this.bunifuFlatButton2.IconRightVisible = true;
            this.bunifuFlatButton2.IconRightZoom = 0D;
            this.bunifuFlatButton2.IconVisible = true;
            this.bunifuFlatButton2.IconZoom = 90D;
            this.bunifuFlatButton2.IsTab = false;
            this.bunifuFlatButton2.Location = new System.Drawing.Point(0, 161);
            this.bunifuFlatButton2.Name = "bunifuFlatButton2";
            this.bunifuFlatButton2.Normalcolor = System.Drawing.Color.DarkGoldenrod;
            this.bunifuFlatButton2.OnHovercolor = System.Drawing.Color.Goldenrod;
            this.bunifuFlatButton2.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton2.selected = false;
            this.bunifuFlatButton2.Size = new System.Drawing.Size(241, 48);
            this.bunifuFlatButton2.TabIndex = 21;
            this.bunifuFlatButton2.Text = " Работа с накладными";
            this.bunifuFlatButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton2.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton2.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton2.Click += new System.EventHandler(this.bunifuFlatButton2_Click);
            // 
            // bunifuFlatButton7
            // 
            this.bunifuFlatButton7.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton7.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.bunifuFlatButton7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton7.BorderRadius = 0;
            this.bunifuFlatButton7.ButtonText = " Просмотр накладных";
            this.bunifuFlatButton7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.bunifuFlatButton7, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton7.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton7.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton7.Iconimage = global::AutoStorage.Properties.Resources.icons8_Search_64;
            this.bunifuFlatButton7.Iconimage_right = null;
            this.bunifuFlatButton7.Iconimage_right_Selected = null;
            this.bunifuFlatButton7.Iconimage_Selected = null;
            this.bunifuFlatButton7.IconMarginLeft = 0;
            this.bunifuFlatButton7.IconMarginRight = 0;
            this.bunifuFlatButton7.IconRightVisible = true;
            this.bunifuFlatButton7.IconRightZoom = 0D;
            this.bunifuFlatButton7.IconVisible = true;
            this.bunifuFlatButton7.IconZoom = 90D;
            this.bunifuFlatButton7.IsTab = false;
            this.bunifuFlatButton7.Location = new System.Drawing.Point(0, 107);
            this.bunifuFlatButton7.Name = "bunifuFlatButton7";
            this.bunifuFlatButton7.Normalcolor = System.Drawing.Color.DarkGoldenrod;
            this.bunifuFlatButton7.OnHovercolor = System.Drawing.Color.Goldenrod;
            this.bunifuFlatButton7.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton7.selected = false;
            this.bunifuFlatButton7.Size = new System.Drawing.Size(241, 48);
            this.bunifuFlatButton7.TabIndex = 22;
            this.bunifuFlatButton7.Text = " Просмотр накладных";
            this.bunifuFlatButton7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton7.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton7.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton7.Click += new System.EventHandler(this.bunifuFlatButton7_Click);
            // 
            // btnSlide
            // 
            this.btnSlide.BackColor = System.Drawing.Color.Transparent;
            this.btnSlide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelAnimator.SetDecoration(this.btnSlide, BunifuAnimatorNS.DecorationType.None);
            this.btnSlide.FlatAppearance.BorderSize = 0;
            this.btnSlide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSlide.Image = global::AutoStorage.Properties.Resources.icons8_Menu_321;
            this.btnSlide.Location = new System.Drawing.Point(6, 12);
            this.btnSlide.Name = "btnSlide";
            this.btnSlide.Size = new System.Drawing.Size(36, 37);
            this.btnSlide.TabIndex = 4;
            this.btnSlide.UseVisualStyleBackColor = false;
            this.btnSlide.Click += new System.EventHandler(this.btnSlide_Click);
            // 
            // metroGrid1
            // 
            this.metroGrid1.AllowUserToResizeRows = false;
            this.metroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(196)))), ((int)(((byte)(37)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(57)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.metroGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.panelAnimator.SetDecoration(this.metroGrid1, BunifuAnimatorNS.DecorationType.None);
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(57)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid1.DefaultCellStyle = dataGridViewCellStyle2;
            this.metroGrid1.EnableHeadersVisualStyles = false;
            this.metroGrid1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.Location = new System.Drawing.Point(48, 33);
            this.metroGrid1.Name = "metroGrid1";
            this.metroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(196)))), ((int)(((byte)(37)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(201)))), ((int)(((byte)(57)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.metroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid1.Size = new System.Drawing.Size(660, 523);
            this.metroGrid1.Style = MetroFramework.MetroColorStyle.Yellow;
            this.metroGrid1.TabIndex = 66;
            this.metroGrid1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.metroGrid1_CellContentClick_1);
            this.metroGrid1.MouseEnter += new System.EventHandler(this.metroGrid1_MouseEnter);
            // 
            // dateSearchBox
            // 
            this.dateSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.dateSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.dateSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.dateSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dateSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.dateSearchBox.HintText = "Поиск по дате прибытия";
            this.dateSearchBox.isPassword = false;
            this.dateSearchBox.LineFocusedColor = System.Drawing.Color.DarkOrange;
            this.dateSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.dateSearchBox.LineMouseHoverColor = System.Drawing.Color.DarkGoldenrod;
            this.dateSearchBox.LineThickness = 3;
            this.dateSearchBox.Location = new System.Drawing.Point(60, -1);
            this.dateSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.dateSearchBox.Name = "dateSearchBox";
            this.dateSearchBox.Size = new System.Drawing.Size(192, 27);
            this.dateSearchBox.TabIndex = 18;
            this.dateSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.toolTip1.SetToolTip(this.dateSearchBox, "Формат даты:YYYY-MM-DD");
            this.dateSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateSearchBox_KeyUp);
            // 
            // bunifuGradientPanel2
            // 
            this.bunifuGradientPanel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel2.BackgroundImage")));
            this.bunifuGradientPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel2.Controls.Add(this.menuStrip1);
            this.panelAnimator.SetDecoration(this.bunifuGradientPanel2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuGradientPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuGradientPanel2.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(196)))), ((int)(((byte)(37)))));
            this.bunifuGradientPanel2.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(196)))), ((int)(((byte)(37)))));
            this.bunifuGradientPanel2.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(196)))), ((int)(((byte)(37)))));
            this.bunifuGradientPanel2.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(196)))), ((int)(((byte)(37)))));
            this.bunifuGradientPanel2.Location = new System.Drawing.Point(20, 60);
            this.bunifuGradientPanel2.Name = "bunifuGradientPanel2";
            this.bunifuGradientPanel2.Quality = 10;
            this.bunifuGradientPanel2.Size = new System.Drawing.Size(1272, 48);
            this.bunifuGradientPanel2.TabIndex = 37;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.menuStrip1, BunifuAnimatorNS.DecorationType.None);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.обновитьToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStrip1.Size = new System.Drawing.Size(1272, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // обновитьToolStripMenuItem
            // 
            this.обновитьToolStripMenuItem.Name = "обновитьToolStripMenuItem";
            this.обновитьToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.обновитьToolStripMenuItem.Text = "Обновить";
            this.обновитьToolStripMenuItem.Click += new System.EventHandler(this.обновитьToolStripMenuItem_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            this.справкаToolStripMenuItem.Click += new System.EventHandler(this.справкаToolStripMenuItem_Click);
            // 
            // backToWorkWithBills
            // 
            this.backToWorkWithBills.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("backToWorkWithBills.BackgroundImage")));
            this.backToWorkWithBills.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelAnimator.SetDecoration(this.backToWorkWithBills, BunifuAnimatorNS.DecorationType.None);
            this.backToWorkWithBills.FlatAppearance.BorderSize = 0;
            this.backToWorkWithBills.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backToWorkWithBills.Location = new System.Drawing.Point(13, 20);
            this.backToWorkWithBills.Name = "backToWorkWithBills";
            this.backToWorkWithBills.Size = new System.Drawing.Size(32, 32);
            this.backToWorkWithBills.TabIndex = 36;
            this.backToWorkWithBills.UseVisualStyleBackColor = true;
            this.backToWorkWithBills.Click += new System.EventHandler(this.backToWorkWithBills_Click_1);
            // 
            // redactPurchaseBills
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScrollMargin = new System.Drawing.Size(50, 50);
            this.ClientSize = new System.Drawing.Size(1312, 737);
            this.Controls.Add(this.bunifuGradientPanel3);
            this.Controls.Add(this.bunifuGradientPanel2);
            this.Controls.Add(this.backToWorkWithBills);
            this.panelAnimator.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.Name = "redactPurchaseBills";
            this.Text = "   Редактирование приходной накладной";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.redactPurchaseInvoice_FormClosing);
            this.Load += new System.EventHandler(this.redactPurchaseInvoice_Load);
            this.bunifuGradientPanel3.ResumeLayout(false);
            this.contentSlideMenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.metroTabControl2.ResumeLayout(false);
            this.metroTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn2)).EndInit();
            this.metroTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn22)).EndInit();
            this.metroTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBtn32)).EndInit();
            this.loaderSliderMenu.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.metroTabControl3.ResumeLayout(false);
            this.metroTabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vilBtn)).EndInit();
            this.metroTabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kovshBtn)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgList)).EndInit();
            this.slidemenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).EndInit();
            this.bunifuGradientPanel2.ResumeLayout(false);
            this.bunifuGradientPanel2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSlide;
        private BunifuAnimatorNS.BunifuTransition panelAnimator;
        private Bunifu.Framework.UI.BunifuGradientPanel slidemenu;
        private MetroFramework.Controls.MetroGrid metroGrid1;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem обновитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel2;
        private System.Windows.Forms.Button backToWorkWithBills;
        private Bunifu.Framework.UI.BunifuFlatButton updPurshaseBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox fromPhoneBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox fromEmailBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox fromCityBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox companyFromBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox fromAdresBox;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private Bunifu.Framework.UI.BunifuMaterialTextbox toPhoneBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox toEmailBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox toCityBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox companyToBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox toAdresBox;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private System.Windows.Forms.GroupBox groupBox3;
        private MetroFramework.Controls.MetroLabel metroLabel17;
        private Bunifu.Framework.UI.BunifuMaterialTextbox totalDaysBox;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private Bunifu.Framework.UI.BunifuMaterialTextbox keepingForWeekPriceBox;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroDateTime metroDateTime2;
        private MetroFramework.Controls.MetroLabel metroLabel28;
        private Bunifu.Framework.UI.BunifuMaterialTextbox cubageBox;
        private MetroFramework.Controls.MetroLabel metroLabel26;
        private Bunifu.Framework.UI.BunifuMaterialTextbox keepingSumBox;
        private Bunifu.Framework.UI.BunifuFlatButton updateGoodsBtn;
        public System.Windows.Forms.PictureBox imgList;
        private MetroFramework.Controls.MetroLabel metroLabel20;
        private MetroFramework.Controls.MetroDateTime metroDateTime1;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private Bunifu.Framework.UI.BunifuMaterialTextbox weightBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox goodsNameBox;
        private MetroFramework.Controls.MetroComboBox goodsTypeBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox idBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox priceSearchBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox typeSearchBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox dateSearchBox;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox5;
        private Bunifu.Framework.UI.BunifuMaterialTextbox transportPriceBox;
        private MetroFramework.Controls.MetroLabel metroLabel27;
        private MetroFramework.Controls.MetroLabel metroLabel23;
        private Bunifu.Framework.UI.BunifuMaterialTextbox loadTypeBox;
        private MetroFramework.Controls.MetroLabel metroLabel21;
        private Bunifu.Framework.UI.BunifuMaterialTextbox carcasMaterialBox;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private Bunifu.Framework.UI.BunifuMaterialTextbox heighBox;
        private MetroFramework.Controls.MetroLabel metroLabel22;
        private Bunifu.Framework.UI.BunifuMaterialTextbox amountBox;
        private MetroFramework.Controls.MetroLabel metroLabel24;
        private MetroFramework.Controls.MetroLabel metroLabel25;
        private MetroFramework.Controls.MetroLabel metroLabel29;
        private Bunifu.Framework.UI.BunifuMaterialTextbox capacityBox;
        private MetroFramework.Controls.MetroLabel metroLabel30;
        private Bunifu.Framework.UI.BunifuMaterialTextbox carcasTypeBox;
        private MetroFramework.Controls.MetroLabel metroLabel31;
        private Bunifu.Framework.UI.BunifuMaterialTextbox carryingBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox lengthBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox widthBox;
        private System.Windows.Forms.GroupBox groupBox6;
        private Bunifu.Framework.UI.BunifuMaterialTextbox loaderTonnageBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox loaderCapacityBox;
        private MetroFramework.Controls.MetroPanel loaderSliderMenu;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
        private MetroFramework.Controls.MetroTabControl metroTabControl3;
        private MetroFramework.Controls.MetroTabPage metroTabPage7;
        private Bunifu.Framework.UI.BunifuImageButton vilBtn;
        private MetroFramework.Controls.MetroTabPage metroTabPage8;
        private Bunifu.Framework.UI.BunifuImageButton kovshBtn;
        private MetroFramework.Controls.MetroPanel contentSlideMenu;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button exitBtn;
        private MetroFramework.Controls.MetroTabControl metroTabControl2;
        private MetroFramework.Controls.MetroTabPage metroTabPage4;
        private Bunifu.Framework.UI.BunifuImageButton imgBtn3;
        private Bunifu.Framework.UI.BunifuImageButton imgBtn4;
        private Bunifu.Framework.UI.BunifuImageButton imgBtn1;
        private Bunifu.Framework.UI.BunifuImageButton imgBtn2;
        private MetroFramework.Controls.MetroTabPage metroTabPage5;
        private Bunifu.Framework.UI.BunifuImageButton imgBtn21;
        private Bunifu.Framework.UI.BunifuImageButton imgBtn22;
        private MetroFramework.Controls.MetroTabPage metroTabPage6;
        private Bunifu.Framework.UI.BunifuImageButton imgBtn31;
        private Bunifu.Framework.UI.BunifuImageButton imgBtn33;
        private Bunifu.Framework.UI.BunifuImageButton imgBtn32;
        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private Bunifu.Framework.UI.BunifuFlatButton updLoaderBtn;
        private Bunifu.Framework.UI.BunifuFlatButton updTransrtBtn;
        private MetroFramework.Controls.MetroTabControl metroTabControl4;
        private Bunifu.Framework.UI.BunifuMaterialTextbox loaderTypeBox;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton1;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton5;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton4;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton3;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton2;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton7;

    }
}