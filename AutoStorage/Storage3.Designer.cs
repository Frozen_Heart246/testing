﻿namespace AutoStorage
{
    partial class Storage3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            BunifuAnimatorNS.Animation animation3 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Storage3));
            this.goodsNameBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.priceSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.typeSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.idBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.fromPhoneBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.fromEmailBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.fromCityBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.companyFromBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.fromAdresBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.toPhoneBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.toEmailBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.toCityBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.companyToBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.toAdresBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroGrid1 = new MetroFramework.Controls.MetroGrid();
            this.dateSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuGradientPanel2 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.обновитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goodsTypeBox = new MetroFramework.Controls.MetroComboBox();
            this.updateBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.fromStorage3Btn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.congestionPG = new Bunifu.Framework.UI.BunifuCircleProgressbar();
            this.bunifuMaterialTextbox1 = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.totalDaysBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.keepingForWeekPriceBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.metroDateTime2 = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel28 = new MetroFramework.Controls.MetroLabel();
            this.store3CongPickerBox = new MetroFramework.Controls.MetroTextBox();
            this.metroGrid2 = new MetroFramework.Controls.MetroGrid();
            this.panelAnimator = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cubageBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel26 = new MetroFramework.Controls.MetroLabel();
            this.keepingSumBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.imgList = new System.Windows.Forms.PictureBox();
            this.metroLabel20 = new MetroFramework.Controls.MetroLabel();
            this.metroDateTime1 = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.weightBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuGradientPanel3 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).BeginInit();
            this.bunifuGradientPanel2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid2)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgList)).BeginInit();
            this.bunifuGradientPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // goodsNameBox
            // 
            this.goodsNameBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.goodsNameBox, BunifuAnimatorNS.DecorationType.None);
            this.goodsNameBox.Enabled = false;
            this.goodsNameBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.goodsNameBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.goodsNameBox.HintForeColor = System.Drawing.Color.Empty;
            this.goodsNameBox.HintText = "";
            this.goodsNameBox.isPassword = false;
            this.goodsNameBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.goodsNameBox.LineIdleColor = System.Drawing.Color.Gray;
            this.goodsNameBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.goodsNameBox.LineThickness = 3;
            this.goodsNameBox.Location = new System.Drawing.Point(218, 28);
            this.goodsNameBox.Margin = new System.Windows.Forms.Padding(4);
            this.goodsNameBox.Name = "goodsNameBox";
            this.goodsNameBox.Size = new System.Drawing.Size(203, 29);
            this.goodsNameBox.TabIndex = 17;
            this.goodsNameBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // priceSearchBox
            // 
            this.priceSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.priceSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.priceSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.priceSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.priceSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.priceSearchBox.HintText = "Поиск по цене";
            this.priceSearchBox.isPassword = false;
            this.priceSearchBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.priceSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.priceSearchBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.priceSearchBox.LineThickness = 3;
            this.priceSearchBox.Location = new System.Drawing.Point(501, 4);
            this.priceSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.priceSearchBox.Name = "priceSearchBox";
            this.priceSearchBox.Size = new System.Drawing.Size(192, 27);
            this.priceSearchBox.TabIndex = 72;
            this.priceSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.priceSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.priceSearchBox_KeyUp);
            // 
            // typeSearchBox
            // 
            this.typeSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.typeSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.typeSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.typeSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.typeSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.typeSearchBox.HintText = "Поиск по отправляющей комании";
            this.typeSearchBox.isPassword = false;
            this.typeSearchBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.typeSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.typeSearchBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.typeSearchBox.LineThickness = 3;
            this.typeSearchBox.Location = new System.Drawing.Point(261, 4);
            this.typeSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.typeSearchBox.Name = "typeSearchBox";
            this.typeSearchBox.Size = new System.Drawing.Size(192, 27);
            this.typeSearchBox.TabIndex = 71;
            this.typeSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.typeSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.typeSearchBox_KeyUp);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.idBox);
            this.groupBox1.Controls.Add(this.fromPhoneBox);
            this.groupBox1.Controls.Add(this.fromEmailBox);
            this.groupBox1.Controls.Add(this.fromCityBox);
            this.groupBox1.Controls.Add(this.companyFromBox);
            this.groupBox1.Controls.Add(this.fromAdresBox);
            this.groupBox1.Controls.Add(this.metroLabel5);
            this.groupBox1.Controls.Add(this.metroLabel4);
            this.groupBox1.Controls.Add(this.metroLabel3);
            this.groupBox1.Controls.Add(this.metroLabel2);
            this.groupBox1.Controls.Add(this.metroLabel1);
            this.panelAnimator.SetDecoration(this.groupBox1, BunifuAnimatorNS.DecorationType.None);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(812, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(381, 206);
            this.groupBox1.TabIndex = 67;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Отправитель";
            // 
            // idBox
            // 
            this.idBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.idBox, BunifuAnimatorNS.DecorationType.None);
            this.idBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.idBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.idBox.HintForeColor = System.Drawing.Color.Empty;
            this.idBox.HintText = "";
            this.idBox.isPassword = false;
            this.idBox.LineFocusedColor = System.Drawing.Color.Blue;
            this.idBox.LineIdleColor = System.Drawing.Color.Gray;
            this.idBox.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.idBox.LineThickness = 3;
            this.idBox.Location = new System.Drawing.Point(25, 68);
            this.idBox.Margin = new System.Windows.Forms.Padding(4);
            this.idBox.Name = "idBox";
            this.idBox.Size = new System.Drawing.Size(50, 33);
            this.idBox.TabIndex = 18;
            this.idBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.idBox.Visible = false;
            // 
            // fromPhoneBox
            // 
            this.fromPhoneBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.fromPhoneBox, BunifuAnimatorNS.DecorationType.None);
            this.fromPhoneBox.Enabled = false;
            this.fromPhoneBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.fromPhoneBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.fromPhoneBox.HintForeColor = System.Drawing.Color.Empty;
            this.fromPhoneBox.HintText = "";
            this.fromPhoneBox.isPassword = false;
            this.fromPhoneBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.fromPhoneBox.LineIdleColor = System.Drawing.Color.Gray;
            this.fromPhoneBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.fromPhoneBox.LineThickness = 3;
            this.fromPhoneBox.Location = new System.Drawing.Point(183, 125);
            this.fromPhoneBox.Margin = new System.Windows.Forms.Padding(4);
            this.fromPhoneBox.Name = "fromPhoneBox";
            this.fromPhoneBox.Size = new System.Drawing.Size(192, 27);
            this.fromPhoneBox.TabIndex = 13;
            this.fromPhoneBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // fromEmailBox
            // 
            this.fromEmailBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.fromEmailBox, BunifuAnimatorNS.DecorationType.None);
            this.fromEmailBox.Enabled = false;
            this.fromEmailBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.fromEmailBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.fromEmailBox.HintForeColor = System.Drawing.Color.Empty;
            this.fromEmailBox.HintText = "";
            this.fromEmailBox.isPassword = false;
            this.fromEmailBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.fromEmailBox.LineIdleColor = System.Drawing.Color.Gray;
            this.fromEmailBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.fromEmailBox.LineThickness = 3;
            this.fromEmailBox.Location = new System.Drawing.Point(183, 151);
            this.fromEmailBox.Margin = new System.Windows.Forms.Padding(4);
            this.fromEmailBox.Name = "fromEmailBox";
            this.fromEmailBox.Size = new System.Drawing.Size(192, 27);
            this.fromEmailBox.TabIndex = 14;
            this.fromEmailBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // fromCityBox
            // 
            this.fromCityBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.fromCityBox, BunifuAnimatorNS.DecorationType.None);
            this.fromCityBox.Enabled = false;
            this.fromCityBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.fromCityBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.fromCityBox.HintForeColor = System.Drawing.Color.Empty;
            this.fromCityBox.HintText = "";
            this.fromCityBox.isPassword = false;
            this.fromCityBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.fromCityBox.LineIdleColor = System.Drawing.Color.Gray;
            this.fromCityBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.fromCityBox.LineThickness = 3;
            this.fromCityBox.Location = new System.Drawing.Point(183, 93);
            this.fromCityBox.Margin = new System.Windows.Forms.Padding(4);
            this.fromCityBox.Name = "fromCityBox";
            this.fromCityBox.Size = new System.Drawing.Size(192, 27);
            this.fromCityBox.TabIndex = 15;
            this.fromCityBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // companyFromBox
            // 
            this.companyFromBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.companyFromBox, BunifuAnimatorNS.DecorationType.None);
            this.companyFromBox.Enabled = false;
            this.companyFromBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.companyFromBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.companyFromBox.HintForeColor = System.Drawing.Color.Empty;
            this.companyFromBox.HintText = "";
            this.companyFromBox.isPassword = false;
            this.companyFromBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.companyFromBox.LineIdleColor = System.Drawing.Color.Gray;
            this.companyFromBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.companyFromBox.LineThickness = 3;
            this.companyFromBox.Location = new System.Drawing.Point(183, 29);
            this.companyFromBox.Margin = new System.Windows.Forms.Padding(4);
            this.companyFromBox.Name = "companyFromBox";
            this.companyFromBox.Size = new System.Drawing.Size(192, 27);
            this.companyFromBox.TabIndex = 1;
            this.companyFromBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // fromAdresBox
            // 
            this.fromAdresBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.fromAdresBox, BunifuAnimatorNS.DecorationType.None);
            this.fromAdresBox.Enabled = false;
            this.fromAdresBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.fromAdresBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.fromAdresBox.HintForeColor = System.Drawing.Color.Empty;
            this.fromAdresBox.HintText = "";
            this.fromAdresBox.isPassword = false;
            this.fromAdresBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.fromAdresBox.LineIdleColor = System.Drawing.Color.Gray;
            this.fromAdresBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.fromAdresBox.LineThickness = 3;
            this.fromAdresBox.Location = new System.Drawing.Point(183, 58);
            this.fromAdresBox.Margin = new System.Windows.Forms.Padding(4);
            this.fromAdresBox.Name = "fromAdresBox";
            this.fromAdresBox.Size = new System.Drawing.Size(192, 27);
            this.fromAdresBox.TabIndex = 17;
            this.fromAdresBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel5, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel5.Location = new System.Drawing.Point(135, 159);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(41, 19);
            this.metroLabel5.TabIndex = 12;
            this.metroLabel5.Text = "Email";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel4, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel4.Enabled = false;
            this.metroLabel4.Location = new System.Drawing.Point(106, 133);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(70, 19);
            this.metroLabel4.TabIndex = 8;
            this.metroLabel4.Text = "Телефоон";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel3, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel3.Enabled = false;
            this.metroLabel3.Location = new System.Drawing.Point(130, 101);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(46, 19);
            this.metroLabel3.TabIndex = 9;
            this.metroLabel3.Text = "Город";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel2, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel2.Enabled = false;
            this.metroLabel2.Location = new System.Drawing.Point(124, 66);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(46, 19);
            this.metroLabel2.TabIndex = 10;
            this.metroLabel2.Text = "Адрес";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel1, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel1.Enabled = false;
            this.metroLabel1.Location = new System.Drawing.Point(6, 37);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(170, 19);
            this.metroLabel1.TabIndex = 11;
            this.metroLabel1.Text = "Отправляющая компания";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.toPhoneBox);
            this.groupBox2.Controls.Add(this.toEmailBox);
            this.groupBox2.Controls.Add(this.toCityBox);
            this.groupBox2.Controls.Add(this.companyToBox);
            this.groupBox2.Controls.Add(this.toAdresBox);
            this.groupBox2.Controls.Add(this.metroLabel6);
            this.groupBox2.Controls.Add(this.metroLabel7);
            this.groupBox2.Controls.Add(this.metroLabel8);
            this.groupBox2.Controls.Add(this.metroLabel9);
            this.groupBox2.Controls.Add(this.metroLabel10);
            this.panelAnimator.SetDecoration(this.groupBox2, BunifuAnimatorNS.DecorationType.None);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(1199, 25);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(381, 206);
            this.groupBox2.TabIndex = 68;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Получатель";
            // 
            // toPhoneBox
            // 
            this.toPhoneBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.toPhoneBox, BunifuAnimatorNS.DecorationType.None);
            this.toPhoneBox.Enabled = false;
            this.toPhoneBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.toPhoneBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.toPhoneBox.HintForeColor = System.Drawing.Color.Empty;
            this.toPhoneBox.HintText = "";
            this.toPhoneBox.isPassword = false;
            this.toPhoneBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.toPhoneBox.LineIdleColor = System.Drawing.Color.Gray;
            this.toPhoneBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.toPhoneBox.LineThickness = 3;
            this.toPhoneBox.Location = new System.Drawing.Point(172, 127);
            this.toPhoneBox.Margin = new System.Windows.Forms.Padding(4);
            this.toPhoneBox.Name = "toPhoneBox";
            this.toPhoneBox.Size = new System.Drawing.Size(192, 27);
            this.toPhoneBox.TabIndex = 18;
            this.toPhoneBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // toEmailBox
            // 
            this.toEmailBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.toEmailBox, BunifuAnimatorNS.DecorationType.None);
            this.toEmailBox.Enabled = false;
            this.toEmailBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.toEmailBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.toEmailBox.HintForeColor = System.Drawing.Color.Empty;
            this.toEmailBox.HintText = "";
            this.toEmailBox.isPassword = false;
            this.toEmailBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.toEmailBox.LineIdleColor = System.Drawing.Color.Gray;
            this.toEmailBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.toEmailBox.LineThickness = 3;
            this.toEmailBox.Location = new System.Drawing.Point(172, 160);
            this.toEmailBox.Margin = new System.Windows.Forms.Padding(4);
            this.toEmailBox.Name = "toEmailBox";
            this.toEmailBox.Size = new System.Drawing.Size(192, 27);
            this.toEmailBox.TabIndex = 19;
            this.toEmailBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // toCityBox
            // 
            this.toCityBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.toCityBox, BunifuAnimatorNS.DecorationType.None);
            this.toCityBox.Enabled = false;
            this.toCityBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.toCityBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.toCityBox.HintForeColor = System.Drawing.Color.Empty;
            this.toCityBox.HintText = "";
            this.toCityBox.isPassword = false;
            this.toCityBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.toCityBox.LineIdleColor = System.Drawing.Color.Gray;
            this.toCityBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.toCityBox.LineThickness = 3;
            this.toCityBox.Location = new System.Drawing.Point(172, 95);
            this.toCityBox.Margin = new System.Windows.Forms.Padding(4);
            this.toCityBox.Name = "toCityBox";
            this.toCityBox.Size = new System.Drawing.Size(192, 27);
            this.toCityBox.TabIndex = 20;
            this.toCityBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // companyToBox
            // 
            this.companyToBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.companyToBox, BunifuAnimatorNS.DecorationType.None);
            this.companyToBox.Enabled = false;
            this.companyToBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.companyToBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.companyToBox.HintForeColor = System.Drawing.Color.Empty;
            this.companyToBox.HintText = "";
            this.companyToBox.isPassword = false;
            this.companyToBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.companyToBox.LineIdleColor = System.Drawing.Color.Gray;
            this.companyToBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.companyToBox.LineThickness = 3;
            this.companyToBox.Location = new System.Drawing.Point(172, 31);
            this.companyToBox.Margin = new System.Windows.Forms.Padding(4);
            this.companyToBox.Name = "companyToBox";
            this.companyToBox.Size = new System.Drawing.Size(192, 27);
            this.companyToBox.TabIndex = 21;
            this.companyToBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // toAdresBox
            // 
            this.toAdresBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.toAdresBox, BunifuAnimatorNS.DecorationType.None);
            this.toAdresBox.Enabled = false;
            this.toAdresBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.toAdresBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.toAdresBox.HintForeColor = System.Drawing.Color.Empty;
            this.toAdresBox.HintText = "";
            this.toAdresBox.isPassword = false;
            this.toAdresBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.toAdresBox.LineIdleColor = System.Drawing.Color.Gray;
            this.toAdresBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.toAdresBox.LineThickness = 3;
            this.toAdresBox.Location = new System.Drawing.Point(172, 60);
            this.toAdresBox.Margin = new System.Windows.Forms.Padding(4);
            this.toAdresBox.Name = "toAdresBox";
            this.toAdresBox.Size = new System.Drawing.Size(192, 27);
            this.toAdresBox.TabIndex = 22;
            this.toAdresBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel6, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel6.Location = new System.Drawing.Point(124, 168);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(41, 19);
            this.metroLabel6.TabIndex = 17;
            this.metroLabel6.Text = "Email";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel7, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel7.Enabled = false;
            this.metroLabel7.Location = new System.Drawing.Point(95, 142);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(70, 19);
            this.metroLabel7.TabIndex = 13;
            this.metroLabel7.Text = "Телефоон";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel8, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel8.Enabled = false;
            this.metroLabel8.Location = new System.Drawing.Point(119, 110);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(46, 19);
            this.metroLabel8.TabIndex = 14;
            this.metroLabel8.Text = "Город";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel9, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel9.Enabled = false;
            this.metroLabel9.Location = new System.Drawing.Point(113, 75);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(52, 19);
            this.metroLabel9.TabIndex = 15;
            this.metroLabel9.Text = "Адресс";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel10, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel10.Enabled = false;
            this.metroLabel10.Location = new System.Drawing.Point(11, 46);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(154, 19);
            this.metroLabel10.TabIndex = 16;
            this.metroLabel10.Text = "Получающая компания";
            // 
            // metroGrid1
            // 
            this.metroGrid1.AllowUserToResizeRows = false;
            this.metroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(173)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(201)))), ((int)(((byte)(206)))));
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.metroGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.panelAnimator.SetDecoration(this.metroGrid1, BunifuAnimatorNS.DecorationType.None);
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(201)))), ((int)(((byte)(206)))));
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid1.DefaultCellStyle = dataGridViewCellStyle14;
            this.metroGrid1.EnableHeadersVisualStyles = false;
            this.metroGrid1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.Location = new System.Drawing.Point(3, 33);
            this.metroGrid1.Name = "metroGrid1";
            this.metroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(173)))));
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(201)))), ((int)(((byte)(206)))));
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.metroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid1.Size = new System.Drawing.Size(705, 505);
            this.metroGrid1.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroGrid1.TabIndex = 66;
            this.metroGrid1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.metroGrid1_CellContentClick);
            // 
            // dateSearchBox
            // 
            this.dateSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.dateSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.dateSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.dateSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dateSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.dateSearchBox.HintText = "Поиск по дате прибытия";
            this.dateSearchBox.isPassword = false;
            this.dateSearchBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.dateSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.dateSearchBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.dateSearchBox.LineThickness = 3;
            this.dateSearchBox.Location = new System.Drawing.Point(21, 4);
            this.dateSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.dateSearchBox.Name = "dateSearchBox";
            this.dateSearchBox.Size = new System.Drawing.Size(192, 27);
            this.dateSearchBox.TabIndex = 18;
            this.dateSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.toolTip1.SetToolTip(this.dateSearchBox, "Формат даты:YYYY-MM-DD");
            this.dateSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateSearchBox_KeyUp);
            // 
            // bunifuGradientPanel2
            // 
            this.bunifuGradientPanel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel2.BackgroundImage")));
            this.bunifuGradientPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel2.Controls.Add(this.menuStrip1);
            this.panelAnimator.SetDecoration(this.bunifuGradientPanel2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuGradientPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuGradientPanel2.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.bunifuGradientPanel2.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.bunifuGradientPanel2.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.bunifuGradientPanel2.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.bunifuGradientPanel2.Location = new System.Drawing.Point(20, 60);
            this.bunifuGradientPanel2.Name = "bunifuGradientPanel2";
            this.bunifuGradientPanel2.Quality = 10;
            this.bunifuGradientPanel2.Size = new System.Drawing.Size(1272, 48);
            this.bunifuGradientPanel2.TabIndex = 43;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.menuStrip1, BunifuAnimatorNS.DecorationType.None);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.обновитьToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStrip1.Size = new System.Drawing.Size(1272, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // обновитьToolStripMenuItem
            // 
            this.обновитьToolStripMenuItem.Name = "обновитьToolStripMenuItem";
            this.обновитьToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.обновитьToolStripMenuItem.Text = "Обновить";
            // 
            // goodsTypeBox
            // 
            this.panelAnimator.SetDecoration(this.goodsTypeBox, BunifuAnimatorNS.DecorationType.None);
            this.goodsTypeBox.Enabled = false;
            this.goodsTypeBox.FormattingEnabled = true;
            this.goodsTypeBox.ItemHeight = 23;
            this.goodsTypeBox.Items.AddRange(new object[] {
            "Продовольственные товары",
            "Электронные товары",
            "Товары с температурным режимом",
            "Строительные материалы",
            "Сыпучие"});
            this.goodsTypeBox.Location = new System.Drawing.Point(618, 102);
            this.goodsTypeBox.Name = "goodsTypeBox";
            this.goodsTypeBox.Size = new System.Drawing.Size(253, 29);
            this.goodsTypeBox.TabIndex = 16;
            this.goodsTypeBox.Tag = "Тип товаров";
            this.goodsTypeBox.UseSelectable = true;
            // 
            // updateBtn
            // 
            this.updateBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.updateBtn.BackColor = System.Drawing.Color.Teal;
            this.updateBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.updateBtn.BorderRadius = 0;
            this.updateBtn.ButtonText = "                          Обновить";
            this.updateBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.updateBtn, BunifuAnimatorNS.DecorationType.None);
            this.updateBtn.DisabledColor = System.Drawing.Color.Gray;
            this.updateBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.updateBtn.Iconimage = null;
            this.updateBtn.Iconimage_right = null;
            this.updateBtn.Iconimage_right_Selected = null;
            this.updateBtn.Iconimage_Selected = null;
            this.updateBtn.IconMarginLeft = 0;
            this.updateBtn.IconMarginRight = 0;
            this.updateBtn.IconRightVisible = true;
            this.updateBtn.IconRightZoom = 0D;
            this.updateBtn.IconVisible = true;
            this.updateBtn.IconZoom = 90D;
            this.updateBtn.IsTab = false;
            this.updateBtn.Location = new System.Drawing.Point(687, 376);
            this.updateBtn.Name = "updateBtn";
            this.updateBtn.Normalcolor = System.Drawing.Color.Teal;
            this.updateBtn.OnHovercolor = System.Drawing.Color.DeepSkyBlue;
            this.updateBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.updateBtn.selected = false;
            this.updateBtn.Size = new System.Drawing.Size(237, 50);
            this.updateBtn.TabIndex = 44;
            this.updateBtn.Text = "                          Обновить";
            this.updateBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.updateBtn.Textcolor = System.Drawing.Color.White;
            this.updateBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // fromStorage3Btn
            // 
            this.fromStorage3Btn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.fromStorage3Btn.BackColor = System.Drawing.Color.Teal;
            this.fromStorage3Btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fromStorage3Btn.BorderRadius = 0;
            this.fromStorage3Btn.ButtonText = "               Удалить со склада";
            this.fromStorage3Btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.fromStorage3Btn, BunifuAnimatorNS.DecorationType.None);
            this.fromStorage3Btn.DisabledColor = System.Drawing.Color.Gray;
            this.fromStorage3Btn.Iconcolor = System.Drawing.Color.Transparent;
            this.fromStorage3Btn.Iconimage = null;
            this.fromStorage3Btn.Iconimage_right = null;
            this.fromStorage3Btn.Iconimage_right_Selected = null;
            this.fromStorage3Btn.Iconimage_Selected = null;
            this.fromStorage3Btn.IconMarginLeft = 0;
            this.fromStorage3Btn.IconMarginRight = 0;
            this.fromStorage3Btn.IconRightVisible = true;
            this.fromStorage3Btn.IconRightZoom = 0D;
            this.fromStorage3Btn.IconVisible = true;
            this.fromStorage3Btn.IconZoom = 90D;
            this.fromStorage3Btn.IsTab = false;
            this.fromStorage3Btn.Location = new System.Drawing.Point(687, 322);
            this.fromStorage3Btn.Name = "fromStorage3Btn";
            this.fromStorage3Btn.Normalcolor = System.Drawing.Color.Teal;
            this.fromStorage3Btn.OnHovercolor = System.Drawing.Color.DeepSkyBlue;
            this.fromStorage3Btn.OnHoverTextColor = System.Drawing.Color.White;
            this.fromStorage3Btn.selected = false;
            this.fromStorage3Btn.Size = new System.Drawing.Size(237, 50);
            this.fromStorage3Btn.TabIndex = 43;
            this.fromStorage3Btn.Text = "               Удалить со склада";
            this.fromStorage3Btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.fromStorage3Btn.Textcolor = System.Drawing.Color.White;
            this.fromStorage3Btn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fromStorage3Btn.Click += new System.EventHandler(this.fromStorage3Btn_Click);
            // 
            // metroLabel18
            // 
            this.metroLabel18.AutoSize = true;
            this.metroLabel18.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.metroLabel18, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel18.Location = new System.Drawing.Point(513, 345);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(130, 19);
            this.metroLabel18.TabIndex = 41;
            this.metroLabel18.Text = "Склад заполнен на:";
            // 
            // congestionPG
            // 
            this.congestionPG.animated = false;
            this.congestionPG.animationIterval = 5;
            this.congestionPG.animationSpeed = 300;
            this.congestionPG.BackColor = System.Drawing.Color.White;
            this.congestionPG.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("congestionPG.BackgroundImage")));
            this.panelAnimator.SetDecoration(this.congestionPG, BunifuAnimatorNS.DecorationType.BottomMirror);
            this.congestionPG.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F);
            this.congestionPG.ForeColor = System.Drawing.Color.Teal;
            this.congestionPG.LabelVisible = true;
            this.congestionPG.LineProgressThickness = 8;
            this.congestionPG.LineThickness = 5;
            this.congestionPG.Location = new System.Drawing.Point(455, 282);
            this.congestionPG.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.congestionPG.MaxValue = 400;
            this.congestionPG.Name = "congestionPG";
            this.congestionPG.ProgressBackColor = System.Drawing.Color.Gray;
            this.congestionPG.ProgressColor = System.Drawing.Color.Teal;
            this.congestionPG.Size = new System.Drawing.Size(227, 227);
            this.congestionPG.TabIndex = 39;
            this.congestionPG.Value = 0;
            // 
            // bunifuMaterialTextbox1
            // 
            this.bunifuMaterialTextbox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.bunifuMaterialTextbox1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuMaterialTextbox1.Enabled = false;
            this.bunifuMaterialTextbox1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.bunifuMaterialTextbox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuMaterialTextbox1.HintForeColor = System.Drawing.Color.Empty;
            this.bunifuMaterialTextbox1.HintText = "";
            this.bunifuMaterialTextbox1.isPassword = false;
            this.bunifuMaterialTextbox1.LineFocusedColor = System.Drawing.Color.Blue;
            this.bunifuMaterialTextbox1.LineIdleColor = System.Drawing.Color.Gray;
            this.bunifuMaterialTextbox1.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.bunifuMaterialTextbox1.LineThickness = 3;
            this.bunifuMaterialTextbox1.Location = new System.Drawing.Point(577, 31);
            this.bunifuMaterialTextbox1.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuMaterialTextbox1.Name = "bunifuMaterialTextbox1";
            this.bunifuMaterialTextbox1.Size = new System.Drawing.Size(35, 29);
            this.bunifuMaterialTextbox1.TabIndex = 36;
            this.bunifuMaterialTextbox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.bunifuMaterialTextbox1.Visible = false;
            // 
            // metroLabel17
            // 
            this.metroLabel17.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.metroLabel17.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel17, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel17.Location = new System.Drawing.Point(713, 225);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(90, 19);
            this.metroLabel17.TabIndex = 35;
            this.metroLabel17.Text = "Кол-во дней ";
            // 
            // totalDaysBox
            // 
            this.totalDaysBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.totalDaysBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.totalDaysBox, BunifuAnimatorNS.DecorationType.None);
            this.totalDaysBox.Enabled = false;
            this.totalDaysBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.totalDaysBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.totalDaysBox.HintForeColor = System.Drawing.Color.Empty;
            this.totalDaysBox.HintText = "";
            this.totalDaysBox.isPassword = false;
            this.totalDaysBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.totalDaysBox.LineIdleColor = System.Drawing.Color.Gray;
            this.totalDaysBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.totalDaysBox.LineThickness = 3;
            this.totalDaysBox.Location = new System.Drawing.Point(810, 207);
            this.totalDaysBox.Margin = new System.Windows.Forms.Padding(4);
            this.totalDaysBox.Name = "totalDaysBox";
            this.totalDaysBox.Size = new System.Drawing.Size(78, 29);
            this.totalDaysBox.TabIndex = 34;
            this.totalDaysBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // metroLabel16
            // 
            this.metroLabel16.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.metroLabel16.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel16, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel16.Location = new System.Drawing.Point(435, 224);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(181, 19);
            this.metroLabel16.TabIndex = 33;
            this.metroLabel16.Text = "Цена хранение за 1 неделю";
            // 
            // keepingForWeekPriceBox
            // 
            this.keepingForWeekPriceBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.keepingForWeekPriceBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.keepingForWeekPriceBox, BunifuAnimatorNS.DecorationType.None);
            this.keepingForWeekPriceBox.Enabled = false;
            this.keepingForWeekPriceBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.keepingForWeekPriceBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.keepingForWeekPriceBox.HintForeColor = System.Drawing.Color.Empty;
            this.keepingForWeekPriceBox.HintText = "";
            this.keepingForWeekPriceBox.isPassword = false;
            this.keepingForWeekPriceBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.keepingForWeekPriceBox.LineIdleColor = System.Drawing.Color.Gray;
            this.keepingForWeekPriceBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.keepingForWeekPriceBox.LineThickness = 3;
            this.keepingForWeekPriceBox.Location = new System.Drawing.Point(623, 207);
            this.keepingForWeekPriceBox.Margin = new System.Windows.Forms.Padding(4);
            this.keepingForWeekPriceBox.Name = "keepingForWeekPriceBox";
            this.keepingForWeekPriceBox.Size = new System.Drawing.Size(78, 29);
            this.keepingForWeekPriceBox.TabIndex = 32;
            this.keepingForWeekPriceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel15, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel15.Location = new System.Drawing.Point(518, 177);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(91, 19);
            this.metroLabel15.TabIndex = 30;
            this.metroLabel15.Text = "Дата отбытия";
            // 
            // metroDateTime2
            // 
            this.metroDateTime2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroDateTime2.Checked = false;
            this.panelAnimator.SetDecoration(this.metroDateTime2, BunifuAnimatorNS.DecorationType.None);
            this.metroDateTime2.Enabled = false;
            this.metroDateTime2.Location = new System.Drawing.Point(619, 176);
            this.metroDateTime2.MinimumSize = new System.Drawing.Size(0, 29);
            this.metroDateTime2.Name = "metroDateTime2";
            this.metroDateTime2.Size = new System.Drawing.Size(320, 29);
            this.metroDateTime2.TabIndex = 29;
            // 
            // metroLabel28
            // 
            this.metroLabel28.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel28, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel28.Location = new System.Drawing.Point(618, 75);
            this.metroLabel28.Name = "metroLabel28";
            this.metroLabel28.Size = new System.Drawing.Size(99, 19);
            this.metroLabel28.TabIndex = 28;
            this.metroLabel28.Text = "Кубатура ( м3 )";
            // 
            // store3CongPickerBox
            // 
            // 
            // 
            // 
            this.store3CongPickerBox.CustomButton.Image = null;
            this.store3CongPickerBox.CustomButton.Location = new System.Drawing.Point(33, 1);
            this.store3CongPickerBox.CustomButton.Name = "";
            this.store3CongPickerBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.store3CongPickerBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.store3CongPickerBox.CustomButton.TabIndex = 1;
            this.store3CongPickerBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.store3CongPickerBox.CustomButton.UseSelectable = true;
            this.store3CongPickerBox.CustomButton.Visible = false;
            this.panelAnimator.SetDecoration(this.store3CongPickerBox, BunifuAnimatorNS.DecorationType.None);
            this.store3CongPickerBox.Lines = new string[0];
            this.store3CongPickerBox.Location = new System.Drawing.Point(729, 89);
            this.store3CongPickerBox.MaxLength = 32767;
            this.store3CongPickerBox.Name = "store3CongPickerBox";
            this.store3CongPickerBox.PasswordChar = '\0';
            this.store3CongPickerBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.store3CongPickerBox.SelectedText = "";
            this.store3CongPickerBox.SelectionLength = 0;
            this.store3CongPickerBox.SelectionStart = 0;
            this.store3CongPickerBox.ShortcutsEnabled = true;
            this.store3CongPickerBox.Size = new System.Drawing.Size(55, 23);
            this.store3CongPickerBox.TabIndex = 79;
            this.store3CongPickerBox.UseSelectable = true;
            this.store3CongPickerBox.Visible = false;
            this.store3CongPickerBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.store3CongPickerBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroGrid2
            // 
            this.metroGrid2.AllowUserToResizeRows = false;
            this.metroGrid2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.metroGrid2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.metroGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.panelAnimator.SetDecoration(this.metroGrid2, BunifuAnimatorNS.DecorationType.None);
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid2.DefaultCellStyle = dataGridViewCellStyle17;
            this.metroGrid2.EnableHeadersVisualStyles = false;
            this.metroGrid2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid2.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid2.Location = new System.Drawing.Point(721, 31);
            this.metroGrid2.Name = "metroGrid2";
            this.metroGrid2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid2.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.metroGrid2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid2.Size = new System.Drawing.Size(72, 52);
            this.metroGrid2.TabIndex = 78;
            // 
            // panelAnimator
            // 
            this.panelAnimator.AnimationType = BunifuAnimatorNS.AnimationType.Leaf;
            this.panelAnimator.Cursor = null;
            animation3.AnimateOnlyDifferences = true;
            animation3.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.BlindCoeff")));
            animation3.LeafCoeff = 1F;
            animation3.MaxTime = 1F;
            animation3.MinTime = 0F;
            animation3.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.MosaicCoeff")));
            animation3.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation3.MosaicShift")));
            animation3.MosaicSize = 0;
            animation3.Padding = new System.Windows.Forms.Padding(0);
            animation3.RotateCoeff = 0F;
            animation3.RotateLimit = 0F;
            animation3.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.ScaleCoeff")));
            animation3.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.SlideCoeff")));
            animation3.TimeCoeff = 0F;
            animation3.TransparencyCoeff = 0F;
            this.panelAnimator.DefaultAnimation = animation3;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.updateBtn);
            this.groupBox3.Controls.Add(this.fromStorage3Btn);
            this.groupBox3.Controls.Add(this.metroLabel18);
            this.groupBox3.Controls.Add(this.congestionPG);
            this.groupBox3.Controls.Add(this.bunifuMaterialTextbox1);
            this.groupBox3.Controls.Add(this.metroLabel17);
            this.groupBox3.Controls.Add(this.totalDaysBox);
            this.groupBox3.Controls.Add(this.metroLabel16);
            this.groupBox3.Controls.Add(this.keepingForWeekPriceBox);
            this.groupBox3.Controls.Add(this.metroLabel15);
            this.groupBox3.Controls.Add(this.metroDateTime2);
            this.groupBox3.Controls.Add(this.metroLabel28);
            this.groupBox3.Controls.Add(this.cubageBox);
            this.groupBox3.Controls.Add(this.metroLabel26);
            this.groupBox3.Controls.Add(this.keepingSumBox);
            this.groupBox3.Controls.Add(this.imgList);
            this.groupBox3.Controls.Add(this.metroLabel20);
            this.groupBox3.Controls.Add(this.metroDateTime1);
            this.groupBox3.Controls.Add(this.metroLabel19);
            this.groupBox3.Controls.Add(this.metroLabel14);
            this.groupBox3.Controls.Add(this.metroLabel12);
            this.groupBox3.Controls.Add(this.metroLabel11);
            this.groupBox3.Controls.Add(this.weightBox);
            this.groupBox3.Controls.Add(this.goodsNameBox);
            this.groupBox3.Controls.Add(this.goodsTypeBox);
            this.panelAnimator.SetDecoration(this.groupBox3, BunifuAnimatorNS.DecorationType.None);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.Location = new System.Drawing.Point(714, 237);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(945, 509);
            this.groupBox3.TabIndex = 77;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Товары";
            // 
            // cubageBox
            // 
            this.cubageBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.cubageBox, BunifuAnimatorNS.DecorationType.None);
            this.cubageBox.Enabled = false;
            this.cubageBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.cubageBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cubageBox.HintForeColor = System.Drawing.Color.Empty;
            this.cubageBox.HintText = "";
            this.cubageBox.isPassword = false;
            this.cubageBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.cubageBox.LineIdleColor = System.Drawing.Color.Gray;
            this.cubageBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.cubageBox.LineThickness = 3;
            this.cubageBox.Location = new System.Drawing.Point(716, 62);
            this.cubageBox.Margin = new System.Windows.Forms.Padding(4);
            this.cubageBox.Name = "cubageBox";
            this.cubageBox.Size = new System.Drawing.Size(78, 29);
            this.cubageBox.TabIndex = 27;
            this.cubageBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // metroLabel26
            // 
            this.metroLabel26.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.metroLabel26.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel26, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel26.Location = new System.Drawing.Point(480, 254);
            this.metroLabel26.Name = "metroLabel26";
            this.metroLabel26.Size = new System.Drawing.Size(136, 19);
            this.metroLabel26.TabIndex = 26;
            this.metroLabel26.Text = "Стоимость хранения";
            // 
            // keepingSumBox
            // 
            this.keepingSumBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.keepingSumBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.keepingSumBox, BunifuAnimatorNS.DecorationType.None);
            this.keepingSumBox.Enabled = false;
            this.keepingSumBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.keepingSumBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.keepingSumBox.HintForeColor = System.Drawing.Color.Empty;
            this.keepingSumBox.HintText = "";
            this.keepingSumBox.isPassword = false;
            this.keepingSumBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.keepingSumBox.LineIdleColor = System.Drawing.Color.Gray;
            this.keepingSumBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.keepingSumBox.LineThickness = 3;
            this.keepingSumBox.Location = new System.Drawing.Point(623, 244);
            this.keepingSumBox.Margin = new System.Windows.Forms.Padding(4);
            this.keepingSumBox.Name = "keepingSumBox";
            this.keepingSumBox.Size = new System.Drawing.Size(78, 29);
            this.keepingSumBox.TabIndex = 25;
            this.keepingSumBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // imgList
            // 
            this.imgList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.imgList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imgList.Cursor = System.Windows.Forms.Cursors.Default;
            this.panelAnimator.SetDecoration(this.imgList, BunifuAnimatorNS.DecorationType.None);
            this.imgList.Location = new System.Drawing.Point(135, 75);
            this.imgList.Name = "imgList";
            this.imgList.Size = new System.Drawing.Size(286, 391);
            this.imgList.TabIndex = 23;
            this.imgList.TabStop = false;
            // 
            // metroLabel20
            // 
            this.metroLabel20.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel20, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel20.Location = new System.Drawing.Point(507, 141);
            this.metroLabel20.Name = "metroLabel20";
            this.metroLabel20.Size = new System.Drawing.Size(102, 19);
            this.metroLabel20.TabIndex = 22;
            this.metroLabel20.Text = "Дата прибытия";
            // 
            // metroDateTime1
            // 
            this.metroDateTime1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroDateTime1.Checked = false;
            this.panelAnimator.SetDecoration(this.metroDateTime1, BunifuAnimatorNS.DecorationType.None);
            this.metroDateTime1.Enabled = false;
            this.metroDateTime1.Location = new System.Drawing.Point(618, 139);
            this.metroDateTime1.MinimumSize = new System.Drawing.Size(0, 29);
            this.metroDateTime1.Name = "metroDateTime1";
            this.metroDateTime1.Size = new System.Drawing.Size(320, 29);
            this.metroDateTime1.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroDateTime1.TabIndex = 21;
            // 
            // metroLabel19
            // 
            this.metroLabel19.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel19, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel19.Location = new System.Drawing.Point(577, 105);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(32, 19);
            this.metroLabel19.TabIndex = 20;
            this.metroLabel19.Text = "Тип";
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel14, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel14.Location = new System.Drawing.Point(9, 75);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(108, 19);
            this.metroLabel14.TabIndex = 18;
            this.metroLabel14.Text = "Список товаров";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel12, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel12.Location = new System.Drawing.Point(662, 41);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(55, 19);
            this.metroLabel12.TabIndex = 18;
            this.metroLabel12.Text = "Вес ( т )";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel11, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel11.Location = new System.Drawing.Point(7, 41);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(204, 19);
            this.metroLabel11.TabIndex = 18;
            this.metroLabel11.Text = "Наименование группы товаров";
            // 
            // weightBox
            // 
            this.weightBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.weightBox, BunifuAnimatorNS.DecorationType.None);
            this.weightBox.Enabled = false;
            this.weightBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.weightBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.weightBox.HintForeColor = System.Drawing.Color.Empty;
            this.weightBox.HintText = "";
            this.weightBox.isPassword = false;
            this.weightBox.LineFocusedColor = System.Drawing.Color.Teal;
            this.weightBox.LineIdleColor = System.Drawing.Color.Gray;
            this.weightBox.LineMouseHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.weightBox.LineThickness = 3;
            this.weightBox.Location = new System.Drawing.Point(716, 28);
            this.weightBox.Margin = new System.Windows.Forms.Padding(4);
            this.weightBox.Name = "weightBox";
            this.weightBox.Size = new System.Drawing.Size(78, 29);
            this.weightBox.TabIndex = 17;
            this.weightBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // bunifuGradientPanel3
            // 
            this.bunifuGradientPanel3.AutoScroll = true;
            this.bunifuGradientPanel3.BackColor = System.Drawing.Color.White;
            this.bunifuGradientPanel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel3.BackgroundImage")));
            this.bunifuGradientPanel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel3.Controls.Add(this.store3CongPickerBox);
            this.bunifuGradientPanel3.Controls.Add(this.metroGrid2);
            this.bunifuGradientPanel3.Controls.Add(this.groupBox3);
            this.bunifuGradientPanel3.Controls.Add(this.priceSearchBox);
            this.bunifuGradientPanel3.Controls.Add(this.typeSearchBox);
            this.bunifuGradientPanel3.Controls.Add(this.groupBox1);
            this.bunifuGradientPanel3.Controls.Add(this.groupBox2);
            this.bunifuGradientPanel3.Controls.Add(this.metroGrid1);
            this.bunifuGradientPanel3.Controls.Add(this.dateSearchBox);
            this.panelAnimator.SetDecoration(this.bunifuGradientPanel3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuGradientPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bunifuGradientPanel3.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel3.Location = new System.Drawing.Point(20, 108);
            this.bunifuGradientPanel3.Name = "bunifuGradientPanel3";
            this.bunifuGradientPanel3.Quality = 10;
            this.bunifuGradientPanel3.Size = new System.Drawing.Size(1272, 609);
            this.bunifuGradientPanel3.TabIndex = 44;
            this.bunifuGradientPanel3.MouseEnter += new System.EventHandler(this.bunifuGradientPanel3_MouseEnter);
            // 
            // Storage3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1312, 737);
            this.Controls.Add(this.bunifuGradientPanel3);
            this.Controls.Add(this.bunifuGradientPanel2);
            this.panelAnimator.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.Name = "Storage3";
            this.Text = "Склад товаров с темепературным режимом";
            this.Load += new System.EventHandler(this.Storage3_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).EndInit();
            this.bunifuGradientPanel2.ResumeLayout(false);
            this.bunifuGradientPanel2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid2)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgList)).EndInit();
            this.bunifuGradientPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuMaterialTextbox goodsNameBox;
        private BunifuAnimatorNS.BunifuTransition panelAnimator;
        private Bunifu.Framework.UI.BunifuMaterialTextbox priceSearchBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox typeSearchBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox idBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox fromPhoneBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox fromEmailBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox fromCityBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox companyFromBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox fromAdresBox;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private Bunifu.Framework.UI.BunifuMaterialTextbox toPhoneBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox toEmailBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox toCityBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox companyToBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox toAdresBox;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroGrid metroGrid1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox dateSearchBox;
        private System.Windows.Forms.ToolTip toolTip1;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem обновитьToolStripMenuItem;
        private MetroFramework.Controls.MetroComboBox goodsTypeBox;
        private Bunifu.Framework.UI.BunifuFlatButton updateBtn;
        private Bunifu.Framework.UI.BunifuFlatButton fromStorage3Btn;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private Bunifu.Framework.UI.BunifuCircleProgressbar congestionPG;
        private Bunifu.Framework.UI.BunifuMaterialTextbox bunifuMaterialTextbox1;
        private MetroFramework.Controls.MetroLabel metroLabel17;
        private Bunifu.Framework.UI.BunifuMaterialTextbox totalDaysBox;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private Bunifu.Framework.UI.BunifuMaterialTextbox keepingForWeekPriceBox;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroDateTime metroDateTime2;
        private MetroFramework.Controls.MetroLabel metroLabel28;
        private MetroFramework.Controls.MetroTextBox store3CongPickerBox;
        private MetroFramework.Controls.MetroGrid metroGrid2;
        private System.Windows.Forms.GroupBox groupBox3;
        private Bunifu.Framework.UI.BunifuMaterialTextbox cubageBox;
        private MetroFramework.Controls.MetroLabel metroLabel26;
        private Bunifu.Framework.UI.BunifuMaterialTextbox keepingSumBox;
        public System.Windows.Forms.PictureBox imgList;
        private MetroFramework.Controls.MetroLabel metroLabel20;
        private MetroFramework.Controls.MetroDateTime metroDateTime1;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private Bunifu.Framework.UI.BunifuMaterialTextbox weightBox;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel3;
    }
}