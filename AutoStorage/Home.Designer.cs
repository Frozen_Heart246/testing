﻿namespace AutoStorage
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.namebox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.secondNameBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.phoneBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.emailBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.changeStatsBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.changePassBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.backToManagersMenu = new System.Windows.Forms.Button();
            this.loginBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.imgListHome = new System.Windows.Forms.PictureBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.changePicBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.updateStatsBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.cancelUpdStatsBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            ((System.ComponentModel.ISupportInitialize)(this.imgListHome)).BeginInit();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(50, 85);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(35, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Имя";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(20, 120);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(65, 19);
            this.metroLabel2.TabIndex = 1;
            this.metroLabel2.Text = "Фамилия";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(20, 155);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(62, 19);
            this.metroLabel3.TabIndex = 2;
            this.metroLabel3.Text = "Телефон";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(41, 190);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(41, 19);
            this.metroLabel4.TabIndex = 3;
            this.metroLabel4.Text = "Email";
            // 
            // namebox
            // 
            this.namebox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.namebox.Enabled = false;
            this.namebox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.namebox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.namebox.HintForeColor = System.Drawing.Color.Empty;
            this.namebox.HintText = "";
            this.namebox.isPassword = false;
            this.namebox.LineFocusedColor = System.Drawing.Color.LightGray;
            this.namebox.LineIdleColor = System.Drawing.Color.Gray;
            this.namebox.LineMouseHoverColor = System.Drawing.Color.DimGray;
            this.namebox.LineThickness = 3;
            this.namebox.Location = new System.Drawing.Point(92, 71);
            this.namebox.Margin = new System.Windows.Forms.Padding(4);
            this.namebox.Name = "namebox";
            this.namebox.Size = new System.Drawing.Size(196, 33);
            this.namebox.TabIndex = 8;
            this.namebox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // secondNameBox
            // 
            this.secondNameBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.secondNameBox.Enabled = false;
            this.secondNameBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.secondNameBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.secondNameBox.HintForeColor = System.Drawing.Color.Empty;
            this.secondNameBox.HintText = "";
            this.secondNameBox.isPassword = false;
            this.secondNameBox.LineFocusedColor = System.Drawing.Color.LightGray;
            this.secondNameBox.LineIdleColor = System.Drawing.Color.Gray;
            this.secondNameBox.LineMouseHoverColor = System.Drawing.Color.DimGray;
            this.secondNameBox.LineThickness = 3;
            this.secondNameBox.Location = new System.Drawing.Point(92, 106);
            this.secondNameBox.Margin = new System.Windows.Forms.Padding(4);
            this.secondNameBox.Name = "secondNameBox";
            this.secondNameBox.Size = new System.Drawing.Size(196, 33);
            this.secondNameBox.TabIndex = 9;
            this.secondNameBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // phoneBox
            // 
            this.phoneBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.phoneBox.Enabled = false;
            this.phoneBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.phoneBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.phoneBox.HintForeColor = System.Drawing.Color.Empty;
            this.phoneBox.HintText = "";
            this.phoneBox.isPassword = false;
            this.phoneBox.LineFocusedColor = System.Drawing.Color.LightGray;
            this.phoneBox.LineIdleColor = System.Drawing.Color.Gray;
            this.phoneBox.LineMouseHoverColor = System.Drawing.Color.DimGray;
            this.phoneBox.LineThickness = 3;
            this.phoneBox.Location = new System.Drawing.Point(92, 141);
            this.phoneBox.Margin = new System.Windows.Forms.Padding(4);
            this.phoneBox.Name = "phoneBox";
            this.phoneBox.Size = new System.Drawing.Size(196, 33);
            this.phoneBox.TabIndex = 10;
            this.phoneBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // emailBox
            // 
            this.emailBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.emailBox.Enabled = false;
            this.emailBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.emailBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.emailBox.HintForeColor = System.Drawing.Color.Empty;
            this.emailBox.HintText = "";
            this.emailBox.isPassword = false;
            this.emailBox.LineFocusedColor = System.Drawing.Color.LightGray;
            this.emailBox.LineIdleColor = System.Drawing.Color.Gray;
            this.emailBox.LineMouseHoverColor = System.Drawing.Color.DimGray;
            this.emailBox.LineThickness = 3;
            this.emailBox.Location = new System.Drawing.Point(92, 176);
            this.emailBox.Margin = new System.Windows.Forms.Padding(4);
            this.emailBox.Name = "emailBox";
            this.emailBox.Size = new System.Drawing.Size(196, 33);
            this.emailBox.TabIndex = 11;
            this.emailBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // changeStatsBtn
            // 
            this.changeStatsBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.changeStatsBtn.BackColor = System.Drawing.Color.DimGray;
            this.changeStatsBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.changeStatsBtn.BorderRadius = 0;
            this.changeStatsBtn.ButtonText = "Редактировать данные";
            this.changeStatsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.changeStatsBtn.DisabledColor = System.Drawing.Color.Gray;
            this.changeStatsBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.changeStatsBtn.Iconimage = null;
            this.changeStatsBtn.Iconimage_right = null;
            this.changeStatsBtn.Iconimage_right_Selected = null;
            this.changeStatsBtn.Iconimage_Selected = null;
            this.changeStatsBtn.IconMarginLeft = 0;
            this.changeStatsBtn.IconMarginRight = 0;
            this.changeStatsBtn.IconRightVisible = true;
            this.changeStatsBtn.IconRightZoom = 0D;
            this.changeStatsBtn.IconVisible = true;
            this.changeStatsBtn.IconZoom = 90D;
            this.changeStatsBtn.IsTab = false;
            this.changeStatsBtn.Location = new System.Drawing.Point(17, 221);
            this.changeStatsBtn.Name = "changeStatsBtn";
            this.changeStatsBtn.Normalcolor = System.Drawing.Color.DimGray;
            this.changeStatsBtn.OnHovercolor = System.Drawing.Color.Gray;
            this.changeStatsBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.changeStatsBtn.selected = false;
            this.changeStatsBtn.Size = new System.Drawing.Size(247, 48);
            this.changeStatsBtn.TabIndex = 12;
            this.changeStatsBtn.Text = "Редактировать данные";
            this.changeStatsBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.changeStatsBtn.Textcolor = System.Drawing.Color.White;
            this.changeStatsBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeStatsBtn.Click += new System.EventHandler(this.changeStatsBtn_Click);
            // 
            // changePassBtn
            // 
            this.changePassBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.changePassBtn.BackColor = System.Drawing.Color.DimGray;
            this.changePassBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.changePassBtn.BorderRadius = 0;
            this.changePassBtn.ButtonText = "Изменить пароль";
            this.changePassBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.changePassBtn.DisabledColor = System.Drawing.Color.Gray;
            this.changePassBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.changePassBtn.Iconimage = null;
            this.changePassBtn.Iconimage_right = null;
            this.changePassBtn.Iconimage_right_Selected = null;
            this.changePassBtn.Iconimage_Selected = null;
            this.changePassBtn.IconMarginLeft = 0;
            this.changePassBtn.IconMarginRight = 0;
            this.changePassBtn.IconRightVisible = true;
            this.changePassBtn.IconRightZoom = 0D;
            this.changePassBtn.IconVisible = true;
            this.changePassBtn.IconZoom = 90D;
            this.changePassBtn.IsTab = false;
            this.changePassBtn.Location = new System.Drawing.Point(17, 275);
            this.changePassBtn.Name = "changePassBtn";
            this.changePassBtn.Normalcolor = System.Drawing.Color.DimGray;
            this.changePassBtn.OnHovercolor = System.Drawing.Color.Gray;
            this.changePassBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.changePassBtn.selected = false;
            this.changePassBtn.Size = new System.Drawing.Size(247, 48);
            this.changePassBtn.TabIndex = 12;
            this.changePassBtn.Text = "Изменить пароль";
            this.changePassBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.changePassBtn.Textcolor = System.Drawing.Color.White;
            this.changePassBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changePassBtn.Click += new System.EventHandler(this.changePassBtn_Click);
            // 
            // backToManagersMenu
            // 
            this.backToManagersMenu.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("backToManagersMenu.BackgroundImage")));
            this.backToManagersMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.backToManagersMenu.FlatAppearance.BorderSize = 0;
            this.backToManagersMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backToManagersMenu.Location = new System.Drawing.Point(6, 20);
            this.backToManagersMenu.Name = "backToManagersMenu";
            this.backToManagersMenu.Size = new System.Drawing.Size(32, 32);
            this.backToManagersMenu.TabIndex = 19;
            this.backToManagersMenu.UseVisualStyleBackColor = true;
            this.backToManagersMenu.Click += new System.EventHandler(this.backToManagersMenu_Click);
            // 
            // loginBox
            // 
            this.loginBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.loginBox.Enabled = false;
            this.loginBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.loginBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.loginBox.HintForeColor = System.Drawing.Color.Empty;
            this.loginBox.HintText = "";
            this.loginBox.isPassword = false;
            this.loginBox.LineFocusedColor = System.Drawing.Color.Gray;
            this.loginBox.LineIdleColor = System.Drawing.Color.Gray;
            this.loginBox.LineMouseHoverColor = System.Drawing.Color.Gray;
            this.loginBox.LineThickness = 3;
            this.loginBox.Location = new System.Drawing.Point(385, 27);
            this.loginBox.Margin = new System.Windows.Forms.Padding(4);
            this.loginBox.Name = "loginBox";
            this.loginBox.Size = new System.Drawing.Size(130, 25);
            this.loginBox.TabIndex = 21;
            this.loginBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // imgListHome
            // 
            this.imgListHome.Location = new System.Drawing.Point(305, 55);
            this.imgListHome.Name = "imgListHome";
            this.imgListHome.Size = new System.Drawing.Size(211, 214);
            this.imgListHome.TabIndex = 20;
            this.imgListHome.TabStop = false;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(284, 33);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(94, 19);
            this.metroLabel5.TabIndex = 22;
            this.metroLabel5.Text = "Вы вошли как";
            // 
            // changePicBtn
            // 
            this.changePicBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.changePicBtn.BackColor = System.Drawing.Color.DimGray;
            this.changePicBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.changePicBtn.BorderRadius = 0;
            this.changePicBtn.ButtonText = "Изменить фотографию";
            this.changePicBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.changePicBtn.DisabledColor = System.Drawing.Color.Gray;
            this.changePicBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.changePicBtn.Iconimage = null;
            this.changePicBtn.Iconimage_right = null;
            this.changePicBtn.Iconimage_right_Selected = null;
            this.changePicBtn.Iconimage_Selected = null;
            this.changePicBtn.IconMarginLeft = 0;
            this.changePicBtn.IconMarginRight = 0;
            this.changePicBtn.IconRightVisible = true;
            this.changePicBtn.IconRightZoom = 0D;
            this.changePicBtn.IconVisible = true;
            this.changePicBtn.IconZoom = 90D;
            this.changePicBtn.IsTab = false;
            this.changePicBtn.Location = new System.Drawing.Point(290, 275);
            this.changePicBtn.Name = "changePicBtn";
            this.changePicBtn.Normalcolor = System.Drawing.Color.DimGray;
            this.changePicBtn.OnHovercolor = System.Drawing.Color.Gray;
            this.changePicBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.changePicBtn.selected = false;
            this.changePicBtn.Size = new System.Drawing.Size(238, 48);
            this.changePicBtn.TabIndex = 23;
            this.changePicBtn.Text = "Изменить фотографию";
            this.changePicBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.changePicBtn.Textcolor = System.Drawing.Color.White;
            this.changePicBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changePicBtn.Click += new System.EventHandler(this.changePicBtn_Click);
            // 
            // updateStatsBtn
            // 
            this.updateStatsBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.updateStatsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.updateStatsBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.updateStatsBtn.BorderRadius = 0;
            this.updateStatsBtn.ButtonText = "Сохранить изменения";
            this.updateStatsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.updateStatsBtn.DisabledColor = System.Drawing.Color.Gray;
            this.updateStatsBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.updateStatsBtn.Iconimage = null;
            this.updateStatsBtn.Iconimage_right = null;
            this.updateStatsBtn.Iconimage_right_Selected = null;
            this.updateStatsBtn.Iconimage_Selected = null;
            this.updateStatsBtn.IconMarginLeft = 0;
            this.updateStatsBtn.IconMarginRight = 0;
            this.updateStatsBtn.IconRightVisible = true;
            this.updateStatsBtn.IconRightZoom = 0D;
            this.updateStatsBtn.IconVisible = true;
            this.updateStatsBtn.IconZoom = 90D;
            this.updateStatsBtn.IsTab = false;
            this.updateStatsBtn.Location = new System.Drawing.Point(17, 221);
            this.updateStatsBtn.Name = "updateStatsBtn";
            this.updateStatsBtn.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.updateStatsBtn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.updateStatsBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.updateStatsBtn.selected = false;
            this.updateStatsBtn.Size = new System.Drawing.Size(247, 48);
            this.updateStatsBtn.TabIndex = 24;
            this.updateStatsBtn.Text = "Сохранить изменения";
            this.updateStatsBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.updateStatsBtn.Textcolor = System.Drawing.Color.White;
            this.updateStatsBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateStatsBtn.Click += new System.EventHandler(this.updateStatsBtn_Click);
            // 
            // cancelUpdStatsBtn
            // 
            this.cancelUpdStatsBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.cancelUpdStatsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.cancelUpdStatsBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cancelUpdStatsBtn.BorderRadius = 0;
            this.cancelUpdStatsBtn.ButtonText = "Отменить редактирование";
            this.cancelUpdStatsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cancelUpdStatsBtn.DisabledColor = System.Drawing.Color.Gray;
            this.cancelUpdStatsBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.cancelUpdStatsBtn.Iconimage = null;
            this.cancelUpdStatsBtn.Iconimage_right = null;
            this.cancelUpdStatsBtn.Iconimage_right_Selected = null;
            this.cancelUpdStatsBtn.Iconimage_Selected = null;
            this.cancelUpdStatsBtn.IconMarginLeft = 0;
            this.cancelUpdStatsBtn.IconMarginRight = 0;
            this.cancelUpdStatsBtn.IconRightVisible = true;
            this.cancelUpdStatsBtn.IconRightZoom = 0D;
            this.cancelUpdStatsBtn.IconVisible = true;
            this.cancelUpdStatsBtn.IconZoom = 90D;
            this.cancelUpdStatsBtn.IsTab = false;
            this.cancelUpdStatsBtn.Location = new System.Drawing.Point(17, 275);
            this.cancelUpdStatsBtn.Name = "cancelUpdStatsBtn";
            this.cancelUpdStatsBtn.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.cancelUpdStatsBtn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.cancelUpdStatsBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.cancelUpdStatsBtn.selected = false;
            this.cancelUpdStatsBtn.Size = new System.Drawing.Size(247, 48);
            this.cancelUpdStatsBtn.TabIndex = 25;
            this.cancelUpdStatsBtn.Text = "Отменить редактирование";
            this.cancelUpdStatsBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cancelUpdStatsBtn.Textcolor = System.Drawing.Color.White;
            this.cancelUpdStatsBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelUpdStatsBtn.Click += new System.EventHandler(this.cancelUpdStatsBtn_Click);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 338);
            this.Controls.Add(this.changeStatsBtn);
            this.Controls.Add(this.updateStatsBtn);
            this.Controls.Add(this.changePicBtn);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.loginBox);
            this.Controls.Add(this.imgListHome);
            this.Controls.Add(this.backToManagersMenu);
            this.Controls.Add(this.changePassBtn);
            this.Controls.Add(this.emailBox);
            this.Controls.Add(this.phoneBox);
            this.Controls.Add(this.secondNameBox);
            this.Controls.Add(this.namebox);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.cancelUpdStatsBtn);
            this.Name = "Home";
            this.Text = "  Личный кабинет";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Home_FormClosing);
            this.Load += new System.EventHandler(this.Home_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgListHome)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private Bunifu.Framework.UI.BunifuMaterialTextbox namebox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox secondNameBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox phoneBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox emailBox;
        private Bunifu.Framework.UI.BunifuFlatButton changeStatsBtn;
        private Bunifu.Framework.UI.BunifuFlatButton changePassBtn;
        private System.Windows.Forms.Button backToManagersMenu;
        private Bunifu.Framework.UI.BunifuMaterialTextbox loginBox;
        private System.Windows.Forms.PictureBox imgListHome;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private Bunifu.Framework.UI.BunifuFlatButton changePicBtn;
        private Bunifu.Framework.UI.BunifuFlatButton updateStatsBtn;
        private Bunifu.Framework.UI.BunifuFlatButton cancelUpdStatsBtn;
    }
}