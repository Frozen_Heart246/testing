﻿namespace AutoStorage
{
    partial class managerRegistr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(managerRegistr));
            this.setFacePicBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.exitBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.regFinBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.loginBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.mailBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.phoneBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.secondNameBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.nameBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.passBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.facePicBox = new System.Windows.Forms.PictureBox();
            this.backtoRegistration = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.facePicBox)).BeginInit();
            this.SuspendLayout();
            // 
            // setFacePicBtn
            // 
            this.setFacePicBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.setFacePicBtn.BackColor = System.Drawing.Color.Teal;
            this.setFacePicBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.setFacePicBtn.BorderRadius = 0;
            this.setFacePicBtn.ButtonText = "Загрузить";
            this.setFacePicBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.setFacePicBtn.DisabledColor = System.Drawing.Color.Gray;
            this.setFacePicBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.setFacePicBtn.Iconimage = null;
            this.setFacePicBtn.Iconimage_right = null;
            this.setFacePicBtn.Iconimage_right_Selected = null;
            this.setFacePicBtn.Iconimage_Selected = null;
            this.setFacePicBtn.IconMarginLeft = 0;
            this.setFacePicBtn.IconMarginRight = 0;
            this.setFacePicBtn.IconRightVisible = true;
            this.setFacePicBtn.IconRightZoom = 0D;
            this.setFacePicBtn.IconVisible = true;
            this.setFacePicBtn.IconZoom = 90D;
            this.setFacePicBtn.IsTab = false;
            this.setFacePicBtn.Location = new System.Drawing.Point(93, 325);
            this.setFacePicBtn.Name = "setFacePicBtn";
            this.setFacePicBtn.Normalcolor = System.Drawing.Color.Teal;
            this.setFacePicBtn.OnHovercolor = System.Drawing.Color.LightSeaGreen;
            this.setFacePicBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.setFacePicBtn.selected = false;
            this.setFacePicBtn.Size = new System.Drawing.Size(83, 37);
            this.setFacePicBtn.TabIndex = 39;
            this.setFacePicBtn.Text = "Загрузить";
            this.setFacePicBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.setFacePicBtn.Textcolor = System.Drawing.Color.White;
            this.setFacePicBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setFacePicBtn.Click += new System.EventHandler(this.setFacePicBtn_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.exitBtn.BackColor = System.Drawing.Color.Teal;
            this.exitBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.exitBtn.BorderRadius = 0;
            this.exitBtn.ButtonText = "Выход";
            this.exitBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBtn.DisabledColor = System.Drawing.Color.Gray;
            this.exitBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.exitBtn.Iconimage = null;
            this.exitBtn.Iconimage_right = null;
            this.exitBtn.Iconimage_right_Selected = null;
            this.exitBtn.Iconimage_Selected = null;
            this.exitBtn.IconMarginLeft = 0;
            this.exitBtn.IconMarginRight = 0;
            this.exitBtn.IconRightVisible = true;
            this.exitBtn.IconRightZoom = 0D;
            this.exitBtn.IconVisible = true;
            this.exitBtn.IconZoom = 90D;
            this.exitBtn.IsTab = false;
            this.exitBtn.Location = new System.Drawing.Point(234, 422);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Normalcolor = System.Drawing.Color.Teal;
            this.exitBtn.OnHovercolor = System.Drawing.Color.LightSeaGreen;
            this.exitBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.exitBtn.selected = false;
            this.exitBtn.Size = new System.Drawing.Size(185, 48);
            this.exitBtn.TabIndex = 37;
            this.exitBtn.Text = "Выход";
            this.exitBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.exitBtn.Textcolor = System.Drawing.Color.White;
            this.exitBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // regFinBtn
            // 
            this.regFinBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.regFinBtn.BackColor = System.Drawing.Color.Teal;
            this.regFinBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.regFinBtn.BorderRadius = 0;
            this.regFinBtn.ButtonText = "Зарегистрироваться";
            this.regFinBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.regFinBtn.DisabledColor = System.Drawing.Color.Gray;
            this.regFinBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.regFinBtn.Iconimage = null;
            this.regFinBtn.Iconimage_right = null;
            this.regFinBtn.Iconimage_right_Selected = null;
            this.regFinBtn.Iconimage_Selected = null;
            this.regFinBtn.IconMarginLeft = 0;
            this.regFinBtn.IconMarginRight = 0;
            this.regFinBtn.IconRightVisible = true;
            this.regFinBtn.IconRightZoom = 0D;
            this.regFinBtn.IconVisible = true;
            this.regFinBtn.IconZoom = 90D;
            this.regFinBtn.IsTab = false;
            this.regFinBtn.Location = new System.Drawing.Point(34, 422);
            this.regFinBtn.Name = "regFinBtn";
            this.regFinBtn.Normalcolor = System.Drawing.Color.Teal;
            this.regFinBtn.OnHovercolor = System.Drawing.Color.LightSeaGreen;
            this.regFinBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.regFinBtn.selected = false;
            this.regFinBtn.Size = new System.Drawing.Size(170, 48);
            this.regFinBtn.TabIndex = 36;
            this.regFinBtn.Text = "Зарегистрироваться";
            this.regFinBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.regFinBtn.Textcolor = System.Drawing.Color.White;
            this.regFinBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.regFinBtn.Click += new System.EventHandler(this.regFinBtn_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(21, 334);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(40, 19);
            this.metroLabel1.TabIndex = 34;
            this.metroLabel1.Text = "Фото";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(21, 294);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(54, 19);
            this.metroLabel7.TabIndex = 33;
            this.metroLabel7.Text = "Пароль";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(21, 252);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(47, 19);
            this.metroLabel6.TabIndex = 32;
            this.metroLabel6.Text = "Логин";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(21, 210);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(47, 19);
            this.metroLabel5.TabIndex = 31;
            this.metroLabel5.Text = "Емаил";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(21, 168);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(62, 19);
            this.metroLabel4.TabIndex = 30;
            this.metroLabel4.Text = "Телефон";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(21, 126);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(65, 19);
            this.metroLabel3.TabIndex = 29;
            this.metroLabel3.Text = "Фамилия";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(21, 84);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(35, 19);
            this.metroLabel2.TabIndex = 28;
            this.metroLabel2.Text = "Имя";
            // 
            // loginBox
            // 
            this.loginBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.loginBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.loginBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.loginBox.HintForeColor = System.Drawing.Color.Empty;
            this.loginBox.HintText = "";
            this.loginBox.isPassword = false;
            this.loginBox.LineFocusedColor = System.Drawing.Color.Blue;
            this.loginBox.LineIdleColor = System.Drawing.Color.Gray;
            this.loginBox.LineMouseHoverColor = System.Drawing.Color.DodgerBlue;
            this.loginBox.LineThickness = 3;
            this.loginBox.Location = new System.Drawing.Point(93, 234);
            this.loginBox.Margin = new System.Windows.Forms.Padding(4);
            this.loginBox.Name = "loginBox";
            this.loginBox.Size = new System.Drawing.Size(313, 33);
            this.loginBox.TabIndex = 27;
            this.loginBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // mailBox
            // 
            this.mailBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.mailBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.mailBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.mailBox.HintForeColor = System.Drawing.Color.Empty;
            this.mailBox.HintText = "";
            this.mailBox.isPassword = false;
            this.mailBox.LineFocusedColor = System.Drawing.Color.Blue;
            this.mailBox.LineIdleColor = System.Drawing.Color.Gray;
            this.mailBox.LineMouseHoverColor = System.Drawing.Color.DodgerBlue;
            this.mailBox.LineThickness = 3;
            this.mailBox.Location = new System.Drawing.Point(93, 193);
            this.mailBox.Margin = new System.Windows.Forms.Padding(4);
            this.mailBox.Name = "mailBox";
            this.mailBox.Size = new System.Drawing.Size(313, 33);
            this.mailBox.TabIndex = 26;
            this.mailBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // phoneBox
            // 
            this.phoneBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.phoneBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.phoneBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.phoneBox.HintForeColor = System.Drawing.Color.Empty;
            this.phoneBox.HintText = "";
            this.phoneBox.isPassword = false;
            this.phoneBox.LineFocusedColor = System.Drawing.Color.Blue;
            this.phoneBox.LineIdleColor = System.Drawing.Color.Gray;
            this.phoneBox.LineMouseHoverColor = System.Drawing.Color.DodgerBlue;
            this.phoneBox.LineThickness = 3;
            this.phoneBox.Location = new System.Drawing.Point(93, 152);
            this.phoneBox.Margin = new System.Windows.Forms.Padding(4);
            this.phoneBox.Name = "phoneBox";
            this.phoneBox.Size = new System.Drawing.Size(313, 33);
            this.phoneBox.TabIndex = 25;
            this.phoneBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // secondNameBox
            // 
            this.secondNameBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.secondNameBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.secondNameBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.secondNameBox.HintForeColor = System.Drawing.Color.Empty;
            this.secondNameBox.HintText = "";
            this.secondNameBox.isPassword = false;
            this.secondNameBox.LineFocusedColor = System.Drawing.Color.Blue;
            this.secondNameBox.LineIdleColor = System.Drawing.Color.Gray;
            this.secondNameBox.LineMouseHoverColor = System.Drawing.Color.DodgerBlue;
            this.secondNameBox.LineThickness = 3;
            this.secondNameBox.Location = new System.Drawing.Point(93, 112);
            this.secondNameBox.Margin = new System.Windows.Forms.Padding(4);
            this.secondNameBox.Name = "secondNameBox";
            this.secondNameBox.Size = new System.Drawing.Size(313, 33);
            this.secondNameBox.TabIndex = 24;
            this.secondNameBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // nameBox
            // 
            this.nameBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.nameBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.nameBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.nameBox.HintForeColor = System.Drawing.Color.Empty;
            this.nameBox.HintText = "";
            this.nameBox.isPassword = false;
            this.nameBox.LineFocusedColor = System.Drawing.Color.Blue;
            this.nameBox.LineIdleColor = System.Drawing.Color.Gray;
            this.nameBox.LineMouseHoverColor = System.Drawing.Color.DodgerBlue;
            this.nameBox.LineThickness = 3;
            this.nameBox.Location = new System.Drawing.Point(93, 70);
            this.nameBox.Margin = new System.Windows.Forms.Padding(4);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(313, 33);
            this.nameBox.TabIndex = 23;
            this.nameBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // passBox
            // 
            this.passBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.passBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.passBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.passBox.HintForeColor = System.Drawing.Color.Empty;
            this.passBox.HintText = "";
            this.passBox.isPassword = false;
            this.passBox.LineFocusedColor = System.Drawing.Color.Blue;
            this.passBox.LineIdleColor = System.Drawing.Color.Gray;
            this.passBox.LineMouseHoverColor = System.Drawing.Color.DodgerBlue;
            this.passBox.LineThickness = 3;
            this.passBox.Location = new System.Drawing.Point(93, 275);
            this.passBox.Margin = new System.Windows.Forms.Padding(4);
            this.passBox.Name = "passBox";
            this.passBox.Size = new System.Drawing.Size(313, 33);
            this.passBox.TabIndex = 22;
            this.passBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // facePicBox
            // 
            this.facePicBox.BackgroundImage = global::AutoStorage.Properties.Resources.icons8_User3_64;
            this.facePicBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.facePicBox.Location = new System.Drawing.Point(182, 315);
            this.facePicBox.Name = "facePicBox";
            this.facePicBox.Size = new System.Drawing.Size(136, 91);
            this.facePicBox.TabIndex = 38;
            this.facePicBox.TabStop = false;
            // 
            // backtoRegistration
            // 
            this.backtoRegistration.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("backtoRegistration.BackgroundImage")));
            this.backtoRegistration.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.backtoRegistration.FlatAppearance.BorderSize = 0;
            this.backtoRegistration.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backtoRegistration.Location = new System.Drawing.Point(18, 22);
            this.backtoRegistration.Name = "backtoRegistration";
            this.backtoRegistration.Size = new System.Drawing.Size(32, 32);
            this.backtoRegistration.TabIndex = 35;
            this.backtoRegistration.UseVisualStyleBackColor = true;
            this.backtoRegistration.Click += new System.EventHandler(this.backtoRegistration_Click);
            // 
            // managerRegistr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 497);
            this.Controls.Add(this.setFacePicBtn);
            this.Controls.Add(this.facePicBox);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.regFinBtn);
            this.Controls.Add(this.backtoRegistration);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.loginBox);
            this.Controls.Add(this.mailBox);
            this.Controls.Add(this.phoneBox);
            this.Controls.Add(this.secondNameBox);
            this.Controls.Add(this.nameBox);
            this.Controls.Add(this.passBox);
            this.Name = "managerRegistr";
            this.Text = "    Регистрация менеджера";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.managerRegistr_FormClosing);
            this.Load += new System.EventHandler(this.managerRegistr_Load);
            ((System.ComponentModel.ISupportInitialize)(this.facePicBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuFlatButton setFacePicBtn;
        private System.Windows.Forms.PictureBox facePicBox;
        private Bunifu.Framework.UI.BunifuFlatButton exitBtn;
        private Bunifu.Framework.UI.BunifuFlatButton regFinBtn;
        private System.Windows.Forms.Button backtoRegistration;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private Bunifu.Framework.UI.BunifuMaterialTextbox loginBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox mailBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox phoneBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox secondNameBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox nameBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox passBox;

    }
}