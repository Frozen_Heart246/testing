﻿namespace AutoStorage
{
    partial class financerBillsLookUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(financerBillsLookUp));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelAnimator = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.bunifuGradientPanel3 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.createXLSBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.createXLS2Btn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.dateSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.cmpnToSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.trnsprtTypeSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.priceSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.gdsTypeSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.cmpnFromSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.arrivaDateSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.daysCountFromSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.kpngPriceSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.gdstpFromSearchBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.lookWayBillsBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.imgList = new System.Windows.Forms.PictureBox();
            this.slidemenu = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.bunifuFlatButton1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton5 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton7 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnSlide = new System.Windows.Forms.Button();
            this.metroGrid2 = new MetroFramework.Controls.MetroGrid();
            this.lookPurchBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.metroGrid1 = new MetroFramework.Controls.MetroGrid();
            this.backToWorkWithBills = new System.Windows.Forms.Button();
            this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.обновитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bunifuGradientPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgList)).BeginInit();
            this.slidemenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).BeginInit();
            this.bunifuGradientPanel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelAnimator
            // 
            this.panelAnimator.AnimationType = BunifuAnimatorNS.AnimationType.Leaf;
            this.panelAnimator.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 1F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 0F;
            this.panelAnimator.DefaultAnimation = animation1;
            // 
            // bunifuGradientPanel3
            // 
            this.bunifuGradientPanel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel3.BackgroundImage")));
            this.bunifuGradientPanel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel3.Controls.Add(this.createXLSBtn);
            this.bunifuGradientPanel3.Controls.Add(this.createXLS2Btn);
            this.bunifuGradientPanel3.Controls.Add(this.dateSearchBox);
            this.bunifuGradientPanel3.Controls.Add(this.cmpnToSearchBox);
            this.bunifuGradientPanel3.Controls.Add(this.trnsprtTypeSearchBox);
            this.bunifuGradientPanel3.Controls.Add(this.priceSearchBox);
            this.bunifuGradientPanel3.Controls.Add(this.gdsTypeSearchBox);
            this.bunifuGradientPanel3.Controls.Add(this.cmpnFromSearchBox);
            this.bunifuGradientPanel3.Controls.Add(this.arrivaDateSearchBox);
            this.bunifuGradientPanel3.Controls.Add(this.daysCountFromSearchBox);
            this.bunifuGradientPanel3.Controls.Add(this.kpngPriceSearchBox);
            this.bunifuGradientPanel3.Controls.Add(this.gdstpFromSearchBox);
            this.bunifuGradientPanel3.Controls.Add(this.metroLabel1);
            this.bunifuGradientPanel3.Controls.Add(this.lookWayBillsBtn);
            this.bunifuGradientPanel3.Controls.Add(this.imgList);
            this.bunifuGradientPanel3.Controls.Add(this.slidemenu);
            this.bunifuGradientPanel3.Controls.Add(this.metroGrid2);
            this.bunifuGradientPanel3.Controls.Add(this.lookPurchBtn);
            this.bunifuGradientPanel3.Controls.Add(this.metroGrid1);
            this.panelAnimator.SetDecoration(this.bunifuGradientPanel3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuGradientPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bunifuGradientPanel3.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientTopLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel3.Location = new System.Drawing.Point(20, 108);
            this.bunifuGradientPanel3.Name = "bunifuGradientPanel3";
            this.bunifuGradientPanel3.Quality = 10;
            this.bunifuGradientPanel3.Size = new System.Drawing.Size(1205, 621);
            this.bunifuGradientPanel3.TabIndex = 23;
            // 
            // createXLSBtn
            // 
            this.createXLSBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.createXLSBtn.BackColor = System.Drawing.Color.Teal;
            this.createXLSBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.createXLSBtn.BorderRadius = 0;
            this.createXLSBtn.ButtonText = "Создать XLSX файл";
            this.createXLSBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.createXLSBtn, BunifuAnimatorNS.DecorationType.None);
            this.createXLSBtn.DisabledColor = System.Drawing.Color.Gray;
            this.createXLSBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.createXLSBtn.Iconimage = null;
            this.createXLSBtn.Iconimage_right = null;
            this.createXLSBtn.Iconimage_right_Selected = null;
            this.createXLSBtn.Iconimage_Selected = null;
            this.createXLSBtn.IconMarginLeft = 0;
            this.createXLSBtn.IconMarginRight = 0;
            this.createXLSBtn.IconRightVisible = true;
            this.createXLSBtn.IconRightZoom = 0D;
            this.createXLSBtn.IconVisible = true;
            this.createXLSBtn.IconZoom = 90D;
            this.createXLSBtn.IsTab = false;
            this.createXLSBtn.Location = new System.Drawing.Point(802, 200);
            this.createXLSBtn.Name = "createXLSBtn";
            this.createXLSBtn.Normalcolor = System.Drawing.Color.Teal;
            this.createXLSBtn.OnHovercolor = System.Drawing.Color.LightSeaGreen;
            this.createXLSBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.createXLSBtn.selected = false;
            this.createXLSBtn.Size = new System.Drawing.Size(380, 48);
            this.createXLSBtn.TabIndex = 78;
            this.createXLSBtn.Text = "Создать XLSX файл";
            this.createXLSBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.createXLSBtn.Textcolor = System.Drawing.Color.White;
            this.createXLSBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createXLSBtn.Click += new System.EventHandler(this.createXLSBtn_Click);
            // 
            // createXLS2Btn
            // 
            this.createXLS2Btn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.createXLS2Btn.BackColor = System.Drawing.Color.Teal;
            this.createXLS2Btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.createXLS2Btn.BorderRadius = 0;
            this.createXLS2Btn.ButtonText = "Создать XLSX файл";
            this.createXLS2Btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.createXLS2Btn, BunifuAnimatorNS.DecorationType.None);
            this.createXLS2Btn.DisabledColor = System.Drawing.Color.Gray;
            this.createXLS2Btn.Iconcolor = System.Drawing.Color.Transparent;
            this.createXLS2Btn.Iconimage = null;
            this.createXLS2Btn.Iconimage_right = null;
            this.createXLS2Btn.Iconimage_right_Selected = null;
            this.createXLS2Btn.Iconimage_Selected = null;
            this.createXLS2Btn.IconMarginLeft = 0;
            this.createXLS2Btn.IconMarginRight = 0;
            this.createXLS2Btn.IconRightVisible = true;
            this.createXLS2Btn.IconRightZoom = 0D;
            this.createXLS2Btn.IconVisible = true;
            this.createXLS2Btn.IconZoom = 90D;
            this.createXLS2Btn.IsTab = false;
            this.createXLS2Btn.Location = new System.Drawing.Point(801, 200);
            this.createXLS2Btn.Name = "createXLS2Btn";
            this.createXLS2Btn.Normalcolor = System.Drawing.Color.Teal;
            this.createXLS2Btn.OnHovercolor = System.Drawing.Color.LightSeaGreen;
            this.createXLS2Btn.OnHoverTextColor = System.Drawing.Color.White;
            this.createXLS2Btn.selected = false;
            this.createXLS2Btn.Size = new System.Drawing.Size(380, 48);
            this.createXLS2Btn.TabIndex = 89;
            this.createXLS2Btn.Text = "Создать XLSX файл";
            this.createXLS2Btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.createXLS2Btn.Textcolor = System.Drawing.Color.White;
            this.createXLS2Btn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createXLS2Btn.Click += new System.EventHandler(this.createXLS2Btn_Click);
            // 
            // dateSearchBox
            // 
            this.dateSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.dateSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.dateSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.dateSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dateSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.dateSearchBox.HintText = "Поиск по дате отправки";
            this.dateSearchBox.isPassword = false;
            this.dateSearchBox.LineFocusedColor = System.Drawing.Color.MidnightBlue;
            this.dateSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.dateSearchBox.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.dateSearchBox.LineThickness = 3;
            this.dateSearchBox.Location = new System.Drawing.Point(1003, 106);
            this.dateSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.dateSearchBox.Name = "dateSearchBox";
            this.dateSearchBox.Size = new System.Drawing.Size(179, 33);
            this.dateSearchBox.TabIndex = 81;
            this.dateSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.dateSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateSearchBox_KeyUp);
            // 
            // cmpnToSearchBox
            // 
            this.cmpnToSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.cmpnToSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.cmpnToSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.cmpnToSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cmpnToSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.cmpnToSearchBox.HintText = "Поиск по получающей компании";
            this.cmpnToSearchBox.isPassword = false;
            this.cmpnToSearchBox.LineFocusedColor = System.Drawing.Color.MidnightBlue;
            this.cmpnToSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.cmpnToSearchBox.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.cmpnToSearchBox.LineThickness = 3;
            this.cmpnToSearchBox.Location = new System.Drawing.Point(812, 7);
            this.cmpnToSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.cmpnToSearchBox.Name = "cmpnToSearchBox";
            this.cmpnToSearchBox.Size = new System.Drawing.Size(370, 33);
            this.cmpnToSearchBox.TabIndex = 82;
            this.cmpnToSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.cmpnToSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmpnToSearchBox_KeyUp_1);
            // 
            // trnsprtTypeSearchBox
            // 
            this.trnsprtTypeSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.trnsprtTypeSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.trnsprtTypeSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.trnsprtTypeSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.trnsprtTypeSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.trnsprtTypeSearchBox.HintText = "Поиск по типу перевозчика";
            this.trnsprtTypeSearchBox.isPassword = false;
            this.trnsprtTypeSearchBox.LineFocusedColor = System.Drawing.Color.MidnightBlue;
            this.trnsprtTypeSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.trnsprtTypeSearchBox.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.trnsprtTypeSearchBox.LineThickness = 3;
            this.trnsprtTypeSearchBox.Location = new System.Drawing.Point(812, 73);
            this.trnsprtTypeSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.trnsprtTypeSearchBox.Name = "trnsprtTypeSearchBox";
            this.trnsprtTypeSearchBox.Size = new System.Drawing.Size(370, 33);
            this.trnsprtTypeSearchBox.TabIndex = 83;
            this.trnsprtTypeSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.trnsprtTypeSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.trnsprtTypeSearchBox_KeyUp_1);
            // 
            // priceSearchBox
            // 
            this.priceSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.priceSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.priceSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.priceSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.priceSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.priceSearchBox.HintText = "Поиск по цене перегрузки";
            this.priceSearchBox.isPassword = false;
            this.priceSearchBox.LineFocusedColor = System.Drawing.Color.MidnightBlue;
            this.priceSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.priceSearchBox.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.priceSearchBox.LineThickness = 3;
            this.priceSearchBox.Location = new System.Drawing.Point(806, 106);
            this.priceSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.priceSearchBox.Name = "priceSearchBox";
            this.priceSearchBox.Size = new System.Drawing.Size(189, 33);
            this.priceSearchBox.TabIndex = 79;
            this.priceSearchBox.Text = "Поиск по цене перевозки";
            this.priceSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.priceSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.priceSearchBox_KeyUp);
            // 
            // gdsTypeSearchBox
            // 
            this.gdsTypeSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.gdsTypeSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.gdsTypeSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.gdsTypeSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gdsTypeSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.gdsTypeSearchBox.HintText = "Поиск по типу товара";
            this.gdsTypeSearchBox.isPassword = false;
            this.gdsTypeSearchBox.LineFocusedColor = System.Drawing.Color.MidnightBlue;
            this.gdsTypeSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.gdsTypeSearchBox.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.gdsTypeSearchBox.LineThickness = 3;
            this.gdsTypeSearchBox.Location = new System.Drawing.Point(812, 40);
            this.gdsTypeSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.gdsTypeSearchBox.Name = "gdsTypeSearchBox";
            this.gdsTypeSearchBox.Size = new System.Drawing.Size(370, 33);
            this.gdsTypeSearchBox.TabIndex = 80;
            this.gdsTypeSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.gdsTypeSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gdsTypeSearchBox_KeyUp_1);
            // 
            // cmpnFromSearchBox
            // 
            this.cmpnFromSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.cmpnFromSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.cmpnFromSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.cmpnFromSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cmpnFromSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.cmpnFromSearchBox.HintText = "Поиск по отправляющей компании";
            this.cmpnFromSearchBox.isPassword = false;
            this.cmpnFromSearchBox.LineFocusedColor = System.Drawing.Color.MidnightBlue;
            this.cmpnFromSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.cmpnFromSearchBox.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.cmpnFromSearchBox.LineThickness = 3;
            this.cmpnFromSearchBox.Location = new System.Drawing.Point(812, 11);
            this.cmpnFromSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.cmpnFromSearchBox.Name = "cmpnFromSearchBox";
            this.cmpnFromSearchBox.Size = new System.Drawing.Size(370, 33);
            this.cmpnFromSearchBox.TabIndex = 88;
            this.cmpnFromSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.cmpnFromSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmpnFromSearchBox_KeyUp_1);
            // 
            // arrivaDateSearchBox
            // 
            this.arrivaDateSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.arrivaDateSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.arrivaDateSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.arrivaDateSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.arrivaDateSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.arrivaDateSearchBox.HintText = "Поиск по дате прибытия";
            this.arrivaDateSearchBox.isPassword = false;
            this.arrivaDateSearchBox.LineFocusedColor = System.Drawing.Color.MidnightBlue;
            this.arrivaDateSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.arrivaDateSearchBox.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.arrivaDateSearchBox.LineThickness = 3;
            this.arrivaDateSearchBox.Location = new System.Drawing.Point(1003, 106);
            this.arrivaDateSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.arrivaDateSearchBox.Name = "arrivaDateSearchBox";
            this.arrivaDateSearchBox.Size = new System.Drawing.Size(179, 33);
            this.arrivaDateSearchBox.TabIndex = 86;
            this.arrivaDateSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.arrivaDateSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.arrivaDateSearchBox_KeyUp_1);
            // 
            // daysCountFromSearchBox
            // 
            this.daysCountFromSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.daysCountFromSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.daysCountFromSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.daysCountFromSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.daysCountFromSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.daysCountFromSearchBox.HintText = "Поиск по кол-ву дней хранения";
            this.daysCountFromSearchBox.isPassword = false;
            this.daysCountFromSearchBox.LineFocusedColor = System.Drawing.Color.MidnightBlue;
            this.daysCountFromSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.daysCountFromSearchBox.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.daysCountFromSearchBox.LineThickness = 3;
            this.daysCountFromSearchBox.Location = new System.Drawing.Point(812, 73);
            this.daysCountFromSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.daysCountFromSearchBox.Name = "daysCountFromSearchBox";
            this.daysCountFromSearchBox.Size = new System.Drawing.Size(370, 33);
            this.daysCountFromSearchBox.TabIndex = 87;
            this.daysCountFromSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.daysCountFromSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.daysCountFromSearchBox_KeyUp_1);
            // 
            // kpngPriceSearchBox
            // 
            this.kpngPriceSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.kpngPriceSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.kpngPriceSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.kpngPriceSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.kpngPriceSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.kpngPriceSearchBox.HintText = "Поиск по цене хранения";
            this.kpngPriceSearchBox.isPassword = false;
            this.kpngPriceSearchBox.LineFocusedColor = System.Drawing.Color.MidnightBlue;
            this.kpngPriceSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.kpngPriceSearchBox.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.kpngPriceSearchBox.LineThickness = 3;
            this.kpngPriceSearchBox.Location = new System.Drawing.Point(806, 106);
            this.kpngPriceSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.kpngPriceSearchBox.Name = "kpngPriceSearchBox";
            this.kpngPriceSearchBox.Size = new System.Drawing.Size(189, 33);
            this.kpngPriceSearchBox.TabIndex = 84;
            this.kpngPriceSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.kpngPriceSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.kpngPriceSearchBox_KeyUp_1);
            // 
            // gdstpFromSearchBox
            // 
            this.gdstpFromSearchBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.panelAnimator.SetDecoration(this.gdstpFromSearchBox, BunifuAnimatorNS.DecorationType.None);
            this.gdstpFromSearchBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.gdstpFromSearchBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gdstpFromSearchBox.HintForeColor = System.Drawing.Color.Empty;
            this.gdstpFromSearchBox.HintText = "Поиск по типу товара";
            this.gdstpFromSearchBox.isPassword = false;
            this.gdstpFromSearchBox.LineFocusedColor = System.Drawing.Color.MidnightBlue;
            this.gdstpFromSearchBox.LineIdleColor = System.Drawing.Color.Gray;
            this.gdstpFromSearchBox.LineMouseHoverColor = System.Drawing.Color.Teal;
            this.gdstpFromSearchBox.LineThickness = 3;
            this.gdstpFromSearchBox.Location = new System.Drawing.Point(812, 40);
            this.gdstpFromSearchBox.Margin = new System.Windows.Forms.Padding(4);
            this.gdstpFromSearchBox.Name = "gdstpFromSearchBox";
            this.gdstpFromSearchBox.Size = new System.Drawing.Size(370, 33);
            this.gdstpFromSearchBox.TabIndex = 85;
            this.gdstpFromSearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.gdstpFromSearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gdstpFromSearchBox_KeyUp_1);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.panelAnimator.SetDecoration(this.metroLabel1, BunifuAnimatorNS.DecorationType.None);
            this.metroLabel1.Location = new System.Drawing.Point(941, 251);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(108, 19);
            this.metroLabel1.TabIndex = 77;
            this.metroLabel1.Text = "Список товаров";
            // 
            // lookWayBillsBtn
            // 
            this.lookWayBillsBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.lookWayBillsBtn.BackColor = System.Drawing.Color.Teal;
            this.lookWayBillsBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.lookWayBillsBtn.BorderRadius = 0;
            this.lookWayBillsBtn.ButtonText = "Перейти к  просмотру расходным накладным";
            this.lookWayBillsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.lookWayBillsBtn, BunifuAnimatorNS.DecorationType.None);
            this.lookWayBillsBtn.DisabledColor = System.Drawing.Color.Gray;
            this.lookWayBillsBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.lookWayBillsBtn.Iconimage = null;
            this.lookWayBillsBtn.Iconimage_right = null;
            this.lookWayBillsBtn.Iconimage_right_Selected = null;
            this.lookWayBillsBtn.Iconimage_Selected = null;
            this.lookWayBillsBtn.IconMarginLeft = 0;
            this.lookWayBillsBtn.IconMarginRight = 0;
            this.lookWayBillsBtn.IconRightVisible = true;
            this.lookWayBillsBtn.IconRightZoom = 0D;
            this.lookWayBillsBtn.IconVisible = true;
            this.lookWayBillsBtn.IconZoom = 90D;
            this.lookWayBillsBtn.IsTab = false;
            this.lookWayBillsBtn.Location = new System.Drawing.Point(802, 146);
            this.lookWayBillsBtn.Name = "lookWayBillsBtn";
            this.lookWayBillsBtn.Normalcolor = System.Drawing.Color.Teal;
            this.lookWayBillsBtn.OnHovercolor = System.Drawing.Color.LightSeaGreen;
            this.lookWayBillsBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.lookWayBillsBtn.selected = false;
            this.lookWayBillsBtn.Size = new System.Drawing.Size(380, 48);
            this.lookWayBillsBtn.TabIndex = 74;
            this.lookWayBillsBtn.Text = "Перейти к  просмотру расходным накладным";
            this.lookWayBillsBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lookWayBillsBtn.Textcolor = System.Drawing.Color.White;
            this.lookWayBillsBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookWayBillsBtn.Click += new System.EventHandler(this.lookWayBillsBtn_Click);
            // 
            // imgList
            // 
            this.imgList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelAnimator.SetDecoration(this.imgList, BunifuAnimatorNS.DecorationType.None);
            this.imgList.Location = new System.Drawing.Point(843, 273);
            this.imgList.Name = "imgList";
            this.imgList.Size = new System.Drawing.Size(324, 339);
            this.imgList.TabIndex = 68;
            this.imgList.TabStop = false;
            // 
            // slidemenu
            // 
            this.slidemenu.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("slidemenu.BackgroundImage")));
            this.slidemenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slidemenu.Controls.Add(this.bunifuFlatButton1);
            this.slidemenu.Controls.Add(this.bunifuFlatButton5);
            this.slidemenu.Controls.Add(this.bunifuFlatButton7);
            this.slidemenu.Controls.Add(this.btnSlide);
            this.panelAnimator.SetDecoration(this.slidemenu, BunifuAnimatorNS.DecorationType.None);
            this.slidemenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.slidemenu.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.slidemenu.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.slidemenu.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.slidemenu.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.slidemenu.Location = new System.Drawing.Point(0, 0);
            this.slidemenu.Name = "slidemenu";
            this.slidemenu.Quality = 10;
            this.slidemenu.Size = new System.Drawing.Size(50, 621);
            this.slidemenu.TabIndex = 66;
            // 
            // bunifuFlatButton1
            // 
            this.bunifuFlatButton1.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton1.BackColor = System.Drawing.Color.Teal;
            this.bunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton1.BorderRadius = 0;
            this.bunifuFlatButton1.ButtonText = "Сменить пользователя";
            this.bunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.bunifuFlatButton1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton1.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton1.Iconimage = global::AutoStorage.Properties.Resources.icons8_Export_64;
            this.bunifuFlatButton1.Iconimage_right = null;
            this.bunifuFlatButton1.Iconimage_right_Selected = null;
            this.bunifuFlatButton1.Iconimage_Selected = null;
            this.bunifuFlatButton1.IconMarginLeft = 0;
            this.bunifuFlatButton1.IconMarginRight = 0;
            this.bunifuFlatButton1.IconRightVisible = true;
            this.bunifuFlatButton1.IconRightZoom = 0D;
            this.bunifuFlatButton1.IconVisible = true;
            this.bunifuFlatButton1.IconZoom = 90D;
            this.bunifuFlatButton1.IsTab = false;
            this.bunifuFlatButton1.Location = new System.Drawing.Point(0, 214);
            this.bunifuFlatButton1.Name = "bunifuFlatButton1";
            this.bunifuFlatButton1.Normalcolor = System.Drawing.Color.Teal;
            this.bunifuFlatButton1.OnHovercolor = System.Drawing.Color.DeepSkyBlue;
            this.bunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton1.selected = false;
            this.bunifuFlatButton1.Size = new System.Drawing.Size(241, 48);
            this.bunifuFlatButton1.TabIndex = 23;
            this.bunifuFlatButton1.Text = "Сменить пользователя";
            this.bunifuFlatButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton1.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton1.Click += new System.EventHandler(this.bunifuFlatButton1_Click);
            // 
            // bunifuFlatButton5
            // 
            this.bunifuFlatButton5.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton5.BackColor = System.Drawing.Color.Teal;
            this.bunifuFlatButton5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton5.BorderRadius = 0;
            this.bunifuFlatButton5.ButtonText = " Личный кабинет";
            this.bunifuFlatButton5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.bunifuFlatButton5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton5.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton5.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton5.Iconimage = global::AutoStorage.Properties.Resources.icons8_User_64;
            this.bunifuFlatButton5.Iconimage_right = null;
            this.bunifuFlatButton5.Iconimage_right_Selected = null;
            this.bunifuFlatButton5.Iconimage_Selected = null;
            this.bunifuFlatButton5.IconMarginLeft = 0;
            this.bunifuFlatButton5.IconMarginRight = 0;
            this.bunifuFlatButton5.IconRightVisible = true;
            this.bunifuFlatButton5.IconRightZoom = 0D;
            this.bunifuFlatButton5.IconVisible = true;
            this.bunifuFlatButton5.IconZoom = 90D;
            this.bunifuFlatButton5.IsTab = false;
            this.bunifuFlatButton5.Location = new System.Drawing.Point(0, 160);
            this.bunifuFlatButton5.Name = "bunifuFlatButton5";
            this.bunifuFlatButton5.Normalcolor = System.Drawing.Color.Teal;
            this.bunifuFlatButton5.OnHovercolor = System.Drawing.Color.DeepSkyBlue;
            this.bunifuFlatButton5.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton5.selected = false;
            this.bunifuFlatButton5.Size = new System.Drawing.Size(241, 48);
            this.bunifuFlatButton5.TabIndex = 18;
            this.bunifuFlatButton5.Text = " Личный кабинет";
            this.bunifuFlatButton5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton5.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton5.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton5.Click += new System.EventHandler(this.bunifuFlatButton5_Click);
            // 
            // bunifuFlatButton7
            // 
            this.bunifuFlatButton7.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton7.BackColor = System.Drawing.Color.Teal;
            this.bunifuFlatButton7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton7.BorderRadius = 0;
            this.bunifuFlatButton7.ButtonText = " Просмотр накладных";
            this.bunifuFlatButton7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.bunifuFlatButton7, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton7.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton7.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton7.Iconimage = global::AutoStorage.Properties.Resources.icons8_Search_64;
            this.bunifuFlatButton7.Iconimage_right = null;
            this.bunifuFlatButton7.Iconimage_right_Selected = null;
            this.bunifuFlatButton7.Iconimage_Selected = null;
            this.bunifuFlatButton7.IconMarginLeft = 0;
            this.bunifuFlatButton7.IconMarginRight = 0;
            this.bunifuFlatButton7.IconRightVisible = true;
            this.bunifuFlatButton7.IconRightZoom = 0D;
            this.bunifuFlatButton7.IconVisible = true;
            this.bunifuFlatButton7.IconZoom = 90D;
            this.bunifuFlatButton7.IsTab = false;
            this.bunifuFlatButton7.Location = new System.Drawing.Point(0, 106);
            this.bunifuFlatButton7.Name = "bunifuFlatButton7";
            this.bunifuFlatButton7.Normalcolor = System.Drawing.Color.Teal;
            this.bunifuFlatButton7.OnHovercolor = System.Drawing.Color.DeepSkyBlue;
            this.bunifuFlatButton7.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton7.selected = false;
            this.bunifuFlatButton7.Size = new System.Drawing.Size(241, 48);
            this.bunifuFlatButton7.TabIndex = 22;
            this.bunifuFlatButton7.Text = " Просмотр накладных";
            this.bunifuFlatButton7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton7.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton7.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton7.Click += new System.EventHandler(this.bunifuFlatButton7_Click);
            // 
            // btnSlide
            // 
            this.btnSlide.BackColor = System.Drawing.Color.Transparent;
            this.btnSlide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelAnimator.SetDecoration(this.btnSlide, BunifuAnimatorNS.DecorationType.None);
            this.btnSlide.FlatAppearance.BorderSize = 0;
            this.btnSlide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSlide.Image = global::AutoStorage.Properties.Resources.icons8_Menu_32;
            this.btnSlide.Location = new System.Drawing.Point(6, 12);
            this.btnSlide.Name = "btnSlide";
            this.btnSlide.Size = new System.Drawing.Size(36, 37);
            this.btnSlide.TabIndex = 4;
            this.btnSlide.UseVisualStyleBackColor = false;
            this.btnSlide.Click += new System.EventHandler(this.btnSlide_Click);
            // 
            // metroGrid2
            // 
            this.metroGrid2.AllowUserToResizeRows = false;
            this.metroGrid2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.metroGrid2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.metroGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.panelAnimator.SetDecoration(this.metroGrid2, BunifuAnimatorNS.DecorationType.None);
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid2.DefaultCellStyle = dataGridViewCellStyle2;
            this.metroGrid2.EnableHeadersVisualStyles = false;
            this.metroGrid2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid2.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid2.Location = new System.Drawing.Point(49, -1);
            this.metroGrid2.Name = "metroGrid2";
            this.metroGrid2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid2.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.metroGrid2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid2.Size = new System.Drawing.Size(746, 613);
            this.metroGrid2.TabIndex = 67;
            this.metroGrid2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.metroGrid2_CellContentClick);
            // 
            // lookPurchBtn
            // 
            this.lookPurchBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.lookPurchBtn.BackColor = System.Drawing.Color.Teal;
            this.lookPurchBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.lookPurchBtn.BorderRadius = 0;
            this.lookPurchBtn.ButtonText = "Перейти к просмотру приходных накладных";
            this.lookPurchBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelAnimator.SetDecoration(this.lookPurchBtn, BunifuAnimatorNS.DecorationType.None);
            this.lookPurchBtn.DisabledColor = System.Drawing.Color.Gray;
            this.lookPurchBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.lookPurchBtn.Iconimage = null;
            this.lookPurchBtn.Iconimage_right = null;
            this.lookPurchBtn.Iconimage_right_Selected = null;
            this.lookPurchBtn.Iconimage_Selected = null;
            this.lookPurchBtn.IconMarginLeft = 0;
            this.lookPurchBtn.IconMarginRight = 0;
            this.lookPurchBtn.IconRightVisible = true;
            this.lookPurchBtn.IconRightZoom = 0D;
            this.lookPurchBtn.IconVisible = true;
            this.lookPurchBtn.IconZoom = 90D;
            this.lookPurchBtn.IsTab = false;
            this.lookPurchBtn.Location = new System.Drawing.Point(802, 146);
            this.lookPurchBtn.Name = "lookPurchBtn";
            this.lookPurchBtn.Normalcolor = System.Drawing.Color.Teal;
            this.lookPurchBtn.OnHovercolor = System.Drawing.Color.LightSeaGreen;
            this.lookPurchBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.lookPurchBtn.selected = false;
            this.lookPurchBtn.Size = new System.Drawing.Size(380, 48);
            this.lookPurchBtn.TabIndex = 11;
            this.lookPurchBtn.Text = "Перейти к просмотру приходных накладных";
            this.lookPurchBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lookPurchBtn.Textcolor = System.Drawing.Color.White;
            this.lookPurchBtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookPurchBtn.Click += new System.EventHandler(this.lookPurchBtn_Click);
            // 
            // metroGrid1
            // 
            this.metroGrid1.AllowUserToResizeRows = false;
            this.metroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.metroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.metroGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.panelAnimator.SetDecoration(this.metroGrid1, BunifuAnimatorNS.DecorationType.None);
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid1.DefaultCellStyle = dataGridViewCellStyle5;
            this.metroGrid1.EnableHeadersVisualStyles = false;
            this.metroGrid1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.Location = new System.Drawing.Point(49, -1);
            this.metroGrid1.Name = "metroGrid1";
            this.metroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.metroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid1.Size = new System.Drawing.Size(746, 571);
            this.metroGrid1.TabIndex = 5;
            this.metroGrid1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.metroGrid1_CellContentClick);
            // 
            // backToWorkWithBills
            // 
            this.backToWorkWithBills.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("backToWorkWithBills.BackgroundImage")));
            this.backToWorkWithBills.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelAnimator.SetDecoration(this.backToWorkWithBills, BunifuAnimatorNS.DecorationType.None);
            this.backToWorkWithBills.FlatAppearance.BorderSize = 0;
            this.backToWorkWithBills.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backToWorkWithBills.Location = new System.Drawing.Point(14, 21);
            this.backToWorkWithBills.Name = "backToWorkWithBills";
            this.backToWorkWithBills.Size = new System.Drawing.Size(32, 32);
            this.backToWorkWithBills.TabIndex = 24;
            this.backToWorkWithBills.UseVisualStyleBackColor = true;
            this.backToWorkWithBills.Click += new System.EventHandler(this.backToWorkWithBills_Click);
            // 
            // bunifuGradientPanel1
            // 
            this.bunifuGradientPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel1.BackgroundImage")));
            this.bunifuGradientPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel1.Controls.Add(this.menuStrip1);
            this.panelAnimator.SetDecoration(this.bunifuGradientPanel1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuGradientPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.bunifuGradientPanel1.Location = new System.Drawing.Point(20, 60);
            this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
            this.bunifuGradientPanel1.Quality = 10;
            this.bunifuGradientPanel1.Size = new System.Drawing.Size(1205, 48);
            this.bunifuGradientPanel1.TabIndex = 22;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.panelAnimator.SetDecoration(this.menuStrip1, BunifuAnimatorNS.DecorationType.None);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.обновитьToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStrip1.Size = new System.Drawing.Size(1205, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // обновитьToolStripMenuItem
            // 
            this.обновитьToolStripMenuItem.Name = "обновитьToolStripMenuItem";
            this.обновитьToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.обновитьToolStripMenuItem.Text = "Обновить ";
            this.обновитьToolStripMenuItem.Click += new System.EventHandler(this.обновитьToolStripMenuItem_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            this.справкаToolStripMenuItem.Click += new System.EventHandler(this.справкаToolStripMenuItem_Click);
            // 
            // financerBillsLookUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1245, 749);
            this.Controls.Add(this.bunifuGradientPanel3);
            this.Controls.Add(this.backToWorkWithBills);
            this.Controls.Add(this.bunifuGradientPanel1);
            this.panelAnimator.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.Name = "financerBillsLookUp";
            this.Text = "   Просмотр накладных и создание отчетов";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.financerBillsLookUp_FormClosing);
            this.Load += new System.EventHandler(this.financerBillsLookUp_Load);
            this.bunifuGradientPanel3.ResumeLayout(false);
            this.bunifuGradientPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgList)).EndInit();
            this.slidemenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).EndInit();
            this.bunifuGradientPanel1.ResumeLayout(false);
            this.bunifuGradientPanel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button backToWorkWithBills;
        private Bunifu.Framework.UI.BunifuFlatButton createXLSBtn;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private Bunifu.Framework.UI.BunifuFlatButton lookWayBillsBtn;
        private System.Windows.Forms.PictureBox imgList;
        private Bunifu.Framework.UI.BunifuGradientPanel slidemenu;
        private System.Windows.Forms.Button btnSlide;
        private MetroFramework.Controls.MetroGrid metroGrid1;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel3;
        private MetroFramework.Controls.MetroGrid metroGrid2;
        private Bunifu.Framework.UI.BunifuFlatButton lookPurchBtn;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem обновитьToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox cmpnFromSearchBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox arrivaDateSearchBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox daysCountFromSearchBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox kpngPriceSearchBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox gdstpFromSearchBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox dateSearchBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox cmpnToSearchBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox trnsprtTypeSearchBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox priceSearchBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox gdsTypeSearchBox;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton1;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton5;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton7;
        private BunifuAnimatorNS.BunifuTransition panelAnimator;
        private Bunifu.Framework.UI.BunifuFlatButton createXLS2Btn;
    }
}